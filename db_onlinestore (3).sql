-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2016 at 01:11 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_onlinestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `word` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(57, 1481328508, '127.0.0.1', 'O8GPD8pU'),
(60, 1481328633, '127.0.0.1', 'lEbT9vL0'),
(59, 1481328624, '127.0.0.1', 'w45uDJZY'),
(52, 1481327838, '127.0.0.1', 'MupwrOtn'),
(53, 1481327967, '127.0.0.1', 'qnDUAlQp'),
(54, 1481327969, '127.0.0.1', 'vfhUYyia'),
(55, 1481328015, '127.0.0.1', 'elDU53E1'),
(56, 1481328298, '127.0.0.1', '0Q0BUdVU'),
(58, 1481328537, '127.0.0.1', 'KOwmCyK4'),
(61, 1481328690, '127.0.0.1', 'ANVkDbxD'),
(62, 1481328695, '127.0.0.1', 'MzYHzXHu'),
(63, 1481328696, '127.0.0.1', 'uCMjzRIG'),
(64, 1481328714, '127.0.0.1', 'PnYUK05c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `kode_banner` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `stts` varchar(1) NOT NULL,
  PRIMARY KEY (`kode_banner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`kode_banner`, `judul`, `deskripsi`, `gambar`, `stts`) VALUES
(1, 'Kami Hadir Untuk Anda | Harmonis Grosir Sandal Online', '<p>Selamat datang di HarmonisGrosirSandal.com. Grosir sandal termurah No.1 di Tasik malaya. Berawal dari dor to dor, pasar ke pasar, toko ke toko, kami memulai bisnis ini. Allhamdulilah sekarang sudah menjangkau daerah jawa dan bali. Target peasaran kami ke seluruh penjuru Indonesia dan luar Negeri. Amien... Kami menyediakan berbagai macam sandal dari anak-anak sampai dewasa dengan berbagai macam fariativ dan selalu trendy. Sandal yang kami pasarkan kesemuanya merupakan hasil dari home industri dengan berbasis cinta produk indonesia.</p>', 'image1.jpg', '1'),
(6, 'Kami Hadir Untuk Anda | Harmonis Grosir Sandal Online', '<p>Selamat datang di HarmonisGrosirSandal.com. Grosir sandal termurah No.1 di Tasik malaya. Berawal dari dor to dor, pasar ke pasar, toko ke toko, kami memulai bisnis ini. Allhamdulilah sekarang sudah menjangkau daerah jawa dan bali. Target peasaran kami ke seluruh penjuru Indonesia dan luar Negeri. Amien... Kami menyediakan berbagai macam sandal dari anak-anak sampai dewasa dengan berbagai macam fariativ dan selalu trendy. Sandal yang kami pasarkan kesemuanya merupakan hasil dari home industri dengan berbasis cinta produk indonesia.</p>', '689091963image1.jpg', '1'),
(7, 'Kami Hadir Untuk Anda | Harmonis Grosir Sandal Online', '<p>Selamat datang di HarmonisGrosirSandal.com. Grosir sandal termurah No.1 di Tasik malaya. Berawal dari dor to dor, pasar ke pasar, toko ke toko, kami memulai bisnis ini. Allhamdulilah sekarang sudah menjangkau daerah jawa dan bali. Target peasaran kami ke seluruh penjuru Indonesia dan luar Negeri. Amien... Kami menyediakan berbagai macam sandal dari anak-anak sampai dewasa dengan berbagai macam fariativ dan selalu trendy. Sandal yang kami pasarkan kesemuanya merupakan hasil dari home industri dengan berbasis cinta produk indonesia.</p>', '544098080image1.jpg', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bglogin`
--

CREATE TABLE IF NOT EXISTS `tbl_bglogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_bglogin`
--

INSERT INTO `tbl_bglogin` (`id`, `logo`) VALUES
(1, 'bg-5.jpg'),
(2, 'bg-6.jpg'),
(3, 'bg-7.jpg'),
(4, 'bg-8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_intermezzo`
--

CREATE TABLE IF NOT EXISTS `tbl_intermezzo` (
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `isi_berita` text COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=130 ;

--
-- Dumping data for table `tbl_intermezzo`
--

INSERT INTO `tbl_intermezzo` (`id_berita`, `judul`, `isi_berita`, `tanggal`, `jam`, `gambar`, `dibaca`) VALUES
(76, 'Laskar Pelangi Pecahkan Rekor', 'Sukses film Laskar Pelangi dalam memecahkan rekor jumlah penonton memberi pembelajaran bahwa penonton film Indonesia bisa menerima inovasi. Mira Lesmana dari Miles Films yang memproduksi film ini mengatakan, sampai Rabu (12/11), pemutaran Laskar Pelangi di 100 layar bioskop di 25 kota menyedot lebih dari 4,4 juta penonton. Padahal, Kamis kemarin, jumlah kota yang memutar film itu bertambah dengan Padang, Tasikmalaya, dan Ambon. Sebelumnya, Ayat-ayat Cinta ditonton 3,7 juta penonton (Kompas, 26/10).<br><br>Jumlah penonton itu belum termasuk penonton layar tancap untuk menjangkau penonton di daerah yang belum memiliki gedung bioskop.<br><br>Menurut Mira, layar tancap di tiga lokasi di Belitung, tempat cerita berlokasi, menyedot penonton lebih dari 60.000 penonton dan di Bangka sekitar 80.000-an orang. Pemutaran layar tancap juga dilakukan di Rantau (Sumatera Utara) dan akan dilakukan di Natuna, Aceh (enam lokasi), Lombok, serta Papua di Timika, Sorong, dan Jayapura.<br><br>Kabar gembira lainnya, film ini menjadi salah satu film yang terpilih untuk menjadi bagian dari Berlin International Film Festival atau Berlinale 2009. Festival ini adalah sebuah peristiwa budaya terpenting di Jerman yang juga adalah salah satu festival film internasional paling bergengsi di dunia.<br><br>Film Laskar Pelangi diangkat dari novel berjudul sama karya Andrea Hirata. Film ini mengangkat realitas sosial masyarakat Belitung, tentang persahabatan, kegigihan dan harapan, dalam bingkai kemiskinan dan ketimpangan kelas sosial.<br><br>"Jumlah penonton dan panjangnya masa pemutaran film sejak 25 September memperlihatkan penonton butuh sesuatu yang baru, yang inovatif, walau yang ditampilkan realitas tidak gemerlap," papar Mira.<br><br>Selama ini, kebanyakan film Indonesia bertema drama cinta, horor, dan komedi, sementara Miles Films dalam empat film terakhirnya menggarap tema realisme sosial-politik.<br><br>Mira mengakui, inovasi itu tidak selalu berhasil secara komersial. Contohnya Gie, juga produksi Miles Films. Meskipun dari sisi kritik film dan kreativitas bagus, tetapi tidak sesukses Laskar Pelangi dalam pemasaran.<br><br>Produksi film ini menghabiskan biaya Rp 9 miliar dan 90 persen dikerjakan di dalam negeri. "Sound mixing dengan sistem Dolby dan transfer optis untuk suara belum bisa dikerjakan di dalam negeri," ujar Mira.<br><br>Miles Films dan para investor, antara lain Mizan Publishing, kini bersiap memproduksi lanjutan Laskar Pelangi. Sang Pemimpi adalah bagian novel tetralogi Andrea Hirata. (sumber: kompas.com)<br>', '2009-02-01', '14:31:58', '76laskarpelangi.jpg', 11),
(69, 'Gelombang Solidaritas untuk Palestina', 'Serangan Israel kepada Palestina yang terjadi mulai akhir Desember 2008 silam telah menewaskan hampir seribu nyawa manusia. Warga sipil yang kebanyakan perempuan dan anak-anak menjadi korban keganasan tentara Israel. Warga dunia pun marah. Saat ini, hampir setiap hari sejumlah unjuk rasa mengecam kebiadaban Israel terjadi di seluruh belahan dunia. Kejahatan Israel adalah kejahatan kemanusiaan dan sudah menjadi isu bersama umat manusia.<br><br>Kecaman tidak hanya datang dari umat Islam, tapi juga dari umat agama lain. Lihat saja sejumlah biksu yang tergabung dalam Perwakilan Umat Buddha Indonesia (Walubi), kemudian Persatuan Tionghoa Indonesia serta Dewan Pemuda Kristen Indonesia.<br><br>Mereka semua ikut berpatisipasi dalam aksi solidaritas untuk Palestina yang dilaksanakan di lapangan Monas, Jakarta Ahad (11/1/2009) lalu. Mereka mengutuk kebiadaban Israel. (sumber: sabili.co.id)<br>', '2009-01-31', '14:34:18', '11solidaritas.jpg', 11),
(78, 'Cristiano Ronaldo Pemain Sepakbola Terbaik 2008', 'Cristiano Ronaldo akhirnya terpilih sebagai Pemain Terbaik Dunia versi FIFA, mengalahkan Lionel Messi (Barcelona), dan Fernando Torres (Liverpool). Penetapan itu diumumkan di Zurich, Swiss, Senin atau Selasa (13/1) dini hari. Ronaldo menjadi pemain pertama dari Premier League yang menerima penghargaan itu. Sebelumnya, dia juga terpilih sebagai Pemain Terbaik Eropa (Ballon d''Or) dan baru saja dinobatkan sebagai Pemain Terbaik Dunia versi suporter.<br><br>Pemain Manchester United (MU) asal Portugal itu meraih 935 pemilih dari poling yang disebar ke seluruh dunia. Pemilihnya hanya dibatasi kapten tim dan pelatih. Hasil itu diumumkan oleh pemain legendaris Brasil, Pele.<br><br>Sementara itu, Lionel Messi berada di tempat kedua. Pemain Barcelona asal Argentina itu meraih 678 suara. Adapun striker Liverpool asal Spanyol, Fernando Torres, berada di tempat ketiga dengan 203 suara.<br><br>Musim lalu, Ronaldo memang tampil bagus. Dia mencetak 42 gol dan membawa Manchester United merebut gelar Premier League dan Liga Champions.<br><br>"Ini momen yang sangat indah buatku. Momen spesial dalam hidupku. Aku ingin mengatakan kepada ibu dan saudara perempuanku bahwa kembang api sudah saatnya disulut," kata Ronaldo seusai menerima penghargaan itu. (sumber: detiksport.com)<br>', '2009-02-02', '14:36:34', '92cristianoronaldo.jpg', 23),
(71, 'Ronaldo "CR7" Pecahkan Transfer Termahal', 'Cristiano Ronaldo segera menjadi pemain termahal dunia, menumbangkan rekor Zinedine Zidane. Agen Ronaldo menyebut bahwa kliennya terikat pra kontrak 91 juta poundsterling dengan Real Madrid. Dengan transfer senilai Rp 1,5 triliun itu, maka CR7 dipastikan akan menjadi pemain termahal dunia. Tapi, itu mungkin baru terealisasi musim depan alias musim panas nanti.<br><br>Sport melansir bahwa Pemain Terbaik Dunia 2008 itu terikat kontrak dengan Madrid untuk jangka panjang. Bahkan, juga disebutkan bahwa agen Ronaldo, Jorge Mendes, akan terkena klausul penalti (penalty clause) 20 juta euro (18 juta pounds) jika Ronaldo tak hadir di Santiago Bernabeu, musim depan.<br><br>Sebelumnya, pemain berusia 23 tahun ini dikabarkan juga terikat kontrak dengan mantan presiden Madrid, Florentino Perez. Ronaldo akan menjadi alat kampanye Perez dalam pemilihan presiden Madrid, pertengahan Juli 2009.<br><br>Rekor pemain termahal dunia kini masih dipegang Zinedine Zidane dengan 46 juta poundsterling pada 2001. Perez juga menjadi aktor di balik kedatangan maestro asal Prancis itu dari Juventus ke Madrid.<br><br>Demikian juga runner up pemain termahal dunia, Luis Figo. Perez membajaknya dari rival bebuyutan Barcelona pada 2000 dengan nilai 38,7 juta pounds. Saat itu, Figo juga jadi alat kampanye Perez. (sumber: vivanews.com)<br>', '2009-01-31', '14:41:25', '97cristiano-ronaldo.jpg', 31),
(72, 'Belajar Dari Krisis Amerika', 'Ibarat karena nila setitik, rusak susu sebelanga. Dan di kolam susu inilah tampaknya warga dunia tengah menunggu kapan giliran nila itu datang yang akan benar-benar melumpuhkan sendi perekonomian di negaranya masing-masing, tak terkecuali kita di Indonesia.<br><br>Dan kini kita paham bahwa kondisi yang cukup serius kali ini memang awalnya bermula dari krisis nasional di AS, yang kemudian menyebar dengan cepat ke seluruh dunia. Namun jelas bahwa ia bukanlah penyebab utamanya seperti yang dituding oleh sejumlah media (lihat ''Runtuhnya Pusat Kapitalisme'', Editorial Harian Radar Bogor, 27/09/08).<br><br>Yang menjadi benang merah dari rentetan krisis ini justru adalah penerapan globalisasi dimana roda perekonomian banyak negara di dunia digantungkan. Sebab dalam sistem ekonomi global yang tengah dipraktikkan banyak negara saat ini, krisis yang dialami suatu negara akan menular bak virus ke negara-negara lain, khususnya bila krisis itu bermula dari negara-negara maju dan yang punya otoritas dalam peta perkonomian dunia.<br><br>Meski belum memiliki definisi yang mapan, istilah globalisasi banyak dihubungkan dengan peningkatan keterkaitan dan ketergantungan antarbangsa dan antarmanusia di seluruh dunia dunia melalui perdagangan, investasi, perjalanan, budaya populer, dan bentuk-bentuk interaksi yang lain sehingga batas-batas suatu negara menjadi bias (wikipedia.com).<br><br>Di alam globalisasi inilah, kesalingbergantungan antara negara satu dengan negara lain terjalin begitu kuat. Dengan begitu, sebuah negara yang telah maju diharapkan akan merangsang perekonomian negara-negara yang sedang berkembang lewat sistem pasar bebas yang saling terhubung dan kompetitif. Tak heran bila globalisasi dipercaya akan mampu membawa kemaslahatan pada segenap umat manusia di dunia.<br><br>Sebuah niat yang kedengarannya cukup mulia memang. Dan sejak diterapkan pada era 80-an, globalisasi menjadi sistem ekonomi (mencakup juga aspek sosial, budaya, dan komunikasi) yang populer di banyak negara. Tak terkecuali bagi negara kita tercinta yang kala itu berada di bawah rezim Orde Baru.<br><br>Tapi dengan adanya krisis global ini, untuk pertama kalinya kita disadarkan, betapa sistem globalisasi yang tengah dipraktikkan kebanyakan negara saat ini, ternyata juga berpotensi membawa umat manusia pada krisis berkepanjangan. Ditambah lagi betapa globalisasi ekonomi dunia kian hari kita lihat semu dan banal, yakni di mana triliunan dollar AS diperjualbelikan dan dipermainkan di pasar modal, tetapi hanya sebagian saja diantaranya yang benar-benar menyentuh sektor riil.<br><br>Dengan kondisi kesalingterhubungan dan kesalingbergantungan inilah globalisasi ekonomi menciptakan budaya ekonomi sebagai jaringan terbuka (open network) yang rawan terhadap kemacetan di suatu titik jaringan dan serangan virus ke seluruh jaringan. Serangan virus (semisal kemacetan likuiditas) di sebuah titik jaringan (seperti AS) dengan cepat menjalar ke seluruh jejaring global tanpa ada yang tersisa.<br><br>Maka di titik ini pulalah kita sadar betapa Indonesia sebagai salah satu peserta yang turut serta dalam sistem ekonomi global, cukup rentan terkena dampak krisis ini.<br><br>Sejatinya, krisis global ini memang lebih banyak berpengaruh pada industri keuangan, khususnya pasar modal. Ruang gerak pasar modal itu sendiri belum meluas bagi usaha dan bisnis yang dijalankan bagi kebanyakan masyarakat Indonesia.<br><br>Bisa disimak bahwa roda perekonomian di Kota Bogor sendiri lebih banyak digerakkan oleh sektor riil dan usaha kecil menengah (UKM). Kebanyakan dari mereka menjalankan usaha yang tak memiliki persinggungan langsung dengan investor, juga dikerjakan oleh SDM dari dalam negeri sendiri.<br><br>Karenanya, kita selaku warga Bogor patut menjadikan peristiwa krisis global saat ini sebagai momentum dalam mendukung segenap pelaku bisnis dan UKM kota Bogor. Sebab, sejarah negeri ini telah membuktikan bahwa para pelaku bisnis dan UKM-lah yang mampu bertahan ketika krisis menerpa Indonesia di tahun 1998.<br><br>Dan kepada merekalah kita bisa berharap krisis global kali ini takkan mampir ke Indonesia. (sumber: http://prys3107.blogspot.com/)<br>', '2009-01-31', '14:48:09', '44amerika.jpg', 9),
(74, 'Google Chrome Susupi Microsoft', '<p>\r\nBrowser Microsoft, Internet Explorer (IE), bisa mendominasi karena tersedia secara default pada banyak komputer di pasaran. Google Chrome akan menggoyang dengan menyusup di lahan yang sama. Google rupanya sudah bersiap-siap menawarkan Google Chrome secara default pada komputer-komputer baru. \r\n</p>\r\n<p>\r\nPichai juga menjanjikan Chrome akan keluar dari versi Beta (uji coba) pada awal 2009. \r\nJika Google berhasil menyusupkan Chrome dalam lahan yang selama ini jadi &#39;mainan&#39; Microsoft, lanskap perang browser akan mengalami perubahan. \r\n</p>\r\n<p>\r\nSaat ini Microsoft masih mendominasi pada kisaran 70 persen lewat Internet Explorer-nya, sedangkan Firefox menguasai sekitar 20 persen. (sumber: <a href="http://detikinet.com" target="_blank">detikinet.com</a>)\r\n</p>\r\n', '2009-01-31', '13:34:25', '25chrome.jpg', 31),
(75, 'Krisis Ekonomi Amerika, Bukti Gagalnya Kapitalisme', '<p>\r\nPresiden Ekuador, Rafael Correa menilai krisis yang terjadi di Amerika menjadi bukti kegagalan sistem kapitalis dan periode Kapitalisme telah berakhir serta ekonomi Amerika sebagai pasar terbesar dunia telah dililit krisis. (Kantor Berita Fars Prensa Latina Kuba).\r\n</p>\r\n<p>\r\nCorrea menambahkan, apa yang terjadi di Amerika tidak terbatas pada krisis keuangan, namun bukti kebuntuan sebuah sistem yang dibangun tanpa dicermati secara serius. \r\n</p>\r\n<p>\r\nMenurut Correa, solusi krisis sistem keuangan Amerika tidak akan bisa selesai dengan menyuntikkan dana 700 miliar dolar kepada bank-bank yang telah bangkrut, namun yang lebih penting lagi adalah Amerika harus melakukan perubahan fundamental. (sumber: hidayatullah.com)\r\n</p>\r\n', '2009-01-31', '14:13:52', '54RafelKarera.jpg', 19),
(79, 'Ahmadinejad: Gaza Akan Jadi Kuburan Israel', 'Iran dan Israel tampaknya tidak akan pernah melakukan genjatan senjata. Presiden Iran Mahmoud Ahmadinejad melontarkan kata-kata serangan terhadap Israel dengan menyebut negara Yahudi itu akan segera lenyap dari bumi. "<span style="font-weight: bold; font-style: italic;">Kejahatan yang dilakukan rejim Zionis (Israel) terjadi karena mereka sadar sudah sampai di akhir dan segera lenyap dari muka bumi</span>," kata Ahmadinejad dalam pawai anti-Israel yang berlangsung di Teheran, seperti dilaporkan kantor berita Mehr dan dikutip DPA, Sabtu (13/12).<br><br>Dia mengatakan Israel sudah kehilangan arah dan kian sadar bahwa kelompok negara-negara kuat makin ragu untuk menunjukkan dukungan untuk negara Yahudi itu.<br><br>Ahmadinejad juga mengatakan bahwa kejahatan Israel di Gaza bertujuan mengganti pemimpin politik di wilayah itu agar sesuai dengan kepentingan politik Israel. (sumber: <a target="_blank" title="Situs Berita Inilah.com" href="http://inilah.com">inilah.com</a>)<br>', '2009-02-02', '14:23:39', '22ahmadinejad.jpg', 84),
(65, 'Michael Heart "Song for Gaza"', 'Banyak cara untuk men-support perjuangan rakyat Palestina di Gaza, salah satunya dengan lagu. Seorang penyanyi di Los Angeles Amerika Serikat - Michael Heart yang bernama asli Annas Allaf kelahiran Syiria, membuat sebuah lagu khusus yang dia tujukan untuk rakyat Gaza yang sampai saat ini masih jadi sasaran pembantaian oleh Zionis Israel.<br><br>Sejak dia merilis lagu yang berjudul "We will not go down" (Song for Gaza), lagu tersebut mendapat banyak respon, berupa komentar dukungan, sampai ia sendiri kewalahan menjawab dan membalas berbagai email yang masuk.<br><br>Michael Heart menggambarkan kondisi rakyat Gaza akan gempuran Zionis Israel dalam lagunya itu membuat kita merasa tersindir dan sedih akan nasib rakyat Gaza. Walaupun lagu itu baru di rilis, namun telah ratusan ribu orang melihatnya di youtube dan mendownload MP3 nya.<br><br>Awalnya dia berencana dengan menjual CD lagu MP3 nya itu dan hasil penjualannya akan dia donasikan untuk kepentingan amal kemanusiaan untuk penduduk Gaza, tapi karena dia merasa sulit kalau harus sendiri mendonasikan hasil penjualan CD MP3 nya, akhirnya dia memutuskan semua orang bisa mendownload gratis lagu tersebut. Dan dia berharap, setelah mendengarkan lagu itu, orang-orang akan tergerak hatinya untuk membantu rakyat Palestina di Gaza dengan mendonasikan uangnya ke lembaga-lembaga kemanusiaan yang ada atau organisasi yang didedikasikan untuk meringankan penderitaan rakyat Palestina.<br><br>Sebagai musisi Michael Heart sangat berterima kasih atas dukungan yang diberikan kepada dia atas lagu tersebut. Dan dia berharap setiap orang yang masih mempunyai hati nurani, mendukung perjuangan rakyat Palestina dan membantu mereka walau hanya dengan berupa doa.<br><br><br><span style="font-weight: bold;">WE WILL NOT GO DOWN (Song for Gaza)</span><br style="font-weight: bold;"><br style="font-style: italic;"><span style="font-style: italic;">A blinding flash of white light</span><br style="font-style: italic;"><span style="font-style: italic;">Lit up the sky over Gaza tonight</span><br style="font-style: italic;"><span style="font-style: italic;">People running for cover</span><br style="font-style: italic;"><span style="font-style: italic;">Not knowing whether they''re dead or alive</span><br style="font-style: italic;"><br style="font-style: italic;"><span style="font-style: italic;">They came with their tanks and their planes</span><br style="font-style: italic;"><span style="font-style: italic;">With ravaging fiery flames</span><br style="font-style: italic;"><span style="font-style: italic;">And nothing remains</span><br style="font-style: italic;"><span style="font-style: italic;">Just a voice rising up in the smoky haze</span><br style="font-style: italic;"><br style="font-style: italic;"><span style="font-style: italic;">We will not go down</span><br style="font-style: italic;"><span style="font-style: italic;">In the night, without a fight</span><br style="font-style: italic;"><span style="font-style: italic;">You can burn up our mosques and our homes and our schools</span><br style="font-style: italic;"><span style="font-style: italic;">But our spirit will never die</span><br style="font-style: italic;"><span style="font-style: italic;">We will not go down</span><br style="font-style: italic;"><span style="font-style: italic;">In Gaza tonight</span><br style="font-style: italic;"><br style="font-style: italic;"><span style="font-style: italic;">Women and children alike</span><br style="font-style: italic;"><span style="font-style: italic;">Murdered and massacred night after night</span><br style="font-style: italic;"><span style="font-style: italic;">While the so-called leaders of countries afar</span><br style="font-style: italic;"><span style="font-style: italic;">Debated on who''s wrong or right</span><br style="font-style: italic;"><br style="font-style: italic;"><span style="font-style: italic;">But their powerless words were in vain</span><br style="font-style: italic;"><span style="font-style: italic;">And the bombs fell down like acid rain</span><br style="font-style: italic;"><span style="font-style: italic;">But through the tears and the blood and the pain</span><br style="font-style: italic;"><span style="font-style: italic;">You can still hear that voice through the smoky haze</span><br style="font-style: italic;"><br style="font-style: italic;"><span style="font-style: italic;">We will not go down</span><br style="font-style: italic;"><span style="font-style: italic;">In the night, without a fight</span><br style="font-style: italic;"><span style="font-style: italic;">You can burn up our mosques and our homes and our schools</span><br style="font-style: italic;"><span style="font-style: italic;">But our spirit will never die</span><br style="font-style: italic;"><span style="font-style: italic;">We will not go down</span><br style="font-style: italic;"><span style="font-style: italic;">In Gaza tonight </span><br><br>(sumber: detik.com)<br>', '2009-01-31', '14:26:40', '24michaelheart.jpg', 24),
(66, 'Demo Kecam Israel Warnai Ibukota', 'Aksi unjuk rasa menentang agresi militer Israel ke Jalur Gaza, Palestina kembali mewarnai Jakarta. Unjuk rasa kali ini dilakukan oleh Ormas Islam Hizbut Thahrir di kawasan Silang Monas, Jakarta. Sejak Minggu (4/1) pagi, para pengunjuk rasa nampak berbondong-bondong membawa karton besar bertuliskan ''Save Palestine'' dan foto anak-anak serta perempuan Palestina yang menjadi korban tak berdosa dari serangan biadab militer Israel.<br><br>Kepada warga Jakarta yang berolahraga di sekitar kawasan Monas, para pengunjuk rasa juga mengedarkan kotak sumbangan untuk didonasikan kepada korban warga Palestina.<br><br>Aksi unjuk rasa dan banyaknya warga Jakarta yang rutin berolahraga di kawasan Silang Monas setiap Minggu pagi, membuat kawasan itu cukup padat untuk dilalui kendaraan bermotor.<br><br>Serangan udara Israel yang dimulai pada 27 Desember 2008 sudah terjadi selama sepekan di Jalur Gaza dan menewaskan lebih 420 orang.<br><br>Meski mendapat kutukan keras dari dunia Internasional, termasuk Indonesia, Israel sampai saat ini belum menunjukkan tanda-tanda akan menghentikan aksi militernya. (sumber: inilah.com)<br>', '2009-01-31', '14:29:16', '32demo.jpg', 23),
(67, 'Ana Ivanovic Dinobatkan Sebagai Ratu Tenis 2008', 'Ana Ivanovic, dara kelahiran Belgrade pada tanggal 6 November 1987 sudah mulai bermain tenis sejak umur 5 tahun sesudah menonton Monica Seles di TV, mengingat nomor telpon sekolah tenis lokal dan memohon kepada orang tuanya untuk mengajak pergi ke sekolah itu. Kemudian di acara ulang tahunnnya yang ke-5, orang tuanya memberi hadiah berupa raket tenis dan sejak itu dia mulai jatuh cinta dengan dunia tenis. Kemudian Ana mulai berlatih tenis secara intens dengan Scott Byrnes pada bulan juli 2006.<br><br>Dragana, ibunya adalah seorang pengacara, sedangkan Miroslav bapaknya adalah seorang pebisnis, Milos kakaknya adalah seorang pemain basket dan seluruh keluarganya menyukai olahraga, tetapi tidak ada yang menyukai tenis seperti ana.<br><br>Senjata utamanya di tenis adalah pukulan forehand-nya, dan dia bisa main di segala jenis lapangan. Hobinya adalah menonton film di bioskop atau menonton DVD di rumah. Ana juga suka membaca, khususnya tentang Mitologi dan Sejarah Yunani. Ana juga senang sekali mendengarkan musik.<br><br>Pada tahun 2008 ini, setelah menjuarai Roland Garros prancis dengan mengalahkan Dinara safina dari rusia di final, maka saat ini peringkat Ana Ivanovic naik menjadi peringkat 1 dunia untuk petenis putri.<br>', '2009-01-31', '14:30:48', '20anaivanovic.jpg', 15),
(73, 'Maria Kirilenko, Petenis Terseksi Versi WTA', 'Pesona kecantikan Maria Sharapova dan Ana Ivanovic sepertinya sudah mendapat saingan baru. Tidak jauh-jauh, nama Maria Kirilenko tiba-tiba menyeruak di daftar petenis terseksi pilihan responden WTA. Artinya, Maria sukses merengkuh gelar yang musim lalu diraih Maria Sharapova.<br><br>Setengah dari 7 ribu responden yang masuk ke WTA menyebut, kalau Maria adalah sosok petenis ideal dan paling proporsional di level bentuk tubuh. Meski hanya berperingkat 18 dunia, namun pesonanya di atas lapangan tenis menjadi daya tarik tersendiri.<br><br>"Tubuhnya sangat indah, siluetnya membuat setiap pria pasti penasaran ingin melihat lebih dekat. Yang jelas, ia memiliki kepribadian baik yang makin menyempurnakan pesona fisiknya," tulis seorang responden. Di kalangan petenis putri sendiri, sudah lama Maria menjadi saingan berat Masha dan Ana ivanovic.<br><br>Di situs pribadinya, petenis bernama asli Maria Yuryevna Kirilenko ini mengaku selalu menjaga proporsi tubuh dengan senam dan renang, selain tentu berlatih fisik tenis. "Olahraga adalah cermin hidupku, jika tak olahraga sehari saja, kadang membuat tubuhku terasa lemas dan tak bergairah," ujar Maria.&nbsp; (persda network/bud)<br><br>Meksi bersaing di lapangan dan dunia mode, namun ternyata sosok Maria Kirilenko adalah sobat sejati Maria Sharapova. Bukan hanya karena sama-sama berasal dari Rusia, namun gaya hidup mereka berdualah yang membuat Maria-Masha banyak memiliki kecocokan.<br>Selain suka fotografi, mereka berdua juga memiliki hobi berbelanja, terutama fashion dan perhiasan. Bukan untuk pamer memang, tapi mereka melakukan itu untuk tabungan dan investasi.<br><br>Di beberapa turnamen, Masha dan Maria memang tampak bersama tatkala berada di luar lapangan. Mereka biasanya menyingkir dari rombongan pemain lain, dan memilih berburu barang kesukaan mereka dengan menyisir bagian kota tempat mereka tengah bertanding. "Aku dan Masha seperti kakak beradik, bagiku dia lebih dari sekedar sahabat, dia begitu dewasa, apalagi saat kami berdua saling curhat," sebut Maria, di tennisnews. <br><br>Daftar petenis terseksi WTA:<br><ol><li>Maria Kirilenko (Russia)</li><li>Maria Sharapova (Russia)</li><li>Ana Ivanovic (Serbian)</li><li>Caroline Wozniacki (Danish)</li><li>Nicole Vaidisova (Czech)</li><li>Sania Mirza (Indian)</li><li>Ashley Harkleroad (American)</li><li>Gisela Dulko (Argentinian)</li><li>Samantha Stosur (Australian)<br></li></ol>', '2009-01-31', '15:01:49', '14mariakirilenko.jpg', 41),
(77, 'Sharapova, Petenis Wanita Berpenghasilan Tertinggi', 'Petenis asal Rusia, Maria Sharapova dengan penghasilan 26 juta dolar AS merupakan petenis wanita berpenghasilan tertinggi. Ia pernah menempati peringkat satu dunia, pasca mundurnya Justine Henin. Ia juga memiliki prestasi dengan menjuarai turnamen grand slam Australia Terbuka dan AS Terbuka. Namun, sebagian besar penghasilannya didapat dari kontrak iklannya bersama Pepsi, Colgate-Palmolive, Nike dan Motorola.<br><br>Berikutnya disusul Williams bersaudara dari Amerika, yaitu Serena Williams dengan penghasilan 14 juta dolar AS. Ia meraih tiga gelar juara tiap tahunnya semenjak tahun 2005. Ia juga merupakan model dari produk Hawlett-Packard, Nike, dan Kraft. Sedangkan kakak kandungnya, yaitu Venus Williams berpenghasilan 13 juta dolar AS. Ia mengalahkan adiknya di final Wimbledon tahun 2008. Ia memiliki dan menjalankan sendiri usaha fashion Eleven.<br><br>Di peringkat ke empat dan kelima adalah petenis Belgia yaitu Justine Henin dengan penghasilan 12,5 juta dolar AS. Dan petenis asal Serbia, yaitu Ana Ivanovic dengan penghasilan 6,5 juta dolar AS.<br>', '2009-02-01', '19:58:16', '89sharapova.jpg', 21),
(68, 'Roger Federer, Petenis Legenda Abad Ini', 'Siapa yang tak kenal dengan Roger Federer saat ini? Masih muda, ganteng, namun sudah jadi legenda. Bayangkan, dalam usia belum menginjak 26 tahun, ia sudah memecahkan rekor bertahan sebagai peringkat pertama dunia tenis selama 161 pekan berturut-turut. Ia memecahkan rekor Jimmy Connor yang sudah bertahan puluhan tahun. <br><br>Itu baru satu rekor. Sebelumnya, ia juga mendapat penghargaan Bagel Award, yakni penghargaan sebagai petenis paling banyak memenangkan set tenis dengan angka sempurna 6-0. "Saya hanya berusaha melakukan yang terbaik dan tidak berhenti memperbaiki kesalahan-kesalahan saya,"sebut Federer merendah tentang prestasinya itu.<br><br>Dengan kerendahhatian dan semangat untuk terus memperbaiki diri, pria keturunan campuran Swiss, German, dan Afrika Selatan ini sepertinya akan terus mengukir prestasi. Sebab, mengingat usia yang masih muda dan jarak nilai ATP dengan peringkat kedua dunia Rafael Nadal, cukup jauh, ia akan bisa terus bertahan di rangking satu dunia. Apalagi jika ia nantinya bisa memenangkan satu-satunya gelar tenis Grand Slam yang belum diraih, Perancis Terbuka. Ia akan jadi satu-satunya petenis pria yang bisa mengawinkan semua gelar tenis Grand Slam.<br><br>Roger Federer memang sepertinya terlahir untuk jadi legenda. Bahkan, menurut pengakuannya, sejak kecil ia sudah disebut banyak orang punya bakat gemilang di bidang olahraga. Tapi, menurut dirinya, bukan bakat yang membuatnya seperti sekarang. Kerja keras, ketekunan berlatih, dan keuletan di lapangan lah yang membuat dia bisa jadi juara sejati. "Saya terus berlatih untuk meningkatkan teknik permainan saya dan menambah kekuatan saya. Proses ini saya jalani sampai hari ini dan bahkan makin saya tingkatkan sejak saya jadi juara. Ini saya lakukan karena saya yakin masih banyak perbaikan yang harus terus dilakukan."<br><br>Dengan tekad untuk terus melakukan perbaikan itu, Roger Federer terus meretas jalan untuk mengukir rekor-rekor lainnya. Namun, semua rekor dan kemenangan yang diperolehnya, ternyata bukan hanya untuk kebanggaan dirinya. Melalui sebuah yayasan yang diberi nama seperti dirinya, Roger Federer Foundation, ia membantu anak-anak kurang beruntung di dunia terutama di Afrika Selatan. Sebagian hadiah yang diperoleh dari kemenangannya di kejuaraan tenis, digunakan untuk membantu anak-anak itu. Ia juga berperan banyak saat terjadi tsunami akhir tahun 2005. Saat itu, ia terpilih menjadi duta UNICEF, untuk membantu anak-anak yang jadi korban tsunami di Tamil Nadu, India. Ia juga berjanji untuk mengukir lebih banyak kemenangan guna mengumpulkan lebih banyak dana untuk yayasannya. Ia juga merelakan beberapa raketnya untuk dilelang guna disumbangkan melalui UNICEF. Roger Federer telah membuktikan, dengan kerja keras, semangat pantang menyerah, tekad kuat, dan kepedulian terhadap sesama, telah menjadikannya sebagai juara sejati.<br><br>Dari kisah sukses Roger Federer ini, kita dapat mengambil pelajaran bahwa dengan kerja keras disertai semangat pantang menyerahlah kita bisa mewujudkan cita-cita. Selain itu, kepedulian kepada sesama juga selayaknya dapat mendorong semangat kita untuk terus mengukir prestasi. (sumber: andriewongso.com)<br>', '2009-01-31', '18:59:14', '33federer.jpg', 16),
(70, 'Kisah Sukses Google', 'Dalam daftar orang terkaya di Amerika baru-baru ini, terselip dua nama yang cukup fenomenal. Masih muda, usianya baru di awal 30-an, namun kekayaannya mencapai miliaran dolar. Nama kedua orang itu adalah Larry Page dan Sergey Brin. Mereka adalah pendiri Google, situs pencari data di internet paling terkenal saat ini.<br><br>Terlepas dari jumlah kekayaan mereka, ada beberapa hal yang perlu dicontoh dari kisah sukses mereka. Satu hal yang pertama, yang disebut Sergey Brin, yang kini menjabat sebagai Presiden Teknologi Google, yakni tentang kekuatan kesederhanaan. Menurutnya, simplicity web adalah hal yang disukai penjelajah internet. Dan, Google berhasil karena menggunakan filosofi tersebut, menghadirkan web yang bukan saja mudah untuk mencari informasi, namun juga menyenangkan orang.<br><br>Kunci sukses kedua adalah integritas mereka dalam mewujudkan impiannya. Mereka rela drop out dari program doktor mereka di Stanford University untuk mengembangkan google. Mereka pun pada awalnya tidak mencari keuntungan dari proyek tersebut. Malah, kedua orang itu berangkat dari sebuah ide sederhana. Yakni, bagaimana membantu banyak orang untuk mempermudah mencari sumber informasi dan data di dunia maya. Mereka sangat yakin, ide mereka akan sangat berguna bagi banyak orang untuk mempermudah mencari data apa saja di internet.<br><br>Kunci sukses lainnya yaitu mereka tidak melupakan jasa orang-orang yang mendukung kesuksesan mereka. Larry dan Sergey sangat memerhatikan kesejahteraan SDM di Google. Kantornya yang diberi nama Googleplex dinobatkan sebagai tempat bekerja terbaik di Amerika tahun 2007 oleh majalah Fortune. Di sana suasananya sangat kekeluargaan, ada makanan gratis tiga kali sehari, ada tempat perawatan bagi bayi ibu muda, bahkan sampai kursi pijat elektronik pun tersedia. Mereka sadar, di balik sukses inovasi yang dilakukan Google, ada banyak doktor matematika dan lulusan terbaik dari berbagai universitas yang membantu mereka.<br><br>Larry dan Sergey memang tak pernah menduga Google akan sesukses sekarang. Kedua orang yang terlahir dari keluarga ilmuwan â€“ ayah Sergey adalah doktor matematika, sedangkan Larry adalah putra almarhum doktor pertama komputer di Amerika â€“ ini memang hanya berangkat dari sebuah masalah sederhana. Mereka berusaha memecahkan masalah tersebut, dan berbagi dengan orang lain. Namun, justru dengan kesederhanaan dan integritas mereka, mampu membuat Google saat ini menjadi mesin pencari terdepan, dikunjungi lebih dari 300 juta orang perhari. Diterjemahkan dalam 88 bahasa dengan nilai saham mencapai lebih dari 500 dolar AS per lembar, membuat sebuah kesederhanaan menjelma menjadi kekuatan yang luar biasa.<br><br>Sebuah niat mulia, meski sesederhana apapun, jika dilandasi kerja keras dan integritas yang tinggi, akan menghasilkan sesuatu yang istimewa. Hal tersebut nampak dari contoh kisah sukses Larry Page dan Sergey Brin di atas. Google yang mereka dirikan terbukti telah membantu banyak orang untuk bisa mendapatkan apa saja dari internet. Dan kini, mereka pun mendapatkan imbalan yang bahkan tak diduga mereka sebelumnya. Kesuksesan sejati memang akan terasa saat kita bisa berbagi. Dan, Larry serta Sergey membuktikannya sendiri. (sumber: andriewongso.com)<br>', '2009-01-25', '20:26:26', '73google.jpg', 7),
(85, 'Windows 7 Gantikan Windows Vista', '<p>\r\nMicrosoft  ingin memudahkan rencana para administrator komputer yang akan bermigrasi ke Windows 7, namun sebuah tulisan di blog salam satu anggota tim Windows 7 berkata sebaliknya.\r\nSkenario uji coba terbaru menunjukkan, sebagian besar pengguna, proses upgrading akan menyulitkan, mengambil waktu kira-kira 30 menit. \r\n</p>\r\n<p>\r\nProsentasi terbesar pengguna menyebut, migrasi butuh waktu hingga 21 jam.\r\nSalah satu anggota tim Windows dari Microsoft, Chris Hernandez, mengungkap hasi pengetesan timnya dengan berbagai merek komputer dan konfigurasi tipikal pengguna lewat simulasi pada tingkatan berbeda dari proses migrasi Vista ke Windwos pada Jumat akhir pekan lalu.\r\n</p>\r\n<p>\r\nTujuan simulasi untuk memastikan apakah upgrade dari Vista Service Pack (SP) 1 ke Windows 7, dalam lima persen percobaan utama, lebih cepat ketimbang upgrade dari Vista SP1 ke Vista SP2, ujar Chris.\r\nProses dari Vista SP1 ke Vista SP2 dipilih karena itu opsi instalasi paling umum digunakan Microsoft Product Support Services, yakni skenario repair (perbaikan ulang) di mana prosedur yang paling dianjurkan adalah melakukan re-instalasi sistem operasi (OS) yang sudah ada di komputer saat itu. \r\n</p>\r\n<p>\r\nChris menampilkan hasil tes dalam blognya.\r\nTim mengetes konfigurasi komputer khusus hadware, merentang dari kategori hardware low-end (spesifikasi rendah), mid-range (spesifikasi menengah) dan high-end (spesifikasi atas). Kategori itu berlawanan dengan skenario pengguna pada umumnya yang berbasis pertanyaan seperti, berapa besar set data yang dibutuhkan pengguna dan bagaimana macam aplikasi tersebut diinstall.\r\n</p>\r\n<p>\r\nUntuk kategori komputer spesifikasi atas, Chris dan timnya mendefinisikan komputer dengan sistem operasi 32 bit dan memiliki CPU berprosesor Inter Core 2 Quad, yang bejalan di 2,4 GHz, memori 4GB dan hardisk 1 Terabyte .\r\nSementara, pengguna umumnya memiliki data sebesar 125 GB yang terikat dalam format dokumen, musik dan gambar dengan 40 aplikasi yang diinstal di komputer mereka.\r\nKinerja upgrade Vista SP1 ke Windows 7 pada hardware spesifikasi atas dengan konfigurasi pemilik pengguna kelas berat, membutuhkan 160 menit, atau sekitar 2,7 jam. \r\n</p>\r\n<p>\r\nSebagai perbandingan, upgrade repair (perbaikan) dari Vista SP1 ke Vista SP1 dengan hadware yang sama dan penggunaan bera membutuhkan 176 menit, atau 2,9 jam.\r\nSkenario terburuk muncul pada konfigurasi hadware kelas menengah, yakni CPU 32 bit namun dengan software dan konfigurasi &quot;pengguna super&quot;. Proses upgrading akan butuh waktu hingga 1.220 menit alias 20,3 jam. Padahal yang dianggap hadware kelas menengah, memiliki spesifikasi setara memory 2GB RAM, prosesor dual core, Athlon 64 X2, pada 2,6GHz dan hardisk 1 Terabyte.\r\n</p>\r\n<p>\r\nMereka yang dianggap pengguna super, memiliki profil lebih sadis dalam istilah penggunaan data, ketimbang pengguna kelas berat pada umumnya. Sebagai contoh, tim penguji menyebut pengguna super memiliki 650 GB data dan 40 aplikasi lebih yang terinstal dalam komputer mereka.\r\nLalu pada kelas rendah, pengguna medium, dengan 70 GB data dan 20 aplikasi, dengan memori sekitar 1 GB, prosesor 64 bit, AMD Athlon pada kecepatan 2,2 GHz, bakal butuh waktu bermigrasi sekitar 175 menit. Hardware yang lebih bertenaga, secara umum membutuhkan waktu instalasi lebih singkat.\r\n</p>\r\n<p>\r\nMicrosoft tidak selalu bisa mencapai target lima persen tujuan tim Chris yang telah dijanjikan. Dalam satu contoh, instalasi bersih (instalasi pertama pada komputer baru tanpa OS) Windows 7 pada hardware spesifikasi menengah membutuhkan 30 menit sementara instalasi bersih Vista SP1 butuhk 31 menit. Hanya saja, secara keseluruhan, tidak ada instalasi Windows 7 yang lebih lambat dibandingkan Vista.\r\n</p>\r\n<p>\r\nPertanyaan tersisa, apakah para toko dan ritel software akan mendengar rayuan Microsoft dan memutuskan hijrah ke Windows 7 lebih cepat? Tradisi yang berlaku, ritel IT akan cenderung menunggu Service Pack I sebelum mendatangkan versi terbaru Windows.\r\nWaktu yang akan menjadi sumber menentukan apakah kalangan profesional IT akan berpindah, sehingga Vista tak lagi menarik bagi ritel dan toko software. Jadi kehijrahan mereka ke Windows 7 dengan segera, menandakan pula, apakah para profesional IT suka dengan hasil pengujian waktu instal yang dilakukan Chris Hernandez.  internetnews/itz.\r\n</p>\r\n', '2009-10-25', '07:25:22', '19windows7.jpg', 18),
(92, 'Pemilik Facebook akan Dibuat Filmnya', '<p>\r\nSutradara David Fincher nampak jeli melihat peluang di tengah booming fenomena Facebook. Fincher akan menghadirkan sebuah film yang menceritakan tentang Mark Zuckerberg dan Facebook bagi para pencinta film dan Facebook tentunya.\r\n</p>\r\n<p>\r\nFincher mengaku rencana pembuatan film ini masih dinegosiasikan dengan pihak Zuckerberg. Dia hanya menyebutkan, filmya akan fokus menceritakan Mark Zuckerberg yang awalnya merancang Facebook sebatas untuk keperluan mahasiswa Universitas Harvard.\r\n</p>\r\n<p>\r\nFilm ini memaparkan bagaimana setelah itu Facebook kemudian berkembang menjadi fenomena yang mendunia sejak diluncurkan pada 2004.\r\n</p>\r\n<p>\r\nDalam penggarapan film ini, Fincher mengajak serta orang-orang kompeten di bidang film. Antara lain Aaron Sorkin, yang merupakan penulis naskah acara serial televisi ternama The West Wing.\r\n</p>\r\n<p>\r\nSementara itu, Columbia Pictures yang menamai film ini &quot;The Social Network&quot; dipercaya untuk memulai produksi film pada akhir tahun ini.\r\n</p>\r\n<p>\r\nSebagian orang menilai kehadiran film ini nantinya akan mengorek kembali kasus lama dimana tiga teman Zuckerberg, Cameron dan Tyler Winklevoss serta Divya Narendra mengklaim Zuckerberg telah mencuri ide mereka untuk membuat Facebook.\r\n</p>\r\n<p>\r\nPada saat Zuckerberg meluncurkan Facebook, mereka menuntut perkara atas Zuckerberg. Awal tahun ini, pengadilan AS memutuskan Facebook harus membayar USD65 juta untuk melunasi perkara ini.\r\n</p>\r\n', '2009-10-25', '07:36:47', '17mark_zuckerberg.jpg', 18),
(90, 'Ferrari 458 Polesan Teknologi Jepang', '<p>\r\nBarangkali hanya Jepang (diluar Italia) yang berani memoles bodi mobil dari Ferrari, sekaligus mengumumkan hasilnya kepada publik. Seperti dilakukan rumah modifikasi ASI terhadap Ferrari 458 yang oleh pabrikannya di Italia baru di launching.\r\n</p>\r\n<p>\r\nASI dengan keberaniannya menggarap proyek berisiko tinggi. Beberapa mobil berharga miliaran rupiah pernah digarap dan membuat tampilan mobil lebih sporty dan tambah dinamis dari versi standar.\r\n</p>\r\n<p>\r\nSebut saja, Bentley Continental GT (yang diberi julukan The ASI Tetsu GTR) dan Ferrari 430. Bahkan Ferrari milik seorang pengusaha muda di Indonesia pernah juga dimodifikasi (body) di Jepang pada 2007.\r\n</p>\r\n<p>\r\nCEO ASI Satoshi Kondo menjelaskan, bahwa tim rekayasanya telah bekerja keras memproduksi aerokit untuk Ferrari 458. ASI, katanya sengaja mengeluarkan sketsa dari hasil kerja mereka dengan terus melakukan finalisasi prototype yang ada, dan menghindari pencurian desain.\r\n</p>\r\n<p>\r\nSentuhan pada bagian depan dari kuda jingkrak menjadi salah satu yang menonjol. Di antaranya moncong yang baru, lubang udara lebih besar, dan dilanjutkan pada bagian roda. Dari sketsa gambar tampak terpasang sayap baru di bagian belakang.\r\n</p>\r\n<p>\r\nPaket body kit dari ASI mempertegas tampilan Ferrari sebagai hasil kawin silang dari gaya tuner Jepang dengan kendaraan eksotis khas Italia. ASI mengklaim, adanya perubahan dan penambahan pada bodi tidak mengurangi performa standar. Bahkan bobot kendaraan lebih ringan dari asli. (sumber: kompas.com)\r\n</p>\r\n', '2009-10-25', '07:44:05', '4ferrari458.jpg', 4),
(86, 'Program 100 Hari Menkominfo Tifatul', '<p>\r\nBelum juga resmi diumumkan masuk jajaran kabinet, sejumlah calon menteri sudah berani membeberkan programnya. Salah satunya, Tifatul Sembiring. Tifatul disebut-sebut sebagai calon kuat Menkominfo (Menteri Komunikasi dan Informasi).\r\n</p>\r\n<p>\r\nApa saja program Tifatul? &quot;100 Hari pertama? Kita targetkan sampai 2014 itu ada 10 ribu desa komputer. Presiden menargetkan tiga bulan ini ada 100 desa komputer harus tercapai,&quot; kata Tifatul di Gedung MPR/DPR, Jakarta, Selasa 20 Oktober 2009.\r\n</p>\r\n<p>\r\nKomputer-komputer ini, kata dia, bisa dimasukkan ke lembaga pendidikan untuk meningkatkan sumber daya manusia. Bagaimana SDM Indonesia bisa masuk ke bisnis supaya Indonesia bisa bersaing dengan negara-negara lain. Selain itu juga untuk meningkatkan e-goverment untuk meminimalisir korupsi, kolusi, kolusi dan nepotisme.\r\n</p>\r\n<p>\r\nDengan e-goverment, kata dia, maka nantinya semua urusan menjadi less paper. Artinya pegawai di tingkat pemda dan kecamatan, tidak lagi menerima uang tunai. &quot;Tapi cukup menerima resi, sehingga sogok menyogok bisa diminimalisir,&quot; kata dia.\r\n</p>\r\n<p>\r\nTifatul sendiri mengaku tidak begitu asing dengan dunia Kominfo karena latar belakang pendidikannya cukup mendukung. Gelar sarjana strata satunya di bidang Informatika dan Komunikasi. Ia juga mengaju pernah bekerja selama delapan tahun di sistem informatika dan komunikasi PT Perusahaan Listrik Negara.\r\n</p>\r\n<p>\r\nSementara strata duanya di bidang politik internasional di Islamabad, Pakistan. &quot;Itu saja sih, pinter ya belum, diupayakan sesuai,&quot; kata dia.\r\n</p>\r\n<p>\r\nNamun ia berharap bisa menembus tantangan Kominfo ke depan, yakni perbedaan kemudahan akses di kota besar dan desa. Selain itu juga soal infrastruktur yang masih lemah. Masalah lain, kurangnya tayangan edukatif di bidang informasi. &quot;Dalam satu riset dikatakan 10 dari 75 tayangan di TV, radio masih bermasalah,&quot; kata dia.\r\n</p>\r\n<p>\r\nDia menambahkan, pelayanan informasi di Indonesia juga masih  lemah. Karena itu ia akan mengusahakan peningkatan layanan informasi ini. (Sumber: vivanews.com)\r\n</p>\r\n', '2009-10-25', '07:49:46', '27tifatul_sembiring.jpg', 16),
(93, 'Dalam Dua Pekan, KCB 2 Ditonton 1,5 Juta Penonton', '<p>\r\nFilm Ketika Cinta Bertasbih (KCB) 2 diyakini bakal mereguk sukses seperti sekuel pertamanya Sejak diputar perdana tanggal 17 September lalu atau selama 15 hari, film garapan SinemArt telah disaksikan 1,5 juta penonton. \r\n</p>\r\n<p>\r\nRekor yang sama juga dialami KCB 1. &quot;Pada pemutaran KCB 1 kami bisa memecah rekor pemutaran film di Indonesia, yaitu mendapat penonton sebanyak 100.000 perhari,&quot; ungkap Frans dari SinemArt saat promo film KCB 2 di Royal Plaza, Minggu (4/10).\r\n</p>\r\n<p>\r\nPihak SinemArt berharap KCB 2 bisa meraih prestasi minimal sama dengan KCB 1 dengan total 3 juta penonton. Untuk mencapai target tersebut, pihak SinemArt tak henti melakukan serangkaian promo di sejumlah kota di Tanah Air maupun di mancanegara.\r\n</p>\r\n<p>\r\n&quot;Hari ini (Minggu, 4/10), Kholidi (Kholidi Asadil Alam, pemeran Azzam) dan Oki (Oki Setiana Dewi pemeran Anna) ke Hongkong untuk promo di sana,&quot; imbuh Frans. Pekan depan (10-12 Oktober 2009), giliran Meyda Sefira berangkat ke Makau untuk kegiatan yang sama.\r\n</p>\r\n<p>\r\nFilm besutan sutradara Chaerul Umam ini juga dijadwalkan diputar di Aceh pada tanggal 11-12 Oktober mendatang. Menurut Frans, pemutaran KCB 1 di kota yang dikenal dengan sebutan Serambi Mekkah ini ditonton 8.000 orang.\r\n</p>\r\n<p>\r\nPadahal di kota tersebut sama sekali tidak ada gedung bioskop. Karena itu kru SinemArt terpaksa mengusung peralatan khusus dari Jakarta dan memutar di sebuah gedung khusus selama dua hari dalam tujuh kali show.\r\n</p>\r\n<p>\r\nBertutur tentang kesan berperan di KCB 2, Kholidi beberapa waktu lalu mengaku paling terkesan dengan adegan kecelakaan saat membonceng Bu&#39;e (Ninik L Karim). Karena ketika sepeda motornya terjatuh dia harus teriak memanggil ibundanya. &quot;Bu&#39;eee! Wah itu lumayan sulit,&quot; ungkap Kholidi.\r\n</p>\r\n<p>\r\nAdegan lain yang cukup berkesan adalah ketika pria asal Pasuruan ini terkapar di rumah sakit paska kecelakaan yang dia alami. &quot;Ekspresi orang sakitnya kan harus dapat. Terus suaranya juga harus disesuaikan, tidak seperti kita ngomong biasa, jadi agak sedikit tertahan di tenggorokan, powernya tidak full seperti ngomong biasanya,&quot; bebernya.\r\n</p>\r\n<p>\r\nUntuk adegan itu Kholidi yang kini menempuh pendidikan di Universitas Al Azhar, Jakarta melakukan observasi pada beberapa orang yang pernah mengalami kecelakaan. &quot;Aku juga tanya-tanya ke dokter. Ternyata di tempat tidurnya nggak bisa pakai bantal, posisi badannya harus lurus. Terus kalau ada gips di kaki, posisi jalan kita akan seperti apa. Biar nantinya terlihat lebih reel lah adengannya,&quot;  pungkas Kholidi. (sumber: <a href="http://surya.co.id">surya.co.id</a>) \r\n</p>\r\n', '2009-10-25', '07:55:45', '54kcb2.jpg', 51),
(91, 'Manchester United Incar Zidane Baru', '<p>\r\nManchester United sedang mengincar pemain muda Perancis berdarah Aljazair. Pemain itu adalah Sofiane Feghouli yang baru berusia 19 tahun.\r\n</p>\r\n<p>\r\nSofiane Feghouli saat ini memperkuat tim Liga Perancis, Grenoble Foot 38. Posisinya adalah di lapangan tengah.\r\n</p>\r\n<p>\r\nPemain yang punya tinggi badan 178 cm itu disebut punya gaya bermain yang serupa dengan Zinedine Zidane. Feghouli sudah masuk dalam tim nasional Perancis U-21.\r\n</p>\r\n<p>\r\nTak hanya MU yang menginginkan pemain yang pernah ditolak Paris Saint-Germain itu. Tim-tim besar macam Barcelona, Liverpool dan Inter Milan juga sedang mengambil ancang-ancang untuk mengajukan tawaran.\r\n</p>\r\n<p>\r\nSeperti diberitakan Tribalfootball, MU sudah berencana untuk melakukan transaksi dengan Grenoble bulan Januari nanti. (Sumber: vivanews.com)\r\n</p>\r\n', '2009-10-25', '13:58:18', '62sofiane.jpg', 18),
(99, 'Editor TextArea Ala Ms Word', '<div style="text-align: center">\r\n</div>\r\n<div style="text-align: center">\r\n</div>\r\n<div style="text-align: center">\r\n</div>\r\n<p>\r\nSecara standar, textarea akan ditampilkan apa adanya, artinya teks yang diketik tidak bisa diatur formatnya, misalnya apabila kita ingin kalimat tertentu ditebalkan, dimiringkan atau diatur jenis dan ukuran hurufnya. Hal ini tidak bisa dilakukan dalam textarea standar, kecuali Anda hapal perintah HTML, kemudian menuliskannya secara manual di textarea tersebut, namun bagi reporter atau user yang awam tentu hal ini cukup menyulitkan mereka.<br />\r\n<br />\r\nSolusinya, gunakan editor <strong>WYSIWYG</strong> (<em>What You See Is What You Get</em>) – Apa yang kau lihat adalah apa yang kau dapatkan. Menurut pengertian dari Wikipedia, WYSIWYG adalah suatu editor yang memungkinkan user untuk menentukan format, ukuran dan jenis huruf, menambahkan hyperlink dan tabel, dan juga bisa mengupload file, gambar, animasi flash, dan video.<br />\r\n</p>\r\n<div style="text-align: center">\r\n<img src="http://localhost./lokomedia/tinymcpuk/gambar/Image/cktini.jpg" alt=" " width="326" height="72" />\r\n</div>\r\n<p>\r\nSaat ini banyak sekali editor WYSIWYG, tapi daripada bingung memilih, saya sarankan untuk menggunakan <strong>TinyMCE</strong> atau <strong>CKEditor</strong>, karena kedua open source editor WYSIWYG tersebut sudah teruji di CMS sekelas Joomla dan Wordpress. Alasan lainnya, karena kelengkapan dokumentasi, kaya fiturnya, kompatibilitas browser, dukungan forum, update, dan plugins. \r\n</p>\r\n<p>\r\nSaat searching di Google, saya ketemu sama yang namanya <strong>tinyFCK</strong> (<a href="http://p4a2.crealabsfoundation.org/tinyfck" target="_blank">http://p4a2.crealabsfoundation.org/tinyfck</a>), editor WYSIWYG yang menggabungkan kelebihan dari TinyMCE dan CKEditor, atau yang lebih kompleks lagi, yaitu <strong>TinyMCPUK</strong>, karena selain menggabungkan kelebihan dari TinyMCE dan CKEditor, juga ditambahkan image manager yang berguna untuk memanipulasi gambar.\r\n</p>\r\n', '2010-01-12', '02:27:42', '72office.jpg', 114);
INSERT INTO `tbl_intermezzo` (`id_berita`, `judul`, `isi_berita`, `tanggal`, `jam`, `gambar`, `dibaca`) VALUES
(101, 'Jadwal Lengkap Sepakbola Piala Dunia 2010', '<p>\r\nPerhelatan akbar piala dunia 2010 yang diselenggrakan di Afsel (Afrika Selatan) akan jatuh pada bulan Juni nanti, walaupun pada piala dunia kali saya kurang antusias karena pemain pujaan tidak lagi bertanding, Zinedine Zidane, tapi tetep berusaha meyakinkan diri bahwa Perancis setidaknya dapat berbicara banyak nanti.<br />\r\n<br />\r\nBerikut ini adalah jadwal piala dunia 2010 berserta jam tayang, tanggal dan bulan, yang akan ditayangkan di ke 2 stasiun TV swasta yakni RCTI dan Global TV karena mereka yang dapat hak siar<br />\r\n<br />\r\n<strong>Keterangan</strong>: Waktu untuk pertandingan ialah GMT+1, yang perlu dilakukan untuk sesuaikan dengan waktu Indonesia cukup memajukan 6 jam saja karena Indonesia termasuk kedalam waktu GMT+7<br />\r\n<br />\r\nGrup A<br />\r\n<br />\r\nJumat, 11 Juni 2010<br />\r\nAfrika Selatan v Meksiko, 15:00<br />\r\nUruguay v Perancis, 19:30<br />\r\n<br />\r\nRabu, 16 Juni 2010<br />\r\nAfrika Selatan v Uruguay, 19:30<br />\r\n<br />\r\nKamis, 17 Juni 2010<br />\r\nFrance v Meksiko, 12:30<br />\r\n<br />\r\nSelasa, 22 Juni 2010<br />\r\nFrance v Afrika Selatan, 15:00<br />\r\nMeksiko v Uruguay, 15:00<br />\r\n<br />\r\nGrup B<br />\r\n<br />\r\nSabtu, 12 Juni 2010<br />\r\nArgentina v Nigeria, 12:30<br />\r\nKorea Selatan v Yunani, 15:00<br />\r\n<br />\r\nKamis, 17 Juni 2010<br />\r\nArgentina v Korea Selatan, 19:30<br />\r\nYunani v Nigeria, 15:00<br />\r\n<br />\r\nSelasa, 22 Juni 2010<br />\r\nYunani v Argentina, 19:30<br />\r\nNigeria v Korea Selatan, 19:30<br />\r\n<br />\r\nGrup C<br />\r\n<br />\r\nSabtu, 12 Juni 2010<br />\r\nEngland v USA, 19:30<br />\r\n<br />\r\nMinggu, 13 Juni 2010<br />\r\nAlgeria v Slovenia, 12:30<br />\r\n<br />\r\nJumat, 18 Juni 2010<br />\r\nEngland v Aljazair, 19:30<br />\r\nSlovenia v USA, 15:00<br />\r\n<br />\r\nRabu, 23 Juni 2010<br />\r\nSlovenia v England, 15:00<br />\r\nUSA v Aljazair, 15:00<br />\r\n<br />\r\nGrup D<br />\r\n<br />\r\nMinggu, 13 Juni 2010<br />\r\nJerman v Australia, 15:00<br />\r\nSerbia v Ghana, 19:30<br />\r\n<br />\r\nJumat, 18 Juni 2010<br />\r\nJerman v Serbia, 12:30<br />\r\n<br />\r\nSabtu, 19 Juni 2010<br />\r\nGhana v Australia, 12:30<br />\r\n<br />\r\nRabu, 23 Juni 2010<br />\r\nAustralia v Serbia, 19:30<br />\r\nGhana v Germany, 19:30<br />\r\n<br />\r\nGrup E<br />\r\n<br />\r\nSenin, 14 Juni 2010<br />\r\nJepang v Kamerun, 15:00<br />\r\nBelanda v denmark, 12:30<br />\r\n<br />\r\nSabtu, 19 Juni 2010<br />\r\nKamerun v denmark, 19:30<br />\r\nBelanda v Jepang, 15:00<br />\r\n<br />\r\nKamis, 24 Juni 2010<br />\r\nKamerun v Belanda, 19:30<br />\r\nDenmark v Jepang, 19:30<br />\r\n<br />\r\nGrup F<br />\r\n<br />\r\nSenin, 14 Juni 2010<br />\r\nItalia v Paraguay, 19:30<br />\r\n<br />\r\nSelasa, 15 Juni 2010<br />\r\nSelandia Baru v Slowakia, 12:30<br />\r\n<br />\r\nMinggu, 20 Juni 2010<br />\r\nItalia v Selandia Baru, 15:00<br />\r\nParaguay v Slowakia, 12:30<br />\r\n<br />\r\nKamis, 24 Juni 2010<br />\r\nParaguay v Selandia Baru, 15:00<br />\r\nSlovakia v Italia, 15:00<br />\r\n<br />\r\nGrup G<br />\r\n<br />\r\nSelasa, 15 Juni 2010<br />\r\nBrasil v Korea Utara, 19:30<br />\r\nPantai Gading v portugal, 15:00<br />\r\n<br />\r\nMinggu, 20 Juni 2010<br />\r\nBrasil v Pantai Gading, 19:30<br />\r\n<br />\r\nSenin, 21 Juni 2010<br />\r\nPortugal v Korea Utara, 12:30<br />\r\n<br />\r\nJumat, 25 Juni 2010<br />\r\nKorea Utara v Pantai Gading, 15:00<br />\r\nPortugal v Brazil, 15:00<br />\r\n<br />\r\nGrup H<br />\r\n<br />\r\nRabu, 16 Juni 2010<br />\r\nHonduras v Chili, 12:30<br />\r\nSpanyol v Swiss, 15:00<br />\r\n<br />\r\nSenin, 21 Juni 2010<br />\r\nChili v Swiss, 15:00<br />\r\nSpanyol v Honduras, 19:30<br />\r\n<br />\r\nJumat, 25 Juni 2010<br />\r\nChili v Spanyol, 19:30<br />\r\nSwiss v Honduras, 19:30\r\n</p>\r\n<p>\r\n(sumber: pialadunia2010com.com) \r\n</p>\r\n', '2010-04-10', '22:21:38', '54bola.jpg', 15),
(102, 'Lionel Messi ''Berlumuran'' Rekor Gol', '<p>\r\nTanpa ampun Lionel Messi menggelontor gawang Arsenal dengan empat gol\r\ndi Camp Nou. Dengan gol-gol itu, si andalan Barcelona pun bikin\r\nsejumlah raihan positif.<br />\r\n<br />\r\nDi hadapan sekitar 95 ribu penonton yang memadati Camp Nou, Rabu (7/4/2010) dinihari WIB, Barca memastikan laju ke semifinal usai Messi menjebol gawang Manuel Almunia pada menit 21,\r\n37, 42 dan 88. Arsenal sendiri hanya sempat membalas lewat gol Nicklas\r\nBendtner pada menit 18.<br />\r\n<br />\r\nDengan penampilan apik berbuah gol-gol\r\ntersebut, Messi dicatat situs Barca membuat sejumlah capaian. Berikut\r\ncapaian-capaian tersebut:<br />\r\n</p>\r\n<ul>\r\n	<li>\r\n	Ini adalah kali pertama Messi bikin\r\n	empat gol dalam satu pertandingan untuk Barca. Sebelumnya, si pemain\r\n	Argentina itu &quot;cuma&quot; bisa bikin lima hat-trick dan 18 kali membuat\r\n	sepasang gol dalam satu laga.</li>\r\n	<li>Messi menjadi pemain pertama musim ini yang berhasil membuat empat gol dalam satu laga di Liga Champions.</li>\r\n	<li>Messi menjadi satu dari enam pemain di dalam sejarah kompetisi ini\r\n	untuk membuat empat gol di satu partai. Sebelumnya telah ada Marco Van\r\n	Basten (AC Milan), Simone Inzaghi (Lazio), Dado Prso (M&ograve;naco), Ruud Van\r\n	Nistelroy (M. United) dan Andriy Shevchenko (AC Milan). Artinya, Messi\r\n	juga menjadi pemain pertama Barca yang melakukannya.</li>\r\n	<li>Berkat\r\n	tiga gol di paruh pertama, Messi menjadi satu dari sembilan pemain yang\r\n	mampu bikin hat-trick di babak pertama partai Liga Champions. Messi\r\n	adalah pemain pertama yang melakukannya musim ini.</li>\r\n	<li>Tambahan\r\n	empat gol ke gawang Arsenal membuat total gol Messi di Liga Champions\r\n	menjadi 25 gol. Ini menyamai pundi gol mantan pemain Barca, Rivaldo,\r\n	yang juga topskorer Barca dalam kompetisi tersebut.</li>\r\n	<li>Dengan\r\n	empat gol ke gawang Arsenal di satu partai, Messi membuat klub London\r\n	tersebut menjadi tim yang paling banyak dia bobol gawangnya di Eropa.\r\n	Sevilla dan Atletico Madrid adalah lumbung gol kesukaan Messi di La\r\n	Liga Primera dengan tujuh gol.</li>\r\n	<li>Dengan tambahan empat gol,\r\n	Messi kini menjadi topskorer sementara Liga Champions dengan delapan\r\n	gol. Pesaing terdekatnya adalah andalan Real Madrid --yang sudah\r\n	tersingkir-- Cristiano Ronaldo (tujuh gol) dan bintang Manchester\r\n	United Wayne Rooney (lima gol).</li>\r\n	<li>Messi sudah mengoleksi total\r\n	39 gol musim ini. Jumlah itu lebih banyak satu gol ketimbang musim\r\n	lalu. Messi kini bahkan melakukannya hanya dalam 42 laga, delapan\r\n	partai lebih sedikit dibandingkan musim lalu.</li>\r\n	<li>Empat gol ke\r\n	gawang Arsenal juga menambah catatan gol Messi di kandang Barca, yang\r\n	kini menjadi 67 gol. Sejumlah 52 gol lain dia buat di laga tandang.\r\n	</li>\r\n</ul>\r\n<p>\r\n(sumber: detiksport.com) \r\n</p>\r\n', '2010-04-10', '22:28:32', '51messi.jpg', 13),
(103, 'Penanganan Gempa Berjalan Cepat, Presiden SBY Puas', '<p>\r\nPresiden\r\nSusilo Bambang Yudhoyono (SBY) mengaku puas atas reaksi\r\ninstansi-instansi terkait dalam menangani gempa di Nanggroe Aceh\r\nDarussalam dan beberapa daerah di Sumatera pada Rabu (7/4) pukul 05.15\r\nWIB. Menurut Presiden, sistem reaksi cepat penanggulangan bencana telah\r\nberjalan dengan baik.<br />\r\n<br />\r\n&quot;Saya juga senang bahwa sistem telah\r\nberjalan karena begitu diterima gempa, satuan reaksi cepat\r\npenanggulangan bencana siap di Halim,&quot; kata Presiden di Bandara Halim\r\nPerdanakusuma, Rabu (7/4). Presiden menyampaikan hal itu sebelum\r\nbertolak menuju Hanoi, Vietnam untuk menghadiri KTT ASEAN hingga Sabtu\r\n(10/4).<br />\r\n<br />\r\nPresiden mengatakan, dirinya langsung berkomunikasi\r\nKetua Badan Nasional Penanggulangan Bencana (BNPB), Gubernur NAD, dan\r\nGubernur Sumatera Utara. Melalui komunikasi itu, Presiden mendapat\r\ninformasi bahwa kerusakan yang ditimbulkan tergolong ringan dan\r\npemadaman listrik sudah berakhir. Presiden mengucapkan terima kasih\r\natas kerja sigap instansi terkait.<br />\r\n<br />\r\nDalam kesempatan sama,\r\nMenteri Sosial, Salim Segaf Aljufri, mengatakan, dampak dari gempa di\r\nAceh itu tergolong ringan, tidak banyak bangunan yang rusak berat.\r\n&quot;Luka berat empat orang, seluruhnya 12 orang yang dirawat di rumah\r\nsakit,&quot; kata mantan Dubes RI di Arab Saudi ini.<br />\r\n<br />\r\nSalim\r\nmengatakan, gempa itu juga masih bisa ditangani oleh pemerintah daerah.\r\nAlasannya, stok bantuan bahan pangan di daerah masih mencukupi,\r\nkhususnya beras dan lauk pauk. &quot;Buffer stock kita di provinsi cukup,\r\nberas ada 50 ton,&quot; ujar Salim. Penyaluran bantuan pun belum ada kendala\r\nberarti.\r\n</p>\r\n<p>\r\n(sumber: republika.co.id) \r\n</p>\r\n', '2010-04-10', '22:32:19', '58sby.jpg', 11),
(104, 'Film ''My Name is Khan'' Cetak Rekor di Amerika', '<p>\r\nDengan US$1,86 juta pada <em>box office</em> di minggu pertamanya, film <em>My\r\nName is Khan</em> yang dibintangi Shah Rukh Khan telah memecahkan rekor \r\nsebagai film India dengan pendapatan terbanyak yang diputar di Amerika \r\nUtara.\r\n<br />\r\n<br />\r\nFilm arahan Karan Johar ini diperkirakan menghasilkan US$15.500 dari\r\n120 bioskop di AS dan Kanada pada akhir pekan 12-14 Februari 2009. \r\nRekor sebelumnya dipegang film musikal yang juga dibintangi Shah Rukh, <em>Om\r\nShanti Om</em>, dengan pendapatan US$1,76 juta dari 114 bioskop saat \r\ndirilis pada 2007.\r\n<br />\r\n<br />\r\nSaat diluncurkan Jumat (12/2), <em>My Name is Khan</em> langsung \r\nmendapatkan US$444 ribu, lalu langsung meningkat 65% menjadi US$734.000 \r\nSabtu (13/2). Tapi, film ini lalu menurun sebanyak 7% menjadi US$682 \r\nribu pada Minggu (14/2) yang bertepatan dengan Hari Valentine.\r\n<br />\r\n<br />\r\nTerlebih lagi, <em>My Name is Khan</em> mendapatkan pujian dari \r\nkritikus AS. Publikasi surat kabar <em>Hollywood Reporter</em> mengatakan,\r\n&quot;Ini sepadan untuk perjalanan selama 162 menit. Shah Rukh Khan datang \r\nke Amerika (walau melalui film Bollywood) dan telah menunjukkan bahwa \r\ndirinya adalah megabintang India,&quot; tambahnya.\r\n<br />\r\n<br />\r\n&quot;Yang khas dari bintang Bollywood adalah mereka tidak hanya aktor \r\nyang berkualitas, tapi juga memiliki kharisma. Jadi, tidak mengejutkan \r\nbila menemukan megabintang Shah Rukh Khan dalam <em>My Name is Khan</em>. \r\nTampaknya ia sedang menantang dirinya sendiri untuk meningkatkan \r\nkemampuan aktingnya dan memperluas jaringan penggemar internasionalnya.&quot;\r\n<br />\r\n \r\n<br />\r\n&quot;Dengan arahan sutradara andal Karan Johar dan musik pengiring yang \r\nmenggugah oleh Shankar, Ehsaan &amp; Loy, Khan membuat kita mudah \r\nmeneteskan air mata seraya mengajarkan kita mengenai Islam dan \r\ntoleransi,&quot; kata surat kabar <em>Times</em>. \r\n<br />\r\n<br />\r\nDi dalam negeri sendiri, pada saat rilis perdananya, Jumat (12/2), \r\nhanya 13 bioskop yang memutarnya dari awal rencana 63 bioskop. Tapi, \r\npada Sabtu (13/2), semua bioskop di Mumbai, Pune, dan Maharashra telah \r\nmenayangkannya. Terakhir, pendapatan film tersebut di seluruh dunia \r\ntelah mencapai US$18 juta.\r\n</p>\r\n<p>\r\n(sumber: mediaindonesia.com) \r\n</p>\r\n', '2010-04-10', '22:46:50', '39khan.jpg', 33),
(105, 'Taufik Berada di Grup Maut Kejuaran Dunia Bulutangkis', '<p>\r\nTaufik Hidayat akan menghadapi pemain China, Bao Chunlai, di babak awal pertadingan Grup A \r\nkejuaraan World Super Series Masters Finals, Rabu (2/11).<br />\r\n<br />\r\nTaufik,\r\nyang merupakan satu-satunya pemain tunggal putra asal Indonesia,\r\nbergabung di Grup A bersama peringkat satu dunia Lee Chong Wei, Bao\r\nChunlai, serta pemain China Taipei, Hsieh Yu Hsin.<br />\r\n<br />\r\nMemakai\r\nsistem pertandingan round robin, Taufik akan menghadapi Chunlai,\r\nsedangkan Chong Wei bertemu dengan Hsieh Yu Hsin dalam pertandingan\r\nlainnya, Rabu (2/11).<br />\r\n<br />\r\nGrup A dianggap sebagai grup neraka atau\r\nmaut, sedangkan Grup B terdiri dari dua pemain Denmark, Peter Hoeg Gade\r\ndan Jan O Jorgensen, serta pemain Thailand, Boonsak Ponsana.<br />\r\n<br />\r\nTaufik\r\nsendiri menjadi satu-satunya pemain tunggal asal Indonesia setelah Sony\r\nDwi Kuncoro dan Simon Santoso absen karena diprioritaskan bermain di\r\najang SEA Games, Laos, Desember ini.<br />\r\n<br />\r\nPeraih medali emas\r\nOlimpiade Atlanta 2004 ini mengaku siap menghadapi tantangan di grup\r\nyang berat ini. Di jejaring sosial Facebook, ia menulis, &quot;Death Group?&quot;\r\nTantangan berat di Johor Bahru, tapi harus memberi yang terbaik! Let&#39;s\r\nGo!\r\n</p>\r\n<p>\r\n&nbsp;\r\n</p>\r\n<div style="text-align: center">\r\n<img src="http://localhost./lokomedia/tinymcpuk/gambar/Image/taufik_hidayat.jpg" alt=" " width="350" height="250" />\r\n</div>\r\n<br />\r\n<p>\r\n&nbsp;\r\n</p>\r\n<p>\r\nGrup A<br />\r\n1 [MAS] LEE Chong Wei<br />\r\n1 [CHN] BAO Chunlai <br />\r\n1 [INA] HIDAYAT Taufik<br />\r\n1 [TPE] HSIEH Yu Hsin<br />\r\n<br />\r\nGrup B<br />\r\n1 [DEN] GADE Peter Hoeg<br />\r\n1 [KOR] PARK Sung Hwan <br />\r\n1 [DEN] O JORGENSEN Jan<br />\r\n1 [THA] PONSANA Boonsak\r\n</p>\r\n<p>\r\n(sumber: beritajitu.com) \r\n</p>\r\n', '2010-04-10', '22:51:14', '92taufik.jpg', 74),
(120, 'The King Speech, Saat Raja Belajar Bertutur Kata', 'Ini dia film yang meraih perolehan nominator terbanyak dalam acara \r\nAcademy Awards ke 83. Dari 24 jumlah nominasi yang ada,&nbsp; The King&#39;s \r\nSpeech<strong> </strong>berhasil meraih setengahnya dan menjadikan film produksi See \r\nSaw Films dan Bedlam Productions itu merajai Oscar 2011 yang merupakan \r\najang perfilman paling bergengsi di dunia.<br />\r\n<br />\r\nSetelah kematian ayahandanya, raja George V (Michael Gambon), pangeran \r\nAlbert (Colin Firth) akhirnya dinobatkan sebagai raja. Diangkatnya ayah \r\ndua anak ini menjadi raja baru Inggris karena sang kakak, pangeran \r\nEdward VIII (Guy Pearce) yang seharusnya berkuasa, rela turun tahta \r\nkarena lebih memilih seorang janda keturunan Amerika untuk dinikahinya.\r\n<br />\r\n<br />\r\nTapi apa jadinya jika seorang raja menderita kesulitan \r\nberbicara?terutama pada saat berpidato. Karena sebelum dinobatkan \r\nsebagai Raja, beberapa kali Bertie (panggilan pangeran Albert dari \r\norang-orang terdekatnya) harus mewakili pidato ayahnya yang sakit, baik \r\nsecara langsung maupun melalui siaran radio dan hasilnya sangat \r\nmengecewakan bagi siapa saja yang mendengarnya.<br />\r\n<br />\r\nDibantu sang istri tercinta, Elizabeth (Helena Bonham Carter), Raja \r\nGeorge VI menemui ahli terapi bicara bersama Lionel Logue (Geoffrey \r\nRush) yang eksentrik. Pertemuan keduanya walau diawali dengan \r\nperseteruan, keduanya akhirnya menjalani program terapi dan akhirnya \r\nmembentuk ikatan yang tak terpisahkan.<br />\r\n<br />\r\nMasalah raja George VI ternyata bukan hanya dari dalam dirinya saja, \r\ndengan keadaan negara yang diambang peperangan, raja baru itu akhirnya \r\nmelakukan pidato pertamanya di radio BBC untuk rakyat dan negaranya. \r\nDengan dukungan dari Lionel, keluarga, pemerintah dan Winston Churchill \r\n(Timothy Spall), mampukah raja baru ini menginspirasi seluruh Inggris \r\nuntuk bersiap melawan kebrutalan tentara Jerman.<strong><br />\r\n</strong> <br />\r\nKejeniusan sang sutradara mengemas film akhirnya memberikan jaminan <strong>The\r\nKing&#39;s Speech</strong> menjadi tontonan yang sangat menarik. Anda akan \r\nmerasakan emosi sekaligus tertawa saat melihat Colin Firth yang sangat \r\nmendalami karakter raja George VI. Akting Geoffrey Rush sebagai ahli \r\nbicara membuktikan dirinya memang jago berbicara, bahkan di hadapan \r\nseorang Raja.\r\n', '2011-02-10', '23:15:39', '89speech.jpg', 11),
(129, 'sss', '<table border="0" cellspacing="0" cellpadding="0" width="997">\r\n\n<!--DWLayoutTable--> \r\n\n<tbody>\r\n\n<tr>\r\n\n<td width="170" height="36" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>\r\n\n<td width="827" valign="top"><input name="textfield" type="text" /></td>\r\n\n</tr>\r\n\n<tr>\r\n\n<td height="36" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>\r\n\n<td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>\r\n\n</tr>\r\n\n<tr>\r\n\n<td height="36" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>\r\n\n<td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>\r\n\n</tr>\r\n\n<tr>\r\n\n<td height="192">&nbsp;</td>\r\n\n<td>&nbsp;</td>\r\n\n</tr>\r\n\n</tbody>\r\n\n</table>', '2012-06-11', '03:48:00', '260306316ic_launcher.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_katalog`
--

CREATE TABLE IF NOT EXISTS `tbl_katalog` (
  `id_katalog` int(100) NOT NULL AUTO_INCREMENT,
  `judul_file` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `nama_file` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` varchar(30) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_katalog`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_katalog`
--

INSERT INTO `tbl_katalog` (`id_katalog`, `judul_file`, `nama_file`, `tgl_posting`) VALUES
(6, 'Katalog Bulan Oktober 2011', '21270469115092009.pdf', ' 2011-11-02'),
(8, 'Asu Cicing', '122473083.pdf', ' 2011-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id_kategori` int(10) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) NOT NULL,
  `kode_level` int(2) NOT NULL,
  `kode_parent` int(5) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`, `kode_level`, `kode_parent`, `status`) VALUES
(1, 'T-SHIRT', 0, 0, 1),
(4, 'JEANS', 0, 0, 1),
(3, 'PANTS', 0, 0, 1),
(2, 'HOODIE', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `urutan` int(1) NOT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `kelas`, `nama_menu`, `link`, `icon`, `status`, `urutan`) VALUES
(1, 'dashboard', 'Dashboard', 'dashboard', 'fa fa-laptop', 0, 1),
(2, 'info', 'Informasi Pribadi', '#', 'fa fa-info', 1, 3),
(3, 'mprod', 'Master Produk', '#', 'fa fa-external-link', 1, 5),
(4, 'referensi', 'Referensi Data', '#', 'fa fa-retweet', 1, 7),
(5, 'admember', 'Master Member', '#', 'fa fa-sitemap', 1, 6),
(6, 'tools', 'Tools', '#', 'fa fa-gears', 1, 9),
(8, 'infoper', 'Informasi Fosa', '#', 'fa fa-info', 0, 2),
(9, 'user', 'Data User admin', '#', 'fa fa-external-link', 1, 4),
(7, 'mntrans', 'History Transaksi', '#', 'fa fa-info', 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE IF NOT EXISTS `tbl_produk` (
  `kode_produk` varchar(500) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_kategori` int(10) NOT NULL,
  `nama_produk` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `brand` varchar(100) NOT NULL,
  `harga` int(10) NOT NULL,
  `stok` int(5) NOT NULL,
  `dibeli` int(5) NOT NULL,
  `gbr_kecil` varchar(1000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gbr_besar` varchar(1000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tipe_produk` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `stts` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_produk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`kode_produk`, `id_kategori`, `nama_produk`, `brand`, `harga`, `stok`, `dibeli`, `gbr_kecil`, `gbr_besar`, `deskripsi`, `tipe_produk`, `stts`) VALUES
('TSStandardCMStockholm', 1, 'Standard CM Stockholm T-Shirt', 'CHEAP MONDAY', 100000, 10, 2, 'TS-Standard CM Stockholm1.jpg', 'TS-Standard CM Stockholm1.jpg', 'Swedish brand Cheap Monday started off as a single second-hand store in Stockholm, but it has since become famous for its high quality jeans and eccentric designs. Since 2000, the brand has branched out from their denim focus and infused its Scandinavian aesthetic into sneakers, flannel and shirts. Its stores, with their distinctive skull logo, can be found in cities from Copenhagen to Beijing.\r\nCheap Monday goes back to the basics with its Standard CM Stockholm T-shirt. Its signature white tee never fails to please, with the brand name embroidered on the front in contrast black stitching. The t-shirt is styled with a ribbed collar.\r\n	•	Print at front\r\n	•	Ribbed collar\r\n	•	100% cotton\r\n	•	Machine wash warm', 'Unisex', 1),
('HKnitHoodie', 4, 'Knit Hoodie', 'ALIFE', 150000, 10, 4, 'H-Knit Hoodie1.jpg', 'H-Knit Hoodie1.jpg', 'Japanese menswear brand DISCOVERED was founded in 2001 by Tatsuya Kimura and Sanae Yoshida. The concept for their menswear collection was a “real clothes” aesthetic with sleek lines and monochromatic palettes. DISCOVERED pursues genre-less designs and the fusion of conflicting tastes under the concept of “opposition,” and its pieces range from traditional to casual.\r\n	•	2-way front zip closure\r\n	•	Chest pocket detailing\r\n	•	Knitted hood\r\n	•	Dual front pockets', 'Unisex', 1),
('TSAfterDarkTShirt', 1, 'After Dark T-Shirt', 'STUSSY', 100000, 10, 6, 'TS-After Dark T-Shirt1.jpg', 'TS-After Dark T-Shirt1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\r\n	•	Regular fit short sleeve t-shirt\r\n	•	Ribbed crew neck\r\n	•	Printed logo\r\n	•	100% cotton', 'Unisex', 1),
('TSAlifexPumaSoccer', 1, 'Alife x Puma Soccer T-Shirt', 'ALIFE', 120000, 10, 8, 'TS-Alife x Puma Soccer1.jpg', 'TS-Alife x Puma Soccer1.jpg', '        •	Alife x Puma collaboration\n	•	Crewneck\n	•	Print at front\n	•	73% cotton 27% polyester\n	•	Machine wash cold', 'Unisex', 1),
('TSArmyVsArmy', 1, 'Army Vs. Army T-Shirt', 'UNDEFEATED', 100000, 10, 5, 'TS-Army Vs. Army1.jpg', 'TS-Army Vs. Army1.jpg', 'Undefeated started out as a LA premium sneaker store in 2002, but has since branched out to become on of the most esteemed streetwear purveyors on the market. Headed by Eddie Cruz and James Bond, Undefeated not only sells coveted, limited edition sneakers but also its own line of quality sweatshirts, caps and tees.\r\n	•	Regular fit\r\n	•	Ribbed collar\r\n	•	Screen print graphic on chest\r\n	•	100% Cotton', 'Unisex', 1),
('TSBlackBox', 1, 'Black Box T-Shirt', 'UNDEFEATED', 100000, 10, 3, 'TS-Black Box1.jpg', 'TS-Black Box1.jpg', 'Undefeated started out as a LA premium sneaker store in 2002, but has since branched out to become on of the most esteemed streetwear purveyors on the market. Headed by Eddie Cruz and James Bond, Undefeated not only sells coveted, limited edition sneakers but also its own line of quality sweatshirts, caps and tees.\r\n	•	Regular fit\r\n	•	Ribbed collar\r\n	•	Screen print graphic on chest\r\n	•	100% Cotton', 'Unisex', 1),
('TSCanineStandard', 1, 'Canine Standard T-Shirt', 'CHEAP MONDAY', 100000, 10, 6, 'TS-Canine Standard1.jpg', 'TS-Canine Standard1.jpg', 'Swedish brand Cheap Monday started off as a single second-hand store in Stockholm, but it has since become famous for its high quality jeans and eccentric designs. Since 2000, the brand has branched out from their denim focus and infused its Scandinavian aesthetic into sneakers, flannel and shirts. Its stores, with their distinctive skull logo, can be found in cities from Copenhagen to Beijing.\r\nThis allover print tee is bound to get second looks, with its dizzying black and white optical illusions that mix patterns from checkerboard to houndstooth. Crafted from soft cotton jersey, the regular-fit t-shirt is styled with a round crewneck collar.\r\n	•	Crewneck\r\n	•	Allover print\r\n	•	100% Cotton\r\n	•	Machine wash', 'Unisex', 1),
('TSChemicalLogo', 1, 'Chemical Logo T-Shirt', 'CHEAP MONDAY', 100000, 10, 3, 'TS-Chemical Logo1.jpg', 'TS-Chemical Logo1.jpg', 'Swedish brand Cheap Monday started off as a single second-hand store in Stockholm, but it has since become famous for its high quality jeans and eccentric designs. Since 2000, the brand has branched out from their denim focus and infused its Scandinavian aesthetic into sneakers, flannel and shirts. Its stores, with their distinctive skull logo, can be found in cities from Copenhagen to Beijing\r\nCheap Monday injects a little humor into the plain t-shirt, stylizing their brand name as a lengthy molecular formula. Crafted from comfortable grey melange fabric, the shirt is finished with a ribbed collar.\r\n	•	Ribbed collar\r\n	•	Print at front\r\n	•	100% cotton\r\n	•	Machine wash warm', 'Unisex', 1),
('TSDisrupt5Strike', 1, 'Disrupt 5 Strike T-Shirt', 'UNDEFEATED', 100000, 10, 9, 'TS-Disrupt 5 Strike1.jpg', 'TS-Disrupt 5 Strike1.jpg', 'Undefeated started out as a LA premium sneaker store in 2002, but has since branched out to become on of the most esteemed streetwear purveyors on the market. Headed by Eddie Cruz and James Bond, Undefeated not only sells coveted, limited edition sneakers but also its own line of quality sweatshirts, caps and tees.\r\n	•	Longsleeve\r\n	•	Printed logo on chest\r\n	•	Ribbed detailing\r\n	•	55% Cotton, 45% Polyester', 'Unisex', 1),
('TSFantastic', 1, 'Fantastic T-Shirt', 'CHEAP MONDAY', 100000, 10, 7, 'TS-Fantastic1.jpg', 'TS-Fantastic1.jpg', 'Founded in 2000 by Örjan Andersson and Adam Friberg, Cheap Monday originally started as a second-hand clothing store in Stockholm. The Swedish label has since expanded from their initial focus on denim to a full collection of apparel including sneakers, flannels and shirts. Despite the brand’s rapid growth and success, Cheap Monday continues to offer quality high-fashion products at affordable prices.\r\nRelaxed-fit, solid jersey t-shirt with crew neck in soft cotton.\r\n	•	Grey Melange\r\n	•	Relaxed-fit\r\n	•	Soft-touch jersey\r\n	•	Crew neck\r\n	•	100% Cotton', 'Unisex', 1),
('TSHeather Grey', 1, 'Heather Grey T-Shirt', 'ALIFE', 100000, 10, 3, 'TS-Heather Grey1.jpg', 'TS-Heather Grey1.jpg', 'HEATHER GREY\r\nFounded in 1999, ALIFE is a New York City-based, multitasking, multifaceted lifestyle-driven company. ALIFE is well known for its curatorial work, creative direction, and production of editorial content for books and magazines, art shows, retail concepts, top-tier branded collaborations and live music events. Short-sleeved crewneck T-shirt with printed graphics. Available now in black or heather grey.', 'Unisex', 1),
('TSRenaissance', 1, 'Renaissance T-Shirt', 'UNDEFEATED', 100000, 10, 3, 'TS-Renaissance1.jpg', 'TS-Renaissance1.jpg', 'Undefeated started out as a LA premium sneaker store in 2002, but has since branched out to become on of the most esteemed streetwear purveyors on the market. Headed by Eddie Cruz and James Bond, Undefeated not only sells coveted, limited edition sneakers but also its own line of quality sweatshirts, caps and tees.\r\n	•	Ribbed trim\r\n	•	Short sleeve\r\n	•	Screen print graphics on front\r\n	•	Undefeated brand tag on the left sleeve\r\n	•	100% cotton\r\n', 'Unisex', 1),
('TSStock Link', 1, 'Stock Link T-Shirt', 'STUSSY', 100000, 10, 4, 'TS-Stock Link1.jpg', 'TS-Stock Link1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\r\n	•	Regular fit short sleeve t-shirt\r\n	•	Ribbed crew neck\r\n	•	Printed logo\r\n	•	100% cotton', 'Unisex', 1),
('TSStussy Rydim', 1, 'Stussy Rydim T-Shirt', 'STUSSY', 100000, 10, 2, 'TS-Stussy Rydim1.jpg', 'TS-Stussy Rydim1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\r\n	•	Regular fit short sleeve t-shirt\r\n	•	Ribbed crew neck\r\n	•	Printed logo\r\n	•	100% cotton', 'Unisex', 1),
('TSTwister', 1, 'Twister T-Shirt', 'STUSSY', 100000, 10, 2, 'TS-Twister1.jpg', 'TS-Twister1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\r\n	•	Regular fit short sleeve t-shirt\r\n	•	Ribbed crew neck\r\n	•	Printed logo\r\n	•	100% cotton', 'Unisex', 1),
('TSYard', 1, 'Yard T-Shirt', 'CHEAP MONDAY', 100000, 10, 2, 'TS-Yard1.jpg', 'TS-Yard1.jpg', 'Founded in 2000 by Örjan Andersson and Adam Friberg, Cheap Monday originally started as a second-hand clothing store in Stockholm. The Swedish label has since expanded from their initial focus on denim to a full collection of apparel including sneakers, flannels and shirts. Despite the brand’s rapid growth and success, Cheap Monday continues to offer quality high-fashion products at affordable prices.\r\nThis classic black long-sleeve shirt features a crew neck and signature Cheap Monday prints at chest and sleeve.\r\n	•	Soft-touch jersey\r\n	•	Crew neck\r\n	•	Long sleeves\r\n	•	Printed design\r\n	•	Relaxed fit\r\n	•	Dry clean\r\n	•	100% Cotton', 'Unisex', 1),
('H8BallZipHoodie', 4, 'H-8 Ball Zip Hoodie', 'STUSSY', 150001, 20, 2, 'H-8 Ball Zip Hoodie1.jpg', 'H-8 Ball Zip Hoodie1.jpg', ' • Zip up front\\\\\\\\\\\\\\\\r\\\\\\\\\\\\\\\\n • Print at front and back\\\\\\\\\\\\\\\\r\\\\\\\\\\\\\\\\n • Drawstring at hood\\\\\\\\\\\\\\\\r\\\\\\\\\\\\\\\\n • 80% cotton 20% polyester fleece\\\\\\\\\\\\\\\\r\\\\\\\\\\\\\\\\n • Made in USA', 'Unisex', 1),
('HBilliardsAppliqueHoodie', 4, 'H-Billiards Applique Hoodie', 'STUSSY', 150000, 10, 6, 'H-Billiards Applique Hoodie1.jpg', 'H-Billiards Applique Hoodie1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\n	•	Ribbed waist and cuffs\n	•	Hood with drawstring\n	•	Embroidered design\n	•	Kangaroo pocket\n	•	80% Cotton, 20% Polyester\n', 'Unisex', 1),
('HDesignCorpHoodie', 4, 'H-Design Corp Hoodie', 'STUSSY', 150000, 10, 3, 'H-Design Corp Hoodie1.jpg', 'H-Design Corp Hoodie1.jpg', 'Founded in 1980,Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\nThis white hoodie is given a high-contrast touch with the Stussy circular emblem screenprinted on the back and embroidered on the front in black. Crafted from cotton and polyester fleece, the hoodie is styled with a classic fit, drawstring hood and pouch.\n	•	Screenprinted and embroidered graphics on front and back\n	•	Drawstring at hood and pouch\n	•	80% cotton and 20% polyester fleece\n	•	Imported\n', 'Unisex', 1),
('HMiddleweightPulloverHoodie', 4, 'Middleweight Pullover Hoodie', 'ALIFE', 150000, 10, 4, 'H-Middleweight Pullover Hoodie1.jpg', 'H-Middleweight Pullover Hoodie1.jpg', 'The brand has since collaborated with artists from Weirdo Dave to Tsuyoshi Noguchii, cultivating a style that is both authentic and glamorous.\r\n	•	Hooded\r\n	•	Printed graphic at back\r\n	•	Kangaroo pocket\r\n	•	Ribbed cuffs and hem\r\n	•	100% Cotton\r\n	•	Made In Japan\r\n', 'Unisex', 1),
('HPulloverHoodie', 4, 'Pullover Hoodie', 'ALIFE', 150000, 10, 4, 'H-Pullover Hoodie1.jpg', 'H-Pullover Hoodie1.jpg', 'The brand has since collaborated with artists from Weirdo Dave to Tsuyoshi Noguchii, cultivating a style that is both authentic and glamorous.\r\n	•	Hooded\r\n	•	Printed graphic at back\r\n	•	Kangaroo pocket\r\n	•	Ribbed cuffs and hem\r\n	•	100% Cotton\r\n	•	Made In Japan\r\n', 'Unisex', 1),
('HRunnerHoodie', 4, 'Runner Hoodie', 'CHEAP MONDAY', 150000, 10, 3, 'H-Runner Hoodie1.jpg', 'H-Runner Hoodie1.jpg', 'Born from NYC skate and hip-hop culture, KRSP takes influence from cultures around the globe, cultivating an international aesthetic. The brand is highly committed to fresh and continuous evolution, fueling its growth with experimental concepts and DIY creativity.\r\n	•	Drawstring adjustable hood\r\n	•	Kangaroo front pocket\r\n	•	Ribbed trim', 'Unisex', 1),
('HSumatraQuilted', 4, 'Sumatra Quilted Hoodie', 'CHEAP MONDAY', 150000, 10, 9, 'H-Sumatra Quilted1.jpg', 'H-Sumatra Quilted1.jpg', 'Established in 1995 from the desire to create a brand that reflected the unique mindset of NYC youth culture of that time, 10.DEEP® has remained a cornerstone brand in the men’s boutique and progressive streetwear community for nearly two decades.\r\n	•	Heavyweight cotton construction\r\n	•	Drawstring adjustable hood\r\n	•	Woven label detail\r\n	•	Chevron quilt pattern\r\n	•	Kangaroo pocket\r\n	•	Embroidered graphic at front\r\n', 'Unisex', 1),
('HClassicLogoHoodie', 4, 'Classic Logo Hoodie', 'UNDEFEATED', 150000, 10, 6, 'H-Classic Logo Hoodie1.jpg', 'H-Classic Logo Hoodie1.jpg', 'Heritage athletic brand Champion has been a go-to for sportswear since its inception in 1919, producing high-quality college staples such as jerseys, sweatshirts, hoodies and shorts. Their commitment to quality gym gear did not go unnoticed by the sports industry - they have been the official outfitters for U.S. Olympic teams, ESPN and the NBA.\r\n	•	Cotton blend\r\n	•	Reverse weave fabric\r\n	•	Stretch ribbed side panels\r\n	•	2-ply drawcord hood\r\n	•	Signature chest branding\r\n	•	Front kangaroo pocket', 'Unisex', 1),
('HEscapeFromTheNestTour', 4, 'Escape From The Nest Tour Hoodie', 'UNDEFEATED', 150000, 10, 6, 'H-Escape From The Nest Tour1.jpg', 'H-Escape From The Nest Tour1.jpg', 'Nid de Guepes Studio, or N.D.G., is a Parisian label that prides itself for creating minimalist menswear with an emphasis on utilitarianism and artisanship. The brand, which takes its name from the French word for wasp''s nest, focuses on layered, well-fitting separates in various earthy shades. N.D.G. Studio produces all of its garments by hand in France, resulting in high-end garments with great fit and quality.\r\n	•	Ultra oversized fit and drop shoulder\r\n	•	Elongated sleeves\r\n	•	Printed graphic at front\r\n	•	Dyed and printed in paris\r\n	•	Hidden side pockets\r\n	•	100% cotton\r\n', 'Unisex', 1),
('HTriColorOversize', 4, 'Tri-Color Oversize Hoodie', 'UNDEFEATED', 150000, 10, 6, 'H-Tri-Color Oversize1.jpg', 'H-Tri-Color Oversize1.jpg', 'Nid de Guepes Studio, or N.D.G., is a Parisian label that prides itself for creating minimalist menswear with an emphasis on utilitarianism and artisanship. The brand, which takes its name from the French word for wasp''s nest, focuses on layered, well-fitting separates in various earthy shades. N.D.G. Studio produces all of its garments by hand in France, resulting in high-end garments with great fit and quality.\r\n	•	Ultra oversized fit and drop shoulder\r\n	•	Elongated sleeves\r\n	•	Printed graphic at front\r\n	•	Dyed and printed in paris\r\n	•	Hidden side pockets\r\n	•	100% cotton\r\n', 'Unisex', 1),
('JCargoZipMoto', 2, 'Cargo Zip Moto Jeans', 'ALIFE', 200000, 10, 8, 'J-Cargo Zip Moto1.jpg', 'J-Cargo Zip Moto1.jpg', 'LA-based brand STAMPD mixes streetwear and luxury influences to create pieces for the contemporary minimalist. Established in 2011 by Chris Stamp, the brand promotes a West Coast "avantstreet" aesthetic that elevates streetwear offerings with a monochromatic palette and edgy yet athletic styling. The brand has undoubtedly made an impact on fashion beyond the streetwear realm, winning GQ''s award for the Best Menswear Designer in America in 2015.\r\n	•	Original camo pattern\r\n	•	Washed cotton cargo pant.\r\n	•	Drawstring waistband\r\n	•	Zippers at heel for pin-roll', 'Unisex', 1),
('JHeadPrint', 2, 'Head Print Jeans', 'ALIFE', 200000, 10, 4, 'J-Head Print1.jpg', 'J-Head Print1.jpg', 'Under the direction of designer Jeremy Scott, the Italian fashion house Moschino has transformed itself into one of the boldest and most playful brands in the luxury market. With its vivid color palette, pop culture-inspired motifs and provocative slogans, the brand continues to reinforce its motto that "fashion should be fun and should send a message.”\r\n	•	Mid-rise\r\n	•	5-pocket styling\r\n	•	Printed detailing on legs\r\n	•	Two-tone construction\r\n	•	Button closure\r\n	•	Zip fly\r\n	•	98% Cotton, 2% Elastane', 'Unisex', 1),
('JTapered Jeans', 2, 'Tapered Jeans', 'ALIFE', 200000, 10, 4, 'J-Tapered Jeans1.jpg', 'J-Tapered Jeans1.jpg', 'Famous for it’s iconic compass patch, Stone Island is an Italian premium men’s apparel brand. Established in 1984, the Ravarino based label mixes high-performance textiles with a refined urban aesthetic. With a wide range of products, the brand has built a loyal following around its military-inspired sportswear styles.\r\nTrousers in Polypropylene Denim a blue polypropylene weft and indigo dyed cotton warp cloth. The result is an impeccable denim tela with a drastically reduced weight: it looks like 19 ounce material but weighs only 10 oz. The garment undergoes the DARK treatment, a manual sanding and light stonewash as well as an enzyme wash.\r\n	•	Bellows hand pockets\r\n	•	Bellows pocket on both legs, horizontal zip fastening\r\n	•	Back pocket with hidden zip fastening\r\n	•	Engineered leg\r\n	•	Zip and button fly\r\n	•	65% Cotton, 35% Polypropylene', 'Unisex', 1),
('PAntoineCargoPant', 3, 'Antoine Cargo Pant', 'ALIFE', 100000, 10, 2, 'P-Antoine Cargo Pant1.jpg', 'P-Antoine Cargo Pant1.jpg', 'Refining basics is the brand''s main focus - Publish''s garments and accessories are utilitarian yet elegant, elevating everyday wear one premium product at a time.\r\n	•	Zip fly\r\n	•	Dual slanted flap pockets at front\r\n	•	Dual flap pockets at sides\r\n	•	Elongated zipper pull\r\n	•	100% Cotton', 'Unisex', 1),
('PFligntPants', 3, 'Flignt Pants', 'ALIFE', 100000, 10, 6, 'P-Flignt Pants1.jpg', 'P-Flignt Pants1.jpg', 'Established in 2009, thisisneverthat is a contemporary streetwear brand based in Seoul, South Korea. Taking a graphic and text heavy approach to traditional streetwear, the South Korean label has quickly developed a cult following.\r\n	•	Relaxed fit\r\n	•	Elasticated waist\r\n	•	Tonal drawstring\r\n	•	Embroidered detailing\r\n	•	Dual front pockets\r\n	•	Dual rear pockets\r\n	•	Made in Korea\r\n	•	100% Cotton', 'Unisex', 1),
('JEssentailKnee', 2, 'Essentail Knee Jeans', 'CHEAP MONDAY', 200000, 10, 8, 'J-Essentail Knee1.jpg', 'J-Essentail Knee1.jpg', 'Under the direction of designer Jeremy Scott, the Italian fashion house Moschino has transformed itself into one of the boldest and most playful brands in the luxury market. With its vivid color palette, pop culture-inspired motifs and provocative slogans, the brand continues to reinforce its motto that "fashion should be fun and should send a message.”\r\n	•	Mid-rise\r\n	•	5-pocket styling\r\n	•	Printed detailing on legs\r\n	•	Two-tone construction\r\n	•	Button closure\r\n	•	Zip fly\r\n	•	98% Cotton, 2% Elastane', 'Unisex', 1),
('JWoodieDestruction', 2, 'Woodie Destruction Jeans', 'CHEAP MONDAY', 200000, 10, 7, 'J-Woodie Destruction1.jpg', 'J-Woodie Destruction1.jpg', 'Under the direction of designer Jeremy Scott, the Italian fashion house Moschino has transformed itself into one of the boldest and most playful brands in the luxury market. With its vivid color palette, pop culture-inspired motifs and provocative slogans, the brand continues to reinforce its motto that "fashion should be fun and should send a message.”\r\n	•	Mid-rise\r\n	•	5-pocket styling\r\n	•	Printed detailing on legs\r\n	•	Two-tone construction\r\n	•	Button closure\r\n	•	Zip fly\r\n	•	98% Cotton, 2% Elastane', 'Unisex', 1),
('PBasicSweatpants', 3, 'Basic Sweatpants', 'STUSSY', 100000, 10, 8, 'P-Basic Sweatpants1.jpg', 'P-Basic Sweatpants1.jpg', 'Founded in 1980, Stussy has been credited for transforming streetwear from just clothing into an entire lifestyle. Today, the brand''s scrawled signature logo by founder Shawn Stussy is universal, featured on items from t-shirts to boardshorts that are used by skateboarders, surfers and street style stars alike. Having sparked the ascent of streetwear culture more than 30 years ago, Stussy remains one of the movement''s current leaders.\r\nThese regular fit sweatpants feature an adjustable waist and printed logo detailing.\r\n	•	Regular comfort fit sweatpants\r\n	•	Elastic waist with drawstring\r\n	•	Printed logo\r\n	•	60% cotton, 40% polyester fleece blend\r\n	•	Made in the USA\r\n', 'Unisex', 1),
('PPolarFleece', 3, 'Polar Fleece Pants', 'STUSSY', 100000, 10, 8, 'P-Polar Fleece1.jpg', 'P-Polar Fleece1.jpg', '	•	Elastic waist\r\n	•	2 side pockets\r\n	•	Contrast material at kneels\r\n	•	Available in black and grey\r\n	•	100% Polyester\r\n	•	Machine wash | Do not bleach | Do not iron | Do not dry clean\r\n	•	Imported\r\nSize & Fit\r\n	•	Fits true to size, take your normal size\r\n	•	Designed to be worn fitted', 'Unisex', 1),
('JKneePatch', 2, 'Knee Patch Jeans', 'UNDEFEATED', 200000, 10, 8, 'J-Knee Patch1.jpg', 'J-Knee Patch1.jpg', 'Founded in 1978, Diesel has evolved from a pioneer in denim to a leader in the world of premium casual wear. The Italian label is famous for their influential line of jeans including an unconventional array of faded, frayed, dyed and distressed styles. Beyond denim, Diesel produces a dynamic range of t-shirts, sneakers, leather jackets and other indisputably cool basics. Since the day of its creation, Diesel’s philosophy has remained the same: Renzo Rosso had envisaged a brand that would stand for passion, individuality and self-expression.\r\n	•	Stretch-cotton\r\n	•	Five pockets\r\n	•	Panelling\r\n	•	93% cotton, 7% polyurethane\r\n	•	', 'Unisex', 1),
('tesdataproduk', 1, 'berghaus-hybrid', 'berghaus', 450000, 10, 0, '', '', 'ok', 'unisex', 1),
('tesdataproduk2', 1, 'berghaus-shirt', 'berghaus', 1200000, 10, 0, 'file_1479806246.jpg', '', 'tes ulang', 'unisex', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile`
--

CREATE TABLE IF NOT EXISTS `tbl_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `manajer` varchar(20) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  `otonomi` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `desa_kel` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `faxsimile` varchar(20) NOT NULL,
  `foto_profile` varchar(100) NOT NULL,
  `website` text NOT NULL,
  `email` text NOT NULL,
  `naungan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_profile`
--

INSERT INTO `tbl_profile` (`id`, `nama`, `manajer`, `provinsi`, `otonomi`, `kecamatan`, `desa_kel`, `alamat`, `kode_pos`, `telepon`, `faxsimile`, `foto_profile`, `website`, `email`, `naungan`) VALUES
(1, 'LKLTSK', '132142132', 'JAWA BARAT', 'KOTA TASIKMALAYA', 'TAWANG', 'KAHURIPAN', 'JL. Rumah Sakit Umum No. 28', 46115, '(0265) 331690', '(0265) 314861', '449e9b2bbbc75b576b3cdcb8886420f7_400x400.png', 'www.sman1-tasik.sch.id', 'info@sman1-tasik.sch.id', 'KEMENTERIAN PENDIDIKAN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_rating` (
  `rt_id` int(11) NOT NULL,
  `rt_kode_produk` varchar(50) NOT NULL,
  `rt_total_rates` int(11) NOT NULL,
  `rt_total_points` decimal(10,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`rt_id`, `rt_kode_produk`, `rt_total_rates`, `rt_total_points`) VALUES
(1, 'SDL000015', 1, '5.0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating_users`
--

CREATE TABLE IF NOT EXISTS `tbl_rating_users` (
  `rtu_id` int(11) NOT NULL,
  `rtu_rate_id` int(11) NOT NULL COMMENT 'kode_produk',
  `rtu_user_id` int(11) NOT NULL COMMENT 'id user',
  `rtu_created` int(11) NOT NULL COMMENT 'tgl dibuat'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slideshow`
--

CREATE TABLE IF NOT EXISTS `tbl_slideshow` (
  `kode_banner` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `stts` varchar(1) NOT NULL,
  PRIMARY KEY (`kode_banner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_slideshow`
--

INSERT INTO `tbl_slideshow` (`kode_banner`, `judul`, `deskripsi`, `gambar`, `stts`) VALUES
(1, 'online store clothing', '', 'banner1-1140x420.jpg', '1'),
(2, 'online store clothing', '', 'banner2-1140x420.jpg', '1'),
(3, 'online store clothing', '', 'YS3_28_banner-homepage.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_spr_admn`
--

CREATE TABLE IF NOT EXISTS `tbl_spr_admn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_spr_admn` varchar(100) NOT NULL,
  `username_admn` varchar(50) NOT NULL,
  `pass_admn` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `nama_admn` varchar(100) NOT NULL,
  `stts` varchar(20) NOT NULL,
  `lvl` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_lahir` varchar(50) NOT NULL,
  `tmp_lahir` varchar(50) DEFAULT NULL,
  `foto` varchar(2000) DEFAULT NULL,
  `no_telp` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kode_spr_admn` (`kode_spr_admn`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_spr_admn`
--

INSERT INTO `tbl_spr_admn` (`id`, `kode_spr_admn`, `username_admn`, `pass_admn`, `text`, `nama_admn`, `stts`, `lvl`, `email`, `alamat`, `tgl_lahir`, `tmp_lahir`, `foto`, `no_telp`) VALUES
(2, '001321', 'admin', '67f1b7d102b27c1a0e2cbf96e934dc6d', 'kacang', 'Aan Hasanudin x', '1', '0', 'aan.h.maulanax@gmail.com', 'Jln Dewi Madri 4 x', '1987-10-23', 'Jakartax', '14573050_10202175284138889_6097663077309258425_n.jpg', '085768349833'),
(16, 'P-0022', 'da', '202cb962ac59075b964b07152d234b70', '123', 'da', '1', '1', 'aan.h.maulana@gmail.com', 'asdkjfas', '2016-12-12', 'kasjfd', 'file_1480785011.jpg', '992342029402');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE IF NOT EXISTS `tbl_submenu` (
  `smenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(5) NOT NULL,
  `nama_smenu` varchar(100) NOT NULL,
  `slink` varchar(50) NOT NULL,
  `sicon` varchar(50) NOT NULL,
  `anak` int(1) NOT NULL,
  `sstatus` tinyint(1) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`smenu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`smenu_id`, `parent`, `nama_smenu`, `slink`, `sicon`, `anak`, `sstatus`, `level`) VALUES
(1, 2, 'Profile', 'profileadm', '#', 0, 1, 1),
(2, 2, 'History', '#', '#', 1, 0, 1),
(19, 7, 'Info Transaksi', 'd_transaksi', '#', 0, 1, 0),
(17, 4, 'Set Kategori', 'set_kategori', '#', 0, 1, 1),
(18, 5, 'Data Member', 'd_member', '#', 0, 1, 1),
(16, 3, 'Data Slide Banner', 'd_slide', '#', 0, 1, 1),
(9, 6, 'Hak Akses', 'hak_akses', '#', 0, 1, 1),
(10, 8, 'Profile Perusahaan', 'profilecom', '#', 0, 1, 1),
(20, 8, 'Lihat ', '', '', 0, 0, 0),
(15, 3, 'Data Produk', 'd_prod', '#', 0, 1, 1),
(13, 9, 'Data Pengguna', 'd_user', '#', 0, 1, 1),
(14, 9, 'Tambah Pengguna', 'd_user/add', '#', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenux`
--

CREATE TABLE IF NOT EXISTS `tbl_submenux` (
  `smenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parentx` int(5) NOT NULL,
  `nama_smenux` varchar(100) NOT NULL,
  `slinkx` varchar(50) NOT NULL,
  `siconx` varchar(50) NOT NULL,
  `sstatusx` tinyint(1) NOT NULL,
  `levelx` int(11) NOT NULL,
  `urut` int(11) NOT NULL,
  PRIMARY KEY (`smenu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonial` (
  `id_testi` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  `status` int(1) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  PRIMARY KEY (`id_testi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi_detail` (
  `kode_transaksi_detail` int(50) NOT NULL AUTO_INCREMENT,
  `kode_transaksi` bigint(150) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(150) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `rate_user_id` int(11) NOT NULL,
  `rate_created` int(11) NOT NULL,
  `rate_user_point` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`kode_transaksi_detail`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_transaksi_detail`
--

INSERT INTO `tbl_transaksi_detail` (`kode_transaksi_detail`, `kode_transaksi`, `kode_produk`, `nama_produk`, `harga`, `jumlah`, `rate_user_id`, `rate_created`, `rate_user_point`) VALUES
(1, 20111027001, 'SDL000015', 'Sandal Wanita Hak Tinggi Cokelat', '100000', 5, 1, 0, '5'),
(14, 20161208003, 'JCargoZipMoto', 'Cargo Zip Moto Jeans', '200000', 1, 0, 0, NULL),
(13, 20161208002, 'JCargoZipMoto', 'Cargo Zip Moto Jeans', '200000', 1, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_header`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi_header` (
  `kode_transaksi` bigint(150) NOT NULL,
  `kode_user` int(20) NOT NULL,
  `nama_penerima` varchar(150) NOT NULL,
  `email_penerima` varchar(150) NOT NULL,
  `alamat_penerima` text NOT NULL,
  `propinsi` varchar(150) NOT NULL,
  `kota` varchar(150) NOT NULL,
  `kodepos` varchar(100) NOT NULL,
  `telpon` varchar(20) NOT NULL,
  `metode` varchar(50) NOT NULL,
  `paket_kirim` varchar(10) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  PRIMARY KEY (`kode_transaksi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_header`
--

INSERT INTO `tbl_transaksi_header` (`kode_transaksi`, `kode_user`, `nama_penerima`, `email_penerima`, `alamat_penerima`, `propinsi`, `kota`, `kodepos`, `telpon`, `metode`, `paket_kirim`, `bank`, `pesan`) VALUES
(20111027001, 1, 'muhammad rama', 'muhammadrama@gmail.com', 'Bali', 'Bali', 'Denpasar', '283728', '083847395705', 'Setoran Tunai, Transfer Bank', 'TIKI', 'Bank Central Asia - No. Rek 1800658299', 'Selamat datang di grosirsandalonline.com.Kami tidak membebani anda dengan barang yag tidak laku atau..'),
(20161208003, 8, 'aan maulana ', 'aan.h.maulana@gmail.com', 're', 're', 'fdddd', '333', '555', 'Setoran Tunai, Transfer Bank', 'TIKI', 'Bank Central Asia - No. Rek 1800658299', '-'),
(20161208002, 8, 'aan maulana ', 'aan.h.maulana@gmail.com', 're', 're', 'fdddd', '333', '555', 'Setoran Tunai, Transfer Bank', 'TIKI', 'Bank Central Asia - No. Rek 1800658299', '-'),
(20161208001, 8, 'aan maulana ', 'aan.h.maulana@gmail.com', 're', 're', 'fdddd', '333', '555', 'Setoran Tunai, Transfer Bank', 'TIKI', 'Bank Central Asia - No. Rek 1800658299', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `kode_user` int(100) NOT NULL AUTO_INCREMENT,
  `username_user` varchar(100) NOT NULL,
  `pass_user` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `telpon` varchar(50) NOT NULL,
  `propinsi` varchar(200) NOT NULL,
  `kota` varchar(200) NOT NULL,
  `kode_pos` varchar(30) NOT NULL,
  `tgl_lahir` varchar(50) NOT NULL,
  `stts` int(1) NOT NULL,
  `kode_aktivasi` varchar(200) NOT NULL,
  `tgl_transaksi` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`kode_user`, `username_user`, `pass_user`, `nama`, `email`, `alamat`, `telpon`, `propinsi`, `kota`, `kode_pos`, `tgl_lahir`, `stts`, `kode_aktivasi`, `tgl_transaksi`) VALUES
(1, 'gede', '21232f297a57a5a743894a0e4a801fc3', 'Gede Suma Wijaya', 'go_blin@gmail.com', '<p>bali</p>', '083847395705', 'Bali', 'Denpasar', '80000', '14-11-1991', 1, '', ''),
(8, 're', '67f1b7d102b27c1a0e2cbf96e934dc6d', 'aan maulana ', 'aan.h.maulana@gmail.com', 're', '555', 're', 'fdddd', '333', '1-1-1950', 1, '9d2f81d93347d00db8f1a2ccee306979', ''),
(9, 'gds', '12eccbdd9b32918131341f38907cbbb5', 're', 'gedelumbung@gail.com', 're', '878', 're', 're', '787', '1-1-1950', 0, '202567a75aef2e66a3ebf2366bff048f', ''),
(10, 'aan_maulana', '1e0ad2ec7e8c3cc595a9ec2e3762b117', 'aan maulana hasanudin ', 'aan.h.maulana13@gmail.com', '<p>sukasari tasikmalaya x</p>', '0857935343529', 'Jawa barat ', 'Tasikmalaya ', '461513', '24-10-1987', 0, '796880d912419019151040cc0a42bf38', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usermenu`
--

CREATE TABLE IF NOT EXISTS `tbl_usermenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `menu` text NOT NULL,
  `menux` text NOT NULL,
  `oleh` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_usermenu`
--

INSERT INTO `tbl_usermenu` (`id`, `kode`, `menu`, `menux`, `oleh`, `tanggal`) VALUES
(1, '001321', '1|2|3|4|', '|', '', '0000-00-00 00:00:00'),
(3, 'P-0022', '1|16|15|18|', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login_admin`
--
CREATE TABLE IF NOT EXISTS `view_login_admin` (
`id` int(11)
,`kode` varchar(100)
,`username` varchar(50)
,`password` varchar(100)
,`nama` varchar(100)
,`email` varchar(50)
,`alamat` text
,`tgl_lahir` varchar(50)
,`lvl` varchar(20)
,`tmp_lahir` varchar(50)
,`foto` varchar(2000)
);
-- --------------------------------------------------------

--
-- Structure for view `view_login_admin`
--
DROP TABLE IF EXISTS `view_login_admin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login_admin` AS select `t_adm`.`id` AS `id`,`t_adm`.`kode_spr_admn` AS `kode`,`t_adm`.`username_admn` AS `username`,`t_adm`.`pass_admn` AS `password`,`t_adm`.`nama_admn` AS `nama`,`t_adm`.`email` AS `email`,`t_adm`.`alamat` AS `alamat`,`t_adm`.`tgl_lahir` AS `tgl_lahir`,`t_adm`.`lvl` AS `lvl`,`t_adm`.`tmp_lahir` AS `tmp_lahir`,`t_adm`.`foto` AS `foto` from `tbl_spr_admn` `t_adm`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
