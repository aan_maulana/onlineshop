jQuery(document).ready(function(){
    $('#tahunna').hide();
    $('#pertahun').hide();
    $('#tombol2').hide();
    jQuery("#tahun").change(function (){
        var year = jQuery('#tahun').val();
        if(year!=""){
            jQuery('#tombol2').show('slow');
        }else{
            jQuery('#tombol2').hide('slow');
        }
    });
    jQuery("#darithn").change(function (){
        var year = jQuery('#darithn').val();
        if(year!=""){
            jQuery('#tombol2').show('slow');
        }else{
            jQuery('#tombol2').hide('slow');
        }
    });
    jQuery("#sampaithn").change(function (){
        var year = jQuery('#sampaithn').val();
        if(year!=""){
            jQuery('#tombol2').show('slow');
        }else{
            jQuery('#tombol2').hide('slow');
        }
    });

    jQuery('.uniformsyear').click(function(){
        var checked_value = jQuery(".uniformsyear:checked").val();
        if(checked_value==0){
            jQuery('#tahunna').show('slow');
            jQuery('#pertahun').hide('slow');
            jQuery('#darithn').val('');
            jQuery('#sampaithn').val('');
            jQuery('#tombol2').hide('slow');
        }

        if(checked_value==1){
            jQuery('#tahunna').hide('slow');
            jQuery('#pertahun').show('slow');
            jQuery('#darithn').val('');
            jQuery('#sampaithn').val('');
            jQuery('#tombol2').hide('slow');
        }
    });

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    })

    jQuery("#nmnasabah").autocomplete({                
    source: function(req, add){
        $.blockUI({
                message : '<i class="fa fa-spinner fa-spin"></i> Sedang Melakukan Pengecekan Data. . .'
        });
        setTimeout(function(){
            $.unblockUI();
        },1000);
        jQuery.ajax({
            url:$BASE_URL+"latranstab/carinasabah/",
            dataType:'json',
            type:'POST',
            data:req,                                                   
            success:function(data){
                if(data.response=='true'){
                    jQuery.unblockUI();
                    add(data.message);                             
                }else{
                    jQuery.unblockUI();
                    jQuery('#nmnasabah').val('');
                    jQuery('#alamat').val('');
                    jQuery('#tlp').val('');
                    jQuery('#kode').val('');
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.responseText);
            }
        })
        },
        minLength:3,
        select: function(event,ui){
            jQuery('#nmnasabah').val(ui.item.id);
            jQuery('#kode').val(ui.item.kode);
            jQuery('#alamat').val(ui.item.alamat);
            jQuery('#tlp').val(ui.item.tlp);
            var nmnasabah = jQuery('#nmnasabah').val();
            if(nmnasabah!=""  ){
                jQuery('#tombol').show('slow');
            }else{
                jQuery('#tombol').hide('slow');
            }   
        },
    })

    jQuery("#kode").autocomplete({                
    source: function(req, add){
        $.blockUI({
                message : '<i class="fa fa-spinner fa-spin"></i> Sedang Melakukan Pengecekan Data. . .'
        });
        setTimeout(function(){
            $.unblockUI();
        },1000);
        jQuery.ajax({
            url:$BASE_URL+"latranstab/carinorek/",
            dataType:'json',
            type:'POST',
            data:req,                                                   
            success:function(data){
                if(data.response=='true'){
                    jQuery.unblockUI();
                    add(data.message);                             
                }else{
                    jQuery.unblockUI();
                    jQuery('#kode').val('');
                    jQuery('#nmnasabah').val('');
                    jQuery('#alamat').val('');
                    jQuery('#tlp').val('');
                    jQuery('#kode').val('');
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.responseText);
            }
        })
        },
        minLength:7,
        select: function(event,ui){
            jQuery('#kode').val(ui.item.id);
            jQuery('#nmnasabah').val(ui.item.nama);
            jQuery('#alamat').val(ui.item.alamat);
            jQuery('#tlp').val(ui.item.tlp);
            var nmnasabah = jQuery('#nmnasabah').val();
            if(nmnasabah!=""  ){
                jQuery('#tombol').show('slow');
            }else{
                jQuery('#tombol').hide('slow');
            }   
        },
    })

    jQuery("#nanas").autocomplete({                
    source: function(req, add){
        $.blockUI({
                message : '<i class="fa fa-spinner fa-spin"></i> Sedang Melakukan Pengecekan Data. . .'
        });
        setTimeout(function(){
            $.unblockUI();
        },1000);
        jQuery.ajax({
            url:$BASE_URL+"latranstab/carinasabah/",
            dataType:'json',
            type:'POST',
            data:req,                                                   
            success:function(data){
                if(data.response=='true'){
                    jQuery.unblockUI();
                    add(data.message);                             
                }else{
                    jQuery.unblockUI();
                    jQuery('#nanas').val('');
                    jQuery('#alamatnas').val('');
                    jQuery('#tlpnas').val('');
                    jQuery('#konas').val('');
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.responseText);
            }
        })
        },
        minLength:3,
        select: function(event,ui){
            jQuery('#nanas').val(ui.item.id);
            jQuery('#konas').val(ui.item.kode);
            jQuery('#alamatnas').val(ui.item.alamat);
            jQuery('#tlpnas').val(ui.item.tlp);
            var konas = jQuery('#konas').val();
            var nanas = jQuery('#nanas').val();
            var danas = jQuery('#danas').val();
            var sanas = jQuery('#sanas').val();
            if(nanas!="" && konas !="" && danas!="" && sanas!="" ){
                jQuery('#tombol').show('slow');
            }else{
                jQuery('#tombol').hide('slow');
            }   
        },
    })

    jQuery("#konas").autocomplete({                
    source: function(req, add){
        $.blockUI({
                message : '<i class="fa fa-spinner fa-spin"></i> Sedang Melakukan Pengecekan Data. . .'
        });
        setTimeout(function(){
            $.unblockUI();
        },1000);
        jQuery.ajax({
            url:$BASE_URL+"latranstab/carinorek/",
            dataType:'json',
            type:'POST',
            data:req,                                                   
            success:function(data){
                if(data.response=='true'){
                    jQuery.unblockUI();
                    add(data.message);                             
                }else{
                    jQuery.unblockUI();
                    jQuery('#konas').val('');
                    jQuery('#nanas').val('');
                    jQuery('#alamatnas').val('');
                    jQuery('#tlpnas').val('');
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.responseText);
            }
        })
        },
        minLength:7,
        select: function(event,ui){
            jQuery('#konas').val(ui.item.id);
            jQuery('#nanas').val(ui.item.nama);
            jQuery('#alamatnas').val(ui.item.alamat);
            jQuery('#tlpnas').val(ui.item.tlp);
            var nanas = jQuery('#nmnasabah').val();
            if(nmnasabah!=""  ){
                jQuery('#tombol').show('slow');
            }else{
                jQuery('#tombol').hide('slow');
            }   
        },
    })    

    function printrekap(page){
        if(jQuery('#y').is(':checked')) {
            if(jQuery("#tahun").val()!=""){
                var tahun = jQuery("#tahun").val();
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 0.5, 
                        color: '#fff' 
                    },
                    message : 'Sedang Melakukan Pengecekan Data, Mohon menunggu ... '
                });
                jQuery.ajax({
                        url     : $BASE_URL+page+"/pertahun/"+tahun,
                        type        : 'POST',
                        success: function(msg){
                  jQuery.unblockUI();
                  window.location.href = $BASE_URL+page+"/pertahun/"+tahun;
                        }
                    });
                    return false;
            }else{
                jQuery.unblockUI();
                $.gritter.add({title:"Informasi Rekap Tabungan!",text: " Pastikan Rekap Laporan Tahun Sudah Terpilih !"});return false;
            }
        }
    }
    
});
