var host = window.location.host;
$BASE_URL = 'http://'+host+'/';    
jQuery("#kdkp").autocomplete({                
	source: function(req, add){
	    $.blockUI({
				message : '<i class="fa fa-spinner fa-spin"></i> Sedang Melakukan Pengecekan Data. . .'
		});
		setTimeout(function(){
            $.unblockUI();
        },1000);
	    jQuery.ajax({
	        url:$BASE_URL+"cabang/carikepala/",
	        dataType:'json',
	        type:'POST',
	        data:req,                                                   
	        success:function(data){
	            if(data.response=='true'){
	                jQuery.unblockUI();
	                add(data.message);                             
	            }else{
	                jQuery.unblockUI();
	                jQuery('#nm').val('');
	                jQuery('#kdkp').val('');
	            }
	        },
	        error:function(XMLHttpRequest){
	            alert(XMLHttpRequest.responseText);
	        }
	    })
	},
	minLength:3,
	select: function(event,ui){
	    jQuery('#nm').val(ui.item.id);
        jQuery('#kdkp').val(ui.item.kode);
	},
})