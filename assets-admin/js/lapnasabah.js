jQuery("#sasih").change(function (){
	var bulan = jQuery('#sasih').val();
	if(bulan!=""){
		jQuery('#tombol').show('slow');
	}else{
		jQuery('#tombol').hide('slow');
	}
});
jQuery("#tanggalna").change(function (){
	var bulan = jQuery('#tanggalna').val();
	if(bulan!=""){
		jQuery('#tombol').show('slow');
	}else{
		jQuery('#tombol').hide('slow');
	}
});
jQuery("#sampai").change(function (){
	var bulan = jQuery('#sampai').val();
	if(bulan!=""){
		jQuery('#tombol').show('slow');
	}else{
		jQuery('#tombol').hide('slow');
	}
});
jQuery("#nmkonsumen").change(function (){
	var nmkonsumen = jQuery('#nmkonsumen').val();
	if(nmkonsumen!=""  ){
		jQuery('#tombol').show('slow');
	}else{
		jQuery('#tombol').hide('slow');
	}
});

jQuery("#bulan").hide();
jQuery("#periode").hide();
jQuery("#tanggal").hide();
jQuery("#konsumen").hide();
jQuery(".uniforms").click(function (){
    var checked_value = jQuery(".uniforms:checked").val();
    if(checked_value==1){
        jQuery("#bulan").show("slow");
        jQuery("#tanggal").hide("slow");
        jQuery("#periode").hide("slow");
        jQuery("#konsumen").hide("slow");
		jQuery('#tombol').hide('slow');
		jQuery('#sasih').val('');
		jQuery('#tanggalna').val('');
		jQuery('#dari').val('');
		jQuery('#sampai').val('');
		jQuery('#tlp').val('');
		jQuery('#alamat').val('');
		jQuery('#nmkonsumen').val('');
		jQuery("#sasih").trigger("liszt:updated");
    }
    if(checked_value==0){
        jQuery("#bulan").hide("slow");
        jQuery("#periode").show("slow");
        jQuery("#tanggal").hide("slow");
		jQuery("#konsumen").hide("slow");
		jQuery('#tombol').hide('slow');
		jQuery('#sasih').val('');
		jQuery('#tanggalna').val('');
		jQuery('#dari').val('');
		jQuery('#sampai').val('');
		jQuery('#nmkonsumen').val('');
		jQuery('#tlp').val('');
		jQuery('#alamat').val('');
		jQuery("#sasih").trigger("liszt:updated");
    }
    if(checked_value==2){
        jQuery("#bulan").hide("slow");
        jQuery("#periode").hide("slow");
        jQuery("#tanggal").show("slow");
        jQuery("#konsumen").hide("slow");
		jQuery('#tombol').hide('slow');
    	jQuery('#sasih').val('');
		jQuery('#tanggalna').val('');
		jQuery('#dari').val('');
		jQuery('#sampai').val('');
		jQuery('#nmkonsumen').val('');
		jQuery('#tlp').val('');
		jQuery('#alamat').val('');
		jQuery("#sasih").trigger("liszt:updated");
    }
    if(checked_value==3){
        jQuery("#bulan").hide("slow");
        jQuery("#periode").hide("slow");
        jQuery("#tanggal").hide("slow");
		jQuery('#tombol').hide('slow');
		jQuery('#konsumen').show('slow');
    	jQuery('#sasih').val('');
		jQuery('#tanggalna').val('');
		jQuery('#dari').val('');
		jQuery('#sampai').val('');
		jQuery('#nmkonsumen').val('');
		jQuery('#tlp').val('');
		jQuery('#alamat').val('');
		jQuery("#sasih").trigger("liszt:updated");
    }
});


function repall(page){
    var $j = jQuery.noConflict();
    if($j('#m').is(':checked')) {
        if($j("#sasih").val()!=""){
            var sasih = $j("#sasih").val();
            $j.ajax({
                url : $BASE_URL+page+"/perbulan/"+sasih,
                type : 'POST',
            	success: function(msg){
                    window.location.href = $BASE_URL+page+"/perbulan/"+sasih 
            	}
        });
        return false;   
        }else{
            notif({
                type: "error",
                msg: "Pastikan Tipe Laporan Sudah Terpilih !",
                position: "center",
                width: 800,
                height: 60,
                autohide: true
            });
        }
    }else if($j('#nm').is(':checked')){
        if($j("#dari").val()!="" || $j("#sampai").val()!=""){
            var dari = $j("#dari").val();
            var sampai = $j("#sampai").val();
            $j.ajax({
                url : $BASE_URL+page+"/periode/"+dari+"/"+sampai,
                type : 'POST',
            //  data : kode,
            success: function(msg){
                window.location.href = $BASE_URL+page+"/periode/"+dari+"/"+sampai 
            }
        });
        return false;   
        }else{
            notif({
                type: "error",
                msg: "Pastikan Tipe Laporan Sudah Terpilih",
                position: "center",
                width: 800,
                height: 60,
                autohide: true
            });
        }
    }else if($j('#mm').is(':checked')){
        if($j("#tanggalna").val()!=""){
            var tgl = $j("#tanggalna").val();
            $j.ajax({
                url : $BASE_URL+page+"/pertanggal/"+tgl,
                type : 'POST',
            //  data : kode,
            success: function(msg){
                window.location.href = $BASE_URL+page+"/pertanggal/"+tgl 
            }
        
        });
        return false;   
        }else{
            notif({
                type: "warning",
                msg: "Pastikan Tipe Laporan Sudah Terpilih",
                position: "center",
                width: 800,
                height: 60,
                autohide: true
            });
        }
    }else if($j('#mn').is(':checked')){
        if($j("#kode").val()!=""){
            var tgl = $j("#kode").val();
            $j.ajax({
                url : $BASE_URL+page+"/perkonsumen/"+tgl,
                type : 'POST',
            //  data : kode,
            success: function(msg){
                window.location.href = $BASE_URL+page+"/perkonsumen/"+tgl 
            }
        
        });
        return false;   
        }else{
            notif({
                type: "warning",
                msg: "Pastikan Tipe Laporan Sudah Terpilih",
                position: "center",
                width: 800,
                height: 60,
                autohide: true
            });
        }
    }
}
    



