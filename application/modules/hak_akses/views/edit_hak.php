<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />

<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>

<div class="row">

    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-validation-2">

        <div class="panel-heading">

            <div class="panel-heading-btn">

                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>

                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

            </div>

            <h4 class="panel-title"><?php echo $halaman;?></h4>

        </div>

         <div class="panel-body">

                <p style="text-align:center">

                    Informasi Hak Akses <b><?php echo $nama;?></b>

                </p>

                <div>

                    <div data-scrollbar="true" data-height="650px">

                      <form class="form-horizontal form-bordered" action="<?php echo base_url(); ?>hak_akses/<?php echo $action;?>" method="post" data-parsley-validate="true">

                        <div class="form-group">

                          <label class="control-label col-md-2 col-sm-2">Menu * :</label>

                          <div class="col-md-6 col-sm-6">

                            <span class="field">

                                <ul style="list-style-type:none;padding-top:5px">

                                <?php

                                  if(!empty($menunya)){

                                     $idmenu = explode("|", $menunya);

                                     $idmenux = explode("|", $submenu);

                                  }

                                  $i=0;

                                  $mutama = $this->db->query("SELECT * FROM tbl_menu WHERE status = '1' ORDER BY urutan")->result();

                                  foreach ($mutama as $key ) {

                                    $i++;

                                    echo '<li><strong>' . "- " . $key->nama_menu . "</strong></li>". "\n";

                                    $smenu = $this->db->get_where('tbl_submenu',array('parent'=>$key->menu_id,'level'=>'1','sstatus'=>'1'))->result();

                                    echo '<ul style="list-style-type:none;padding-bottom:10px">' . "\n";

                                    foreach ($smenu as $keys) {

                                      // $anak = $keys->anak;

                                      // if($anak!='1'){

                                        echo '<li><input type="checkbox" class="submenu" name="submenu[]" id="submenu'. $i .'" value="'. $keys->smenu_id . '"';

                                        if(!empty($menunya)){

                                          for($i=0;$i<count($idmenu);$i++){

                                            if($idmenu[$i]==$keys->smenu_id){

                                              echo " checked";

                                            }

                                          }

                                        }

                                        echo '>&nbsp;&nbsp;' . $keys->nama_smenu . "</li>" . "\n";

                                      // }else{

                                      //   echo '<li><strong>' . $keys->nama_smenu . "</strong></li>". "\n";

                                      // }

                                  $xx = 0;

                                  $smenux = $this->db->query("SELECT * FROM tbl_submenux WHERE parentx = '$keys->smenu_id' AND levelx = '1' AND sstatusx = '1' ORDER BY urut")->result();

                                  echo '<ul style="list-style-type:none;padding-bottom:10px">' . "\n";

                                  foreach ($smenux as $keysz) {

                                    $xx++;

                                    echo '<li><input type="checkbox" class="submenux" name="submenux[]" id="submenux'. $xx .'" value="'. $keysz->smenu_id . '"';

                                    if(!empty($menunya)){

                                      for($xx=0;$xx<count($idmenux);$xx++){

                                        if($idmenux[$xx]==$keysz->smenu_id){

                                          echo " checked";

                                        }

                                      }

                                    }

                                    echo '>&nbsp;&nbsp;' . $keysz->nama_smenux . "</li>" . "\n";

                                  }

                                  echo "</ul>" . "\n";

                                    }

                                    echo "</ul>" . "\n";

                                  }

                                ?>

                                </ul>

                           </span>

                          </div>

                        </div>

                        <div class="form-group">

                          <label class="control-label col-md-2 col-sm-2"></label>

                          <div class="col-md-2 col-sm-2">

                            <button type="submit" class="btn btn-success"><?php echo $tombolsimpan;?></button>

                            <button type="button" onclick="history.go(-1)" class="btn btn-info"><?php echo $tombolbatal ; ?></button>

                          </div>

                        </div>

                      </form>

                    </div>

                  </div>

                </div>

        </div>

    </div>

  </div>

</div>

