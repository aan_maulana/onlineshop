<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>

<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>

<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>

<script>

$(document).ready(function() {

    TableManageResponsive.init();

});

</script>    

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-inverse">

            <div class="panel-heading">

                <div class="panel-heading-btn">

                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>

                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

                </div>

                <h4 class="panel-title"><?php echo $halaman;?></h4>

            </div>

            <div class="panel-body">
              <div class="table-responsive">      
                <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">

                    <thead>

                        <tr>

                            <th style="text-align:center" width="1%">No.</th>

                            <th style="text-align:center" width="10">Foto</th>

                            <th style="text-align:center" width="25%">Nama</th>

                            <th style="text-align:center" width="15%">Hak Akses</th>

                            <th style="text-align:center" width="5%">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                    <?php

                    $ii= 0;

                    $staff = $this->db->get_where('tbl_spr_admn',array('lvl'=>'1'))->result();

                    foreach ($staff as $key) {

                        $ii++;

                        $kode = $key->kode_spr_admn;

                        ?>

                        <tr class="odd gradeX">

                            <td width="1%" style="text-align:center"><?php echo $ii . "." ;?></td>

                            <td width="10%" style="text-align:center"><a class="fancybox" href="<?php echo base_url();?>assets-admin/foto/pegawai/<?php echo $key->foto;?>" style="width:80px;text-align:center;height:80px;" title="<?php echo $key->nama_admn;?>"><img src="<?php echo base_url();?>assets-admin/foto/pegawai/<?php echo $key->foto;?>" style="width:71px;" alt="" /></a></td>

                            <td width="20%"><?php echo $key->nama_admn;?></td>

                            <td width="40%">

                                <?php

                                $hakna = $this->db->get_where('tbl_usermenu',array('kode'=>$key->kode_spr_admn))->result();

                                foreach ($hakna as $asik) {

                                    $hakmenu = $asik->menu;

                                    $haksubmenu = $asik->menux;

                                    $mnids = explode("|", $hakmenu);

                                    $mnidsx = explode("|", $haksubmenu);

                                    for ($i=0; $i < count($mnids)-1; $i++) { 

                                        $menusub = $this->db->query("SELECT * FROM tbl_submenu WHERE smenu_id = '$mnids[$i]' AND sstatus = '1' AND level = '1'")->result();

                                        foreach ($menusub as $key ) {  

                                            $hakmenuna =$key->nama_smenu;

                                            $parent = $key->parent;

                                            ?>

                                            <?php echo "<span class=\"label label-success\">" . $hakmenuna . "</span>";?>

                                            <?php

                                        }

                                    }

                                    for ($ix=0; $ix < count($mnidsx)-1; $ix++) { 

                                        $menusubx = $this->db->get_where('tbl_submenux',array('smenu_id'=>$mnidsx[$ix],'sstatusx'=>'1','levelx'=>'1'))->result();

                                        foreach ($menusubx as $keyx ) {  

                                            $hakmenunax =$keyx->nama_smenux;

                                            ?>

                                            <?php echo "<span class=\"label label-success\">". $hakmenunax ."</span>";?>

                                            <?php

                                        }

                                    }  

                                }

                                ?>

                            </td>

                            <td style="text-align:center" width="10%">

                                <a href="javascript:void(0)" onclick="edit(<?php echo "'" .$kode . "'";?>,<?php echo "'" .$halaman . "'";?>,<?php echo "'" .$link . "'";?>,<?php echo "'" .$actionedit . "'";?>)" data-toggle="tooltip" class="btn btn-warning btn-sm" title='Edit Data'><i class="icon-pencil icon-white"></i></a>

                            </td>

                        </tr>

                        <?php

                    }

                    ?>

                    </tbody>

                </table>
               </div>     
            </div>

        </div>

    </div>

</div>