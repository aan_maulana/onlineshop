<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hak_akses extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		// $this->load->model('d_member/member_model');
 	}

 	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}

	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'tools';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Hak Akses';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "hak_akses";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "hak_akses";
					$isi['namamenu'] = "Hak Akses";
					$isi['page'] = "hak_akses";
					$isi['link'] = 'hak_akses';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Data Hak Akses Pengguna";
					$isi['judul'] = "Halaman";
					$isi['content'] = "akses_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}

	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'hak_akses';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
							$isi['nama'] = $key->nama_admn;
						}

						$this->session->set_userdata('ses_admin_akses',$kode);

						$umenu = $this->db->get_where('tbl_usermenu',array('kode'=>$kode))->result();
						foreach ($umenu as $key) {
							$isi['menunya'] = $key->menu;
							$isi['submenu'] = $key->menux;
						}
						$isi['cek']	 = "edit";
						$isi['kelas'] = "hak_akses";
						$isi['namamenu'] = "Edit Hak Akses";
						$isi['page'] = "hak_akses";
						$isi['link'] = 'hak_akses';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Data Akses";
						$isi['judul'] = "Halaman Data Akses";
						$isi['content'] = "edit_hak";
						
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}

	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'hak_akses';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$ckdata = $this->db->get_where('tbl_usermenu',array('kode'=>$this->session->userdata('kodena')))->result();

                    if(count($ckdata)>0){

                        $out ="";

                        $smenu = $this->input->post('submenu');

                        for ($i=0; $i < count($smenu) ; $i++) { 

                            $out .= $smenu[$i] . "|";

                        }

                        $in = "";

                        $smenux = $this->input->post('submenux');

                        for ($ix=0; $ix < count($smenux) ; $ix++) { 

                            $in .= $smenux[$ix] . "|";

                        }

                        $dt = array('menu'=>$out,'menux'=>$in);

                        $this->db->where('kode',$this->session->userdata('kodena'));

                        $this->db->update('tbl_usermenu', $dt);     

                        $this->session->unset_userdata('kodena');

                    }else{

                        $out ="";

                        $smenu = $this->input->post('submenu');

                        for ($i=0; $i < count($smenu) ; $i++) { 

                            $out .= $smenu[$i] . "|";

                        }

                        $in = "";

                        $smenux = $this->input->post('submenux');

                        for ($ix=0; $ix < count($smenux) ; $ix++) { 

                            $in .= $smenux[$ix] . "|";

                        }

                        $dt = array('kode'=>$this->session->userdata('kodena'),'menu'=>$out,'menux'=>$in);

                        $this->db->insert('tbl_usermenu', $dt);     

                        $this->session->unset_userdata('kodena');
                    }
                    	redirect('hak_akses','refresh');

				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}


	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'hak_akses';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$data['say'] = "ok";
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

} 	