<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>
<script>
$(document).ready(function() {
    TableManageResponsive.init();
    var host = window.location.host;
    $BASE_URL = 'http://'+host+'/';  
    $('#data-user').dataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp = $('td:eq(0)', nRow).text();
            var temp = temp.split('|');
            var no = temp[0]+".";
            var kode = temp[1];
            var nama = $('td:eq(1)', nRow).text();
            var tl = $('td:eq(2)', nRow).text();
            var tgl = $('td:eq(3)', nRow).text();
            var hp = $('td:eq(4)', nRow).text();
            var stts = $('td:eq(5)', nRow).text();
            var foto = $('td:eq(6)', nRow).text();
            var fotox = '<a class="fancybox" href="<?php echo base_url();?>assets-admin/foto/pegawai/'+foto+'" style="width:80px;text-align:center;height:80px;" title="'+nama+'"><img src="<?php echo base_url();?>assets-admin/foto/pegawai/'+foto+'" style="width:71px;" alt=""></a>'
            var action = '<center><a href="javascript:void(0)" onclick="hapus('+"'"+kode+"'"+',\'Data User\',\'d_user\',\'hapus\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Hapus Data"><i class="icon-remove icon-white"></i></a> '      +      ' <a href="javascript:void(0)" onclick="edit('+"'"+kode+"'"+',\'Data User\',\'d_user\',\'edit\')" data-toggle="tooltip" class="btn btn-warning btn-sm" title="Edit Data"><i class="icon-pencil icon-white"></i></a></center>';
            if(stts=="1"){
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\','+"'"+kode+"'"+',\'Data User\',\'d_user\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-info btn-sm" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>';
            }else{
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\','+"'"+kode+"'"+',\'Data User\',\'d_user\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Status NonAktif"><i class="fa fa-lock icon-white"></i></a>';
            }
            $('td:eq(0)', nRow).html(no);
            $('td:eq(1)', nRow).html(fotox);
            $('td:eq(2)', nRow).html(kode);
            $('td:eq(3)', nRow).html(nama);
            $('td:eq(4)', nRow).html(tl);
            $('td:eq(5)', nRow).html(tgl);
            $('td:eq(6)', nRow).html(hp);
            $('td:eq(7)', nRow).html(status);
            $('td:eq(8)', nRow).html(action);
            $('td:eq(0),td:eq(3),td:eq(4)', nRow).css('text-align','center');
        },
        "bAutoWidth": false,
        "aoColumns": [
            { "sWidth": "1%" },
            { "sWidth": "10%" },
            { "sWidth": "20%" },
            { "sWidth": "20%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "1%" }
        ],
        "bProcessing": false,
        "bServerSide": true,
        "responsive":false,
        "sAjaxSource": $BASE_URL+"d_user/get_data"
    });
    $('#data-nasabah').each(function(){
        var datatable = $(this);
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    });
});
</script>    
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="<?php echo base_url();?><?php echo $link;?>/add" title="Tambah <?php echo $halaman;?>" class="btn btn-primary btn-xs m-r-5"><i class="icon-plus-sign"></i> Tambah Data</a>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table id="data-user" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">Photo</th>
                                <th style="text-align:center" width="10%">Kode Pengguna</th>
                                <th style="text-align:center" width="20%">Nama</th>
                                <th style="text-align:center" width="20%">Tempat Lahir</th>
                                <th style="text-align:center" width="10%">Tanggal Lahir</th>
                                <th style="text-align:center" width="10%">Handphone</th>
                                <th style="text-align:center" width="9%">Status</th>
                                <th style="text-align:center" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>