<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    <div class="panel-body panel-form">
		        <form class="form-horizontal form-bordered" action="<?php echo base_url();?>d_user/<?php echo $action;?>" method="post" data-parsley-validate="true" enctype="multipart/form-data" >
					<?php
                    if($cek==='edit'){
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Foto Aset :</label>
                            <div class="col-md-2 col-sm-2">
                            <?php
                                if($foto==""){
                                    $fotox = "no.jpg";
                                }else{
                                    $fotox = $foto;
                                }
                            ?>	
                                <a class="fancybox" href="<?php echo base_url()?>assets-admin/foto/pegawai/<?php echo $fotox?>" style="width:150px;text-align:center;height:180px;">
                                    <img class="fancybox" src="<?php echo base_url();?>assets-admin/foto/pegawai/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
                                </a>
                            </div>
                        </div>
                        <?php   
                    }
                    ?>
                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Username * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="username" minlength="2" name="username" value="<?php echo set_value('username',isset($default['username']) ? $default['username'] : ''); ?>" data-type="username" placeholder="Masukan username member" data-parsley-required="true" data-parsley-minlength="2"/>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="control-label col-md-3 col-sm-3">password * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="password"  name="password" value="<?php echo set_value('password',isset($default['password']) ? $default['password'] : ''); ?>" data-type="password" placeholder="Masukan password member">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Nama Lengkap * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="nama" minlength="2" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" placeholder="Masukan Nama Nasabah" data-parsley-required="true" data-parsley-minlength="2"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Tempat Lahir * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="tmp_lhr" minlength="5" name="tmp_lhr" value="<?php echo set_value('tmp_lhr',isset($default['tmp_lhr']) ? $default['tmp_lhr'] : ''); ?>" data-type="tmp_lhr" placeholder="Masukan Tempat Lahir" data-parsley-required="true" data-parsley-minlength="5"/>
						</div>
					</div>
					<div class="form-group">                                        
						<label class="control-label col-md-3 col-sm-3">Tanggal Lahir * :<br/><small><b>* Format dd-mm-yyyy</b></small></label>                                        
						<div class="col-md-3 col-sm-3">                                             
							<div class="input-group date" id="datepicker-default" data-date-format="dd-mm-yyyy">                                                
								<input type="text" class="form-control" data-parsley-group="wizard-step-2" name="tgl_lahir" value="<?php echo set_value('tgl_lahir',isset($default['tgl_lahir']) ? $default['tgl_lahir'] : ''); ?>" data-type="tgl_lahir" data-parsley-required="true"/><span class="input-group-addon"><i class="fa fa-calendar"></i></span>                                            
							</div>                                        
						</div>                                    
					</div>
					<div class="form-group">                                        
						<label class="control-label col-md-3 col-sm-3">Alamat * :</label>                                        
						<div class="col-md-5 col-sm-5">                                           
							 <textarea class="form-control" data-parsley-group="wizard-step-2" data-parsley-required="true" id="almt" name="almt" rows="3"><?php echo set_value('almt',isset($default['almt']) ? $default['almt'] : ''); ?></textarea>
						</div>                                    
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">No. Handphone * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="hp" minlength="2" name="hp" value="<?php echo set_value('hp',isset($default['hp']) ? $default['hp'] : ''); ?>" data-type="hp" placeholder="Masukan No Handphone" data-parsley-required="true" data-parsley-minlength="2"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Email * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="email" id="email" minlength="3" name="email" required="true" data-parsley-required="true" value="<?php echo set_value('email',isset($default['email']) ? $default['email'] : ''); ?>" data-type="email" placeholder="Masukan Email anda" data-parsley-required="true" data-parsley-minlength="3"/>
						</div>
					</div>
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Foto Profile :</label>
                        <div class="col-md-3 col-sm-3">
                            <input name="MAX_FILE_SIZE" value="1024000" type="hidden">
                            <input type="file" id="foto" name="foto" />  
                        </div>
                    </div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3"></label>
						<div class="col-md-3 col-sm-3">
							<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
						</div>
					</div>
		        </form>
		    </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	 $("#hp").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });    
</script>