<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    /*==================================== data_agama ============================================*/
    function data_user($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                SELECT a.*, CONCAT_WS('|', a.kode_spr_admn) AS add_data
                FROM tbl_spr_admn a 
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }
    function data_user_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT a.*, CONCAT_WS('|', a.kode_spr_admn) AS add_data
                FROM tbl_spr_admn a
            ) A
        ");
        return $query;
    }

    // function getMaxNasabah()
    // {
    //     $q = $this->db->query('select MAX(RIGHT(norek,5)) as kd_max from tbl_nasbah');
    //     $kd = "";
    //     if($q->num_rows() > 0)
    //     {
    //         foreach ($q->result() as $key) {
    //             $tmp = ((int) $key->kd_max) + 1;
    //             $kd = sprintf("%05s", $tmp);
    //         }
    //     }else
    //     {
    //         $kd = "00001";
    //     }
    //     return "KN".'-'.$kd;     
    // }   

}