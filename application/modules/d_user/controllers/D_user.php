<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class D_user extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('d_user/User_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'user';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Data Pengguna';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "d_user";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "user";
					$isi['namamenu'] = "Data Pengguna";
					$isi['page'] = "d_user";
					$isi['link'] = 'd_user';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Data User";
					$isi['judul'] = "Halaman Data User";
					$isi['content'] = "User_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function get_data(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$aColumns = array('kode_spr_admn','nama_admn','tmp_lahir','tgl_lahir','no_telp','stts','foto','kode_spr_admn','kode_spr_admn');
	        $sIndexColumn = "kode_spr_admn";
	        // pagings
	        $sLimit = "";
	        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
	            $sLimit = "LIMIT ".$this->kacang->anti( $_GET['iDisplayStart'] ).", ".
	                $this->kacang->anti( $_GET['iDisplayLength'] );
	        }
	        $numbering = $this->kacang->anti( $_GET['iDisplayStart'] );
	        $page = 1;
	        // ordering
	        if ( isset( $_GET['iSortCol_0'] ) ){
	            $sOrder = "ORDER BY  ";
	            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
	                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
	                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
	                        ".$this->kacang->anti( $_GET['sSortDir_'.$i] ) .", ";
	                }
	            }            
	            $sOrder = substr_replace( $sOrder, "", -2 );
	            if ( $sOrder == "ORDER BY" ){
	                $sOrder = "";
	            }
	        }
	        // filtering
	        $sWhere = "";
	        if ( $_GET['sSearch'] != "" ){
	            $sWhere = "WHERE (";
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti( $_GET['sSearch'] )."%' OR ";
	            }
	            $sWhere = substr_replace( $sWhere, "", -3 );
	            $sWhere .= ')';
	        }
	        // individual column filtering
	        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
	                if ( $sWhere == "" ){
	                    $sWhere = "WHERE ";
	                }
	                else{
	                    $sWhere .= " AND ";
	                }
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti($_GET['sSearch_'.$i])."%' ";
	            }
	        }
	        $rResult = $this->User_model->data_User($aColumns, $sWhere, $sOrder, $sLimit);
	        $iFilteredTotal = 10;
	        $rResultTotal = $this->User_model->data_User_total($sIndexColumn);
	        $iTotal = $rResultTotal->num_rows();
	        $iFilteredTotal = $iTotal;
	        $output = array(
	            "sEcho" => intval($_GET['sEcho']),
	            "iTotalRecords" => $iTotal,
	            "iTotalDisplayRecords" => $iFilteredTotal,
	            "aaData" => array()
	        );
	        foreach ($rResult->result_array() as $aRow){
	            $row = array();
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                /* General output */
	                if($i < 1)
	                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
	                else
	                    $row[] = $aRow[ $aColumns[$i] ];
	            }
	            $page++;
	            $output['aaData'][] = $row;
	        }
	        echo json_encode( $output );
	    }    
	}
	
	public function add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_user';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['cek']	 = "add";
					$isi['kelas'] = "d_user";
					$isi['namamenu'] = "Tambah Data User";
					$isi['page'] = "d_user";
					$isi['link'] = 'd_user';
					$isi['actionhapus'] = 'hapus';
					$isi['action'] = "proses_add";
					$isi['tombolsimpan'] = 'Simpan';
					$isi['tombolbatal'] = 'Batal';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Tambah Data User";
					$isi['judul'] = "Halaman Data User";
					$isi['jk'] = "";
					$isi['content'] = "d_user/form_User";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_user';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('username','username','required|min_length[2]');
		 			$this->form_validation->set_rules('password','password','required|min_length[3]');
		 			$this->form_validation->set_rules('nama','nama','required|min_length[2]');
		 			$this->form_validation->set_rules('tmp_lhr','tempat lahir','required|min_length[3]');
		 			$this->form_validation->set_rules('tgl_lahir','tanggal lahir','required');
		 			$this->form_validation->set_rules('almt','alamat','required');
		 			$this->form_validation->set_rules('hp','no handphone','required|numeric');
		 			$this->form_validation->set_rules('email','email','required');

					if ($this->form_validation->run() == TRUE){
						$ahhhhhh = $this->db->query("SELECT SUBSTR(MAX(kode_spr_admn),-6) as nona FROM tbl_spr_admn")->result();
					 		foreach ($ahhhhhh as $zzz) {
					 			$xx = substr($zzz->nona, 3, 6); 
					 		}
					 		if($xx==''){
					 			$newID = 'P-0001';
					 		}else{
					 			$noUrut = (int) substr($xx, 1, 4);
					 			$noUrut++;
					 			$newID = "P-" . sprintf("%04s", $noUrut);
					 		}

					 	$username 	= $this->kacang->anti($this->input->post('username'));
						$password 	= $this->kacang->anti($this->input->post('password'));
						$nama = $this->kacang->anti($this->input->post('nama'));
						$tmp_lahir = $this->kacang->anti($this->input->post('tmp_lhr'));
						$tgl_lahir = $this->kacang->anti(date('Y-m-d',strtotime($this->input->post('tgl_lahir'))));
						$alamat = $this->kacang->anti($this->input->post('almt'));
						$hp = $this->kacang->anti($this->input->post('hp'));
						$email = $this->kacang->anti($this->input->post('email'));
						$foto = $this->kacang->anti(str_replace(" ", "_", $_FILES['foto']['name']));
						$tgl_regis = date("Y-m-d H:i:s");
						$nmfile = "file_".time();
		 				$tmpName = $_FILES['foto']['tmp_name'];
						if($tmpName!=''){
							$config['file_name'] = $nmfile;
							$config['upload_path'] = 'assets-admin/foto/pegawai';
							$config['allowed_types'] = 'jpg|jpeg|png';
							$config['max_size'] = '10048';
							$config['max_width'] = '0';
					        $config['max_height'] = '0';
					        $config['overwrite'] = TRUE;
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if ($this->upload->do_upload('foto')){
								$gbr = $this->upload->data();

								$simpan = array('kode_spr_admn'=>$newID,'username_admn'=>$nama,'pass_admn'=>md5($password),'text'=>$password,
									'stts'=>1,'lvl'=>1,'nama_admn'=>$nama,'tmp_lahir'=>$tmp_lahir,'tgl_lahir'=>$tgl_lahir,'alamat'=>$alamat,
									  'no_telp'=>$hp,'email'=>$email,'foto'=>$gbr['file_name']
									  );


							}else{
								?>
									<script type="text/javascript">
						                alert("Pastikan Type File jpeg /jpg / png /  dan ukuran file maksimal 1MB");
						                window.location.href="<?php echo base_url();?>d_user";
									</script>
									<?php
							}		
						}else{
								$simpan = array('kode_spr_admn'=>$newID,'username_admn'=>$nama,'pass_admn'=>md5($password),'text'=>$password,
									'stts'=>1,'lvl'=>1,'nama_admn'=>$nama,'tmp_lahir'=>$tmp_lahir,'tgl_lahir'=>$tgl_lahir,'alamat'=>$alamat,
									  'no_telp'=>$hp,'email'=>$email
									  );
						}

                        //simpan data User
           //              $simpan = array('kode_spr_admn'=>$newID,'nama_admn'=>$nama,'tmp_lahir'=>$tmp_lahir,
									  // 'tgl_lahir'=>$tgl_lahir,'alamat'=>$alamat,
									  // 'no_telp'=>$hp,'email'=>$email
									  // );
                      
                        //simpan username
                        // $simpanusername = array('kode'=>$nrek,
                        //                         'username'=>$nrek,
                        //                         'password'=>md5('12345'),
                        //                         'text'=>'12345',
                        //                         'loginadmin'=>'1',
                        //                         'level'=>'1'
                        //                 );  
                        // //simpanusermenu
                        // $simpanusermenu = array('kode'=>$nrek,
                        // 						'menu'=>'2|',
                        // 						'menux'=>'17|'
                        // 				);
                        $this->db->trans_start();
						$this->db->insert('tbl_spr_admn',$simpan);
						// $this->db->insert('tbl_username',$simpanusername);
						// $this->db->insert('tbl_usermenu',$simpanusermenu);
						$this->db->trans_complete();
						redirect('d_user','refresh');
	                }else{
                        $this->add();
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_user';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					if($jns=="aktif"){
						$data = array('stts'=>'0');
					}else{
						$data = array('stts'=>'1');
					}
					$this->db->where('kode_spr_admn',$this->kacang->anti($id));	
					$this->db->update('tbl_spr_admn',$data);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function hapus($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_user';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$this->hapusfoto($kode);
							$this->db->where('kode_spr_admn',$kode);
							$this->db->where('kode_spr_admn',$this->kacang->anti($kode));
							if($this->db->delete('tbl_spr_admn')){
								$this->db->trans_start();							
								// $this->db->where('kode',$this->kacang->anti($kode));
								// $this->db->delete('tbl_username');
								$this->db->where('kode',$this->kacang->anti($kode));
								$this->db->delete('tbl_usermenu');
								$this->db->trans_complete();
									
								$data['say'] = "ok";
							}else{
								$data['say'] = "NotOk";
							}
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

	function hapusfoto($kode=null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$ckdata = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$kode))->result();
			foreach ($ckdata as $hehe) {
				$fotona = $hehe->foto;
			}
			if($fotona!=""){
				if($fotona!="no.jpg"){
					unlink('assets-admin/foto/pegawai/' . $fotona);
				}
			}
		}	
	}

	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_user';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "ok";
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}
	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_user';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
						}
						$isi['default']['kode'] 	= $key->kode_spr_admn;
						$isi['default']['nama'] 	= $key->nama_admn;
						$isi['default']['tmp_lhr'] = $key->tmp_lahir;
						$isi['default']['tgl_lahir'] = date('d-m-Y',strtotime($key->tgl_lahir));
						$isi['default']['almt'] = $key->alamat;
						$isi['default']['hp'] = $key->no_telp;
						$isi['default']['email'] = $key->email;
						$isi['foto'] = $key->foto;
						$this->session->set_userdata('kodena',$kode);
						$isi['kelas'] = "d_user";
						$isi['namamenu'] = "Edit Data User";
						$isi['page'] = "d_user";
						$isi['link'] = 'd_user';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Data User";
						$isi['judul'] = "Halaman Data User";
						$isi['content'] = "d_user/form_User";
						$isi['cek']	 = "edit";
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_user';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('nama','nama','required|min_length[2]');
		 			$this->form_validation->set_rules('tmp_lhr','tempat lahir','required|min_length[3]');
		 			$this->form_validation->set_rules('tgl_lahir','tanggal lahir','required');
		 			$this->form_validation->set_rules('almt','alamat','required');
		 			$this->form_validation->set_rules('hp','no handphone','required|numeric');
		 			$this->form_validation->set_rules('email','email','required');
					if ($this->form_validation->run() == TRUE){	
						$nama = $this->kacang->anti($this->input->post('nama'));
						$tmp_lahir = $this->kacang->anti($this->input->post('tmp_lhr'));
						$tgl_lahir = $this->kacang->anti(date('Y-m-d',strtotime($this->input->post('tgl_lahir'))));
						$alamat = $this->kacang->anti($this->input->post('almt'));
						$hp = $this->kacang->anti($this->input->post('hp'));
						$email = $this->kacang->anti($this->input->post('email'));

						$edit = array('nama_admn'=>$nama,'tmp_lahir'=>$tmp_lahir,
									  'tgl_lahir'=>$tgl_lahir,'alamat'=>$alamat,
									  'no_telp'=>$hp,'email'=>$email
									  );
						$this->db->where('kode_spr_admn',$this->kacang->anti($this->session->userdata('kodena')));
						$this->db->update('tbl_spr_admn',$edit);
						$this->session->unset_userdata('kodena');
						redirect('d_user','refresh');
	                }else{
						redirect(base_url().'d_user/edit/'.$this->session->userdata('kodena'),'refresh');
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
