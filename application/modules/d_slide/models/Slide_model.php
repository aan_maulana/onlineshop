<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slide_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    /*==================================== data slide show ============================================*/


    function data_slide($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                SELECT ts.kode_banner AS kode_banner,ts.judul AS judul,
                        ts.deskripsi AS deskripsi,ts.gambar AS gambar,ts.stts AS stts FROM tbl_slideshow ts
                        ORDER BY ts.kode_banner ASC
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }

    function data_slide_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT ts.kode_banner AS kode_banner,ts.judul AS judul,
                        ts.deskripsi AS deskripsi,ts.gambar AS gambar,ts.stts AS stts FROM tbl_slideshow ts
                        ORDER BY ts.kode_banner ASC  
            ) A
        ");
        return $query;
    }   

}