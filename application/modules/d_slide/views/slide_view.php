<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>

<script>
$(document).ready(function() {

    TableManageResponsive.init();
    var host = window.location.host;
    $BASE_URL = 'http://'+host+'/';  
    $('#data-produk').dataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp = $('td:eq(0)', nRow).text();
            var temp = temp.split('|');
            var no = temp[0]+".";
            var kode = temp[1];
            var judul = $('td:eq(1)', nRow).text();
            var deskripsi = $('td:eq(2)', nRow).text();
            var foto = $('td:eq(3)', nRow).text();
            var stts = $('td:eq(4)', nRow).text();
            var fotox = '<a class="fancybox" href="<?php echo base_url();?>assets/image/slider/'+foto+'" style="width:80px;text-align:center;height:80px;" title="'+judul+'"><img src="<?php echo base_url();?>assets/image/slider/'+foto+'" style="width:71px;" alt=""></a>'
            var action = '<center><a href="javascript:void(0)" onclick="hapus('+"'"+kode+"'"+',\'Data Slideshow\',\'d_slide\',\'hapus\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Hapus Data"><i class="icon-remove icon-white"></i></a> '      +      ' <a href="javascript:void(0)" onclick="edit('+"'"+kode+"'"+',\'Data Slideshow\',\'d_slide\',\'edit\')" data-toggle="tooltip" class="btn btn-warning btn-sm" title="Edit Data"><i class="icon-pencil icon-white"></i></a></center>';
            if(stts=="1"){
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\','+"'"+kode+"'"+',\'Data Slideshow\',\'d_slide\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-info btn-sm" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>';
            }else{
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\','+"'"+kode+"'"+',\'Data Slideshow\',\'d_slide\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Status NonAktif"><i class="fa fa-lock icon-white"></i></a>';
            }
            
            $('td:eq(0)', nRow).html(no);
            $('td:eq(1)', nRow).html(fotox);
            $('td:eq(2)', nRow).html(judul);
            $('td:eq(3)', nRow).html(deskripsi);
            $('td:eq(4)', nRow).html(status);
            $('td:eq(5)', nRow).html(action);
            $('td:eq(0),td:eq(3),td:eq(4)', nRow).css('text-align','center');
        },
        "bAutoWidth": false,
        "aoColumns": [
            { "sWidth": "1%" },
            { "sWidth": "10%" },
            { "sWidth": "20%" },
            { "sWidth": "20%" },
            { "sWidth": "10%" },
            { "sWidth": "1%" }
        ],
        "bProcessing": false,
        "bServerSide": true,
        "responsive":false,
        "sAjaxSource": $BASE_URL+"d_slide/get_data"
    });
    $('#data-nasabah').each(function(){
        var datatable = $(this);
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    });
});
</script>    
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="<?php echo base_url();?><?php echo $link;?>/add" title="Tambah <?php echo $halaman;?>" class="btn btn-primary btn-xs m-r-5"><i class="icon-plus-sign"></i> Tambah Data</a>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table id="data-produk" class="table table-striped table-bordered nowrap" width="100%">
                        <thead> 
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">Gambar Slide</th>
                                <th style="text-align:center" width="10%">Judul</th>
                                <th style="text-align:center" width="20%">Deskripsi</th>
                                <th style="text-align:center" width="20%">Status</th>
                                <th style="text-align:center" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>