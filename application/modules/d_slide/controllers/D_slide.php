<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class D_slide extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('d_slide/slide_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'mprod';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Data Slide Banner';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "d_slide";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "mprod";
					$isi['namamenu'] = "Data Slide Banner";
					$isi['page'] = "d_slide";
					$isi['link'] = 'd_slide';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Data Slide Banner";
					$isi['judul'] = "Halaman Data Slide Banner";
					$isi['content'] = "slide_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function get_data(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$aColumns = array('kode_banner','judul','deskripsi','gambar','stts','kode_banner');
	        $sIndexColumn = "kode_banner";
	        // pagings
	        $sLimit = "";
	        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
	            $sLimit = "LIMIT ".$this->kacang->anti( $_GET['iDisplayStart'] ).", ".
	                $this->kacang->anti( $_GET['iDisplayLength'] );
	        }
	        $numbering = $this->kacang->anti( $_GET['iDisplayStart'] );
	        $page = 1;
	        // ordering
	        if ( isset( $_GET['iSortCol_0'] ) ){
	            $sOrder = "ORDER BY  ";
	            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
	                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
	                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
	                        ".$this->kacang->anti( $_GET['sSortDir_'.$i] ) .", ";
	                }
	            }            
	            $sOrder = substr_replace( $sOrder, "", -2 );
	            if ( $sOrder == "ORDER BY" ){
	                $sOrder = "";
	            }
	        }
	        // filtering
	        $sWhere = "";
	        if ( $_GET['sSearch'] != "" ){
	            $sWhere = "WHERE (";
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti( $_GET['sSearch'] )."%' OR ";
	            }
	            $sWhere = substr_replace( $sWhere, "", -3 );
	            $sWhere .= ')';
	        }
	        // individual column filtering
	        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
	                if ( $sWhere == "" ){
	                    $sWhere = "WHERE ";
	                }
	                else{
	                    $sWhere .= " AND ";
	                }
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti($_GET['sSearch_'.$i])."%' ";
	            }
	        }
	        $rResult = $this->slide_model->data_slide($aColumns, $sWhere, $sOrder, $sLimit);
	        $iFilteredTotal = 10;
	        $rResultTotal = $this->slide_model->data_slide_total($sIndexColumn);
	        $iTotal = $rResultTotal->num_rows();
	        $iFilteredTotal = $iTotal;
	        $output = array(
	            "sEcho" => intval($_GET['sEcho']),
	            "iTotalRecords" => $iTotal,
	            "iTotalDisplayRecords" => $iFilteredTotal,
	            "aaData" => array()
	        );
	        foreach ($rResult->result_array() as $aRow){
	            $row = array();
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                /* General output */
	                if($i < 1)
	                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
	                else
	                    $row[] = $aRow[ $aColumns[$i] ];
	            }
	            $page++;
	            $output['aaData'][] = $row;
	        }
	        echo json_encode( $output );
	    }    
	}
	
	public function add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_slide';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['cek']	 = "add";
					$isi['kelas'] = "d_slide";
					$isi['namamenu'] = "Tambah Data Slide Banner";
					$isi['page'] = "d_slide";
					$isi['link'] = 'd_slide';
					$isi['actionhapus'] = 'hapus';
					$isi['action'] = "proses_add";
					$isi['tombolsimpan'] = 'Simpan';
					$isi['tombolbatal'] = 'Batal';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Tambah Data Slide Banner";
					$isi['judul'] = "Halaman Data Slide Banner";
					$isi['jk'] = "";
					$isi['content'] = "d_slide/form_slide";

					$isi['option_kategori']['']="-- Pilih kategori --";
					$tipe = $this->db->query("SELECT * FROM tbl_kategori where status = '1'ORDER BY id_kategori ASC")->result();
					foreach($tipe as $tah){
						$isi['option_kategori'][$tah->id_kategori] = strtoupper($tah->nama_kategori);
					}
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_slide';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){
		 			$this->form_validation->set_rules('judul','Judul Slide','required|min_length[2]');
		 			$this->form_validation->set_rules('desc','desc','required');
					if ($this->form_validation->run() == TRUE){
						$judul = $this->kacang->anti($this->input->post('judul'));
						$desc = $this->kacang->anti($this->input->post('desc'));				
						
							$foto = $this->kacang->anti(str_replace(" ", "_", $_FILES['foto']['name']));
							$nmfile = "file_".time();
				 			$tmpName = $_FILES['foto']['tmp_name'];
				 			if($tmpName!=''){
				 				$config['file_name'] = $nmfile;
								$config['upload_path'] = 'assets/image/slider/';
								$config['allowed_types'] = 'gif|jpg|jpeg|png';
								$config['max_size'] = '104800';
								$config['max_width'] = '0';
						    	$config['max_height'] = '0';
						   		$config['overwrite'] = TRUE;
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if ($this->upload->do_upload('foto')){
									$gbr = $this->upload->data();
									$simpan = array('judul'=>$judul,'stts'=>1,'deskripsi'=>$desc,
										  'gambar'=>$gbr['file_name']
										  );
								}else{
									?>
									<script type="text/javascript">
						                alert("Pastikan  gambar  type file jpeg /jpg / png / gif  dan ukuran file maksimal 10MB");
						                window.location.href="<?php echo base_url();?>d_slide/add";
									</script>
									<?php
								}
								$simpan = array('judul'=>$judul,'stts'=>1,'deskripsi'=>$desc);			 				
							$this->db->insert('tbl_slideshow',$simpan);
							redirect('d_slide','refresh');
						}	
	                }else{
                        $this->add();
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_slide';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					if($jns=="aktif"){
						$data = array('stts'=>'0');
					}else{
						$data = array('stts'=>'1');
					}
					$this->db->where('kode_banner',$this->kacang->anti($id));	
					$this->db->update('tbl_slideshow',$data);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function hapus($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_slide';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$this->hapusfoto($kode);
							$this->db->where('kode_banner',$this->kacang->anti($kode));
							if($this->db->delete('tbl_slideshow')){
								$data['say'] = "ok";
							}else{
								$data['say'] = "NotOk";
							}
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

	function hapusfoto($kode=null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$kode = $this->kacang->anti($kode);
			$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$kode))->result();
			foreach ($ckdata as $hehe) {
				$fotona = $hehe->gambar;
			}
			if($fotona!=""){
				if($fotona!="no.jpg"){
					unlink('assets/image/slider/'.$fotona);
				}
			}
		}	
	}

	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_slide';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "ok";
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}
	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_slide';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
						}
						$isi['default']['kode_slide'] 	= $key->kode_banner;
						$isi['default']['judul'] 	= $key->judul;
						$isi['default']['desc'] = $key->deskripsi;
						$isi['foto'] = $key->gambar;
						$this->session->set_userdata('kode_slide',$kode);
						$isi['cek']	 = "edit";
						$isi['kelas'] = "d_slide";
						$isi['namamenu'] = "Edit Data Slide Banner";
						$isi['page'] = "d_slide";
						$isi['link'] = 'd_slide';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Data Slide Banner";
						$isi['judul'] = "Halaman Data Slide Banner";
						$isi['content'] = "d_slide/form_slide";
						
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_slide';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('judul','Judul Slide','required|min_length[2]');
		 			$this->form_validation->set_rules('desc','desc','required');
					if ($this->form_validation->run() == TRUE){	
						$judul = $this->kacang->anti($this->input->post('judul'));
						$desc = $this->kacang->anti($this->input->post('desc'));				
						$cekcode = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$this->session->userdata('kode_slide')))->result();
						if(count($cekcode)>0){
							$foto = $this->kacang->anti(str_replace(" ", "_", $_FILES['foto']['name']));
							$nmfile = "file_".time();
				 			$tmpName = $_FILES['foto']['tmp_name'];
				 			if($tmpName!=''){
				 				$config['file_name'] = $nmfile;
								$config['upload_path'] = 'assets/image/slider/';
								$config['allowed_types'] = 'jpg|jpeg|png|gif';
								$config['max_size'] = '104800';
								$config['max_width'] = '0';
						    	$config['max_height'] = '0';
						   		$config['overwrite'] = TRUE;
						   		$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if ($this->upload->do_upload('foto')){	
									$fotokode = $this->session->userdata('kode_slide');
									$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$fotokode))->result();
									foreach ($ckdata as $hehe) {
										$fotona = $hehe->gambar;
									}
									if($fotona!=""){
										if($fotona!="no.jpg"){
											unlink('assets/image/slider/'.$fotona);
										}
									}				
														
									$gbr = $this->upload->data();
									$edit = array('judul'=>$judul,'stts'=>1,'deskripsi'=>$desc,
										  'gambar'=>$gbr['file_name']
										  );
								}else{
									?>
									<script type="text/javascript">
						                alert("Pastikan  gambar type file jpeg /jpg / png /  dan ukuran file maksimal 10MB");
						                window.location.href="<?php echo base_url();?>d_slide/edit/<?php echo $this->session->userdata('kode_slide'); ?>";
									</script>
									<?php	
								}	
						   	}else{
						   		$edit = array('judul'=>$judul,'stts'=>1,'deskripsi'=>$desc);
						   	}	
							$this->db->where('kode_banner',$this->kacang->anti($this->session->userdata('kode_slide')));
							$this->db->update('tbl_slideshow',$edit);
							$this->session->unset_userdata('kode_slide');
							redirect('d_slide','refresh');
						}	
	                }else{
						redirect(base_url().'d_slide/edit/'.$this->session->userdata('kode_slide'),'refresh');
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
