<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	
	public function _content(){
		$isi['namamenu'] = "";
		$isi['page'] = "dashboard";
		$isi['kelas'] = "dashboard";
		$isi['link'] = 'dashboard';
		$isi['halaman'] = "Dashboard";
		$isi['judul'] = "Halaman Dashboard";
		$isi['option_uker'][''] = "Pilih Unit Kerja";
		$isi['option_iker'][''] = "Pilih Instansi Kerja";
		$isi['content'] = "welcome";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function log_out(){
		$this->session->sess_destroy();
	}
	public function kalender(){
		if($this->session->userdata('login')==TRUE){
			$hmm = $this->db->query("SELECT * FROM tbl_kalender")->result_array();
			if('IS_AJAX'){
				echo json_encode($hmm);
			}
		}else{
			redirect("login","refresh");
		}
	}

	// public function credit_today(){
	// 	$curday = date('Y-m-d');
	// 	$this->db->select('tgl_trans');
	// 	$this->db->where('tgl_trans', $curday);
	// 	$this->db->where('id_jtrans', '01');	
	// 	$this->db->from('tbl_tabungan');
	// 	return $this->db->count_all_results();
	// }

	// public function wajib_today(){
	// 	$curday = date('Y-m-d');
	// 	$this->db->select('tgl_trans');
	// 	$this->db->where('tgl_trans', $curday);
	// 	$this->db->where('id_jtrans', '03');	
	// 	$this->db->from('tbl_tabungan');
	// 	return $this->db->count_all_results();
	// }

	// public function debet_today(){
	// 	$curday = date('Y-m-d');
	// 	$this->db->select('tgl_trans');
	// 	$this->db->where('tgl_trans', $curday);
	// 	$this->db->where('id_jtrans', '02');	
	// 	$this->db->from('tbl_tabungan');
	// 	return $this->db->count_all_results();
	// }
}
