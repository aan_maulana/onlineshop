<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model {
 public function __construct()
 {
  parent::__construct();
 }
	public function cekmenu($namamenu){
		$smenu = $this->db->get_where('tbl_submenu',array('slink'=>$namamenu,'sstatus'=>'1'))->result();
		foreach ($smenu as $key ) {
 			$menuid = trim($key->smenu_id);
 		}
 		$hak = explode("|",$this->session->userdata('priv'));
 		$out = "";
 		for($i=0;$i<count($hak);$i++){
	 		if($hak[$i]===$menuid){
	 				$out .= "true|";
	 		}else{
	 			$out .= "false|";
	 		}
 		} 
 		if(strpos($out,'true') !== false) {
    		return TRUE;
		}else{
			return FALSE;
		}
 	}
 	public function cekmenux($namamenu){
		$smenu = $this->db->get_where('tbl_submenux',array('slinkx'=>$namamenu,'sstatusx'=>'1'))->result();
		foreach ($smenu as $key ) {
 			$menuid = trim($key->smenu_id);
 		}
 		$hak = explode("|",$this->session->userdata('privx'));
 		$out = "";
 		for($i=0;$i<count($hak);$i++){
	 		if($hak[$i]===$menuid){
	 				$out .= "true|";
	 		}else{
	 			$out .= "false|";
	 		}
 		} 
 		if(strpos($out,'true') !== false) {
    		return TRUE;
		}else{
			return FALSE;
		}
 	}
 	function update_mesin($id,$datax){
		$this->db->where('id !=',$id);
		$this->db->update('tbl_mesin',$datax);
	}
	function update_thnajaran($id,$datax){
		$this->db->where('id !=',$id);
		$this->db->update('tbl_tahun_ajaran',$datax);
	}
	function update_template($id,$datax){
		$this->db->where('id !=',$id);
		$this->db->update('tbl_style',$datax);
	}
	function update_templatex($id,$datax){
		$this->db->where('id_style !=',$id);
		$this->db->update('tbl_style_js',$datax);
	}
	function update_templatexx($id,$data){
		$this->db->where('id_style !=',$id);
		$this->db->update('tbl_style_js',$data);
	}
	function update_semester($id,$datax){
		$this->db->where('id !=',$id);
		$this->db->update('tbl_semester',$datax);
	}
}