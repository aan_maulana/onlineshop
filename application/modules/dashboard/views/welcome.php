<script type="text/javascript">
    jQuery(document).ready(function(){
        var calendar = jQuery('#calendar').fullCalendar({
            editable: true,
            header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: "<?php echo base_url();?>dashboard/kalender/",
            eventRender: function(event, element, view) {
                if(event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            }  
        });
    });
</script>
<div class="row">
    <!-- <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-green">
            <div class="stats-icon"><i class="fa fa-suitcase"></i></div>
			<div class="stats-info">
				<h4>Total Transaksi Simpanan Hari ini</h4>
				<p><?php echo $dash_ttlkredit; ?></p>	
			</div>
            <div class="stats-desc">Total Pengambilan : xxx </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-purple">
            <div class="stats-icon"><i class="fa fa-envelope-o"></i></div>
            <div class="stats-info">
                <h4>Total Transaksi Pengambilan / Penarikan Tabungan Hari Ini</h4>
                <p><? echo $dash_ttldebet; ?></p> 
            </div>
            <div class="stats-desc">Total : yyy </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-blue">
            <div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				<h4>Data Pegawai</h4>
				<p><?php echo $this->db->get("view_pegawai")->num_rows();?> Orang</p>	
			</div>
            <div class="stats-desc">Total Data Pegawai : <?php echo $this->db->get("view_pegawai")->num_rows();?> Orang</div>
        </div>
    </div> -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
			<div class="stats-info">
				<h4>Waktu</h4>
				<p><span id="waktos"><script type="text/javascript">window.onload = waktos('waktos');</script></span></p>	
			</div>
            <div class="stats-desc"><span id="kaping"><script type="text/javascript">window.onload = kaping('kaping');</script></span></div>
        </div>
    </div>
    <?php
    date_default_timezone_set('Asia/Jakarta');
    $bln = date("m");
    $cekbln = $this->db->query("SELECT bulan FROM tbl_bulan WHERE kode = '$bln'")->result();
    if(count($cekbln)>0){
        foreach ($cekbln as $ju) {
            $blnx = $ju->bulan;
        }
    }else{
        $blnx = "";
    }
    ?>
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Papan Informasi</h4>
            </div>
            <div class="panel-body">
                <p style="text-align:center">
                    Informasi Untuk Bulan <strong><?php echo $blnx . " " . date("Y");?></strong>
                </p>
                <div>
                    <div data-scrollbar="true" data-height="247px">
                        <?php
                        $i = 0;
                        date_default_timezone_set('Asia/Jakarta');
                        $akhir = date("Y-m-d");
                        $tgl = date("m");
                        $news = $this->db->query("SELECT * FROM tbl_news WHERE SUBSTRING(tgl_dibuat,6,2) = '$tgl' AND publish = '1' ORDER BY tgl_dibuat DESC")->result();
                        if(count($news)>0){
                            foreach ($news as $huy) {
                                $i++;
                                $berita = $huy->berita;
                                $tgl_dibuat = date("d-m-Y",strtotime($huy->tgl_dibuat));
                                $dibuat = $huy->dibuat_oleh;
                                $ckdata = $this->db->get_where('view_pegawai',array('kode'=>$dibuat))->result();
                                if(count($ckdata)>0){
                                    foreach ($ckdata as $tah) {
                                        $nama = $tah->nama;
                                    }
                                }else{
                                    $nama = "Administrator";
                                }

                        ?>
                        <li class="media media-lg">
                            <div class="media-body">
                                <h5 class="media-heading" title="Berita Tanggal <?php echo $tgl_dibuat;?>"><?php echo $i . " .";?>&nbsp;<?php echo $berita;?></h5>
                                <small>Dibuat Oleh : <?php echo $nama;?></small>
                                <small>Tanggal Di Buat : <?php echo $tgl_dibuat;?></small>
                            </div>
                        </li>
                        <hr></hr>
                        <?php
                            }
                        }
                        ?>      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-6">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Selamat Ulang Tahun</h4>
            </div>
            <div class="panel-body">
                <p style="text-align:center">
                    Yang Berulang Tahun Bulan <strong><?php echo $blnx . " " . date("Y");?></strong>
                </p>
                <div data-scrollbar="true" data-height="247px">
                    <?php
                    date_default_timezone_set('Asia/Jakarta');
                    $akhir = date("Y-m-d");
                    $tgl = date("m");
                    $ultah = $this->db->query("SELECT * FROM view_pegawai WHERE SUBSTRING(tgl_lahir,6,2) = '$tgl' ORDER BY tgl_lahir ASC")->result();
                    if(count($ultah)>0){
                        foreach ($ultah as $huy) {
                            $nip = $huy->kode;
                            $unit = $huy->nama_jabatan;
                            $satker = $huy->nama_golongan;
                            $tanggal_lahir = $huy->tgl_lahir;
                            $nama = $huy->nama;
                            $foto = $huy->foto;
                            $diff = abs(strtotime($akhir) - strtotime($tanggal_lahir));
                            $years = floor($diff / (365*60*60*24));
                            if(empty($foto)){
                                $fotox = "no.jpg";
                            }else{
                                $fotox = $foto;
                            }
                            $pecah = explode("-", $tanggal_lahir);
                            $thn = $pecah['0'];
                            $tglx = $pecah['2'];
                            $bln = $pecah['1'];
                        
                    ?>
                    <li class="media media-lg">
                        <a class="fancybox pull-left" href="<?php echo base_url();?>foto/pegawai/<?php echo $fotox;?>" style="height:92px;width:78px" data-fancybox-group="gallery" title="<?php echo $nama;?>"><img src="<?php echo base_url();?>foto/pegawai/<?php echo $fotox;?>" style="width:71px;" alt="" /></a>
                        <div class="media-body">
                            <h5 class="media-heading" title="Nama Pegawai"><?php echo $nama;?></h5>
                            <!-- <div title="NIP Pegawai"><?php echo $nip;?></div> -->
                            <div title="Tanggal Lahir"><?php echo $tglx . " " . $blnx . " " . $thn;?></div>
                            <div title="Jabatan"><?php echo $unit;?></div>
                            <div title="Golongan"><?php echo $satker;?></div>
                            <div title="Usia"><?php echo $years;?> Tahun</div>
                        </div>
                    </li>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>    
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Data Kalender</h4>
    </div>

    <div class="panel-body p-0">
        <div class="vertical-box">
            <div id="calendar" class="vertical-box-column p-20 calendar"></div>
        </div>
    </div>
</div>
