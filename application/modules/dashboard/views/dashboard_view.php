<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>SISTEM MANAJEMEN ASET</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="Seven Creative" />
    <link rel="shortcut icon" href="<?php echo base_url();?>assets-admin/img/favicon.ico">
    <link href="<?php echo base_url();?>assets-admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/funcy/jquery.fancybox.css" media="screen" />
    <!-- <link href="<?php echo base_url();?>assets-admin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" /> -->
    <link href="<?php echo base_url();?>assets-admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/style_nyunyu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/style-responsive_nyunyu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/theme/default_nyunyu.css">
    <link href="<?php echo base_url();?>assets-admin/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/DataTables/css/data-table.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/ionRangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/morris/morris.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>assets-admin/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets-admin/css/prettify/prettify.js"></script>
    
    <script src="<?php echo base_url();?>assets-admin/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets-admin/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/masked-input/masked-input.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/password-indicator/js/password-indicator.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery-tag-it/js/tag-it.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-daterangepicker/moment.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets-admin/funcy/jquery.fancybox.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/js/form-plugins.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/js/apps.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/js/app.js"></script>
    <script>
        $(document).ready(function() {
            jQuery('.fancybox').fancybox({'type' : 'image'});
            App.init();
            FormPlugins.init();
        });
    </script>
</head>
<body>
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <?php
            $this->load->view('dashboard/menu_atas');
        ?>
        <?php
            $this->load->view('dashboard/menu_samping');
        ?>
        <div class="sidebar-bg"></div>
        <div id="content" class="content">
            <h1 class="page-header"><?php echo $halaman;?> <small><?php echo $judul;?></small></h1>
            <?php
            $this->load->view($content);
            ?>
        </div>
        <?php 
        $this->load->view('dashboard/footer');
        ?>
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    </div>
</body>
</html>
<script type="text/javascript" src="<?php echo base_url();?>assets-admin/js/jquery.blockUI.js"></script>