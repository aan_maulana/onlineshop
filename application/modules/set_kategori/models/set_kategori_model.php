<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Set_kategori_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    function data_set_kategori($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                select sk.id_kategori AS id,sk.nama_kategori AS nama,sk.kode_level AS kode_level,sk.kode_parent AS kode_parent,sk.status AS status from tbl_kategori sk order by sk.nama_kategori
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }
    function data_set_kategori_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                select sk.id_kategori AS id,sk.nama_kategori AS nama,sk.kode_level AS kode_level,sk.kode_parent AS kode_parent,sk.status AS status from tbl_kategori sk order by sk.nama_kategori
            ) A
        ");
        return $query;
    }
}