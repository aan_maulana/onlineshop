<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/duit.js"></script>
<div class="row">
    <div class="col-md-12">
    	

		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    
			    <div class="panel-body panel-form">
			        <form class="form-horizontal form-bordered" action="<?php echo base_url();?>set_kategori/<?php echo $action;?>" method="post" data-parsley-validate="true" enctype="multipart/form-data" >
	                    <?php
	                    if($cek==='edit'){
	                    	?> 		
	                    		<div class="form-group">
									<label class="control-label col-md-3 col-sm-3">Kode kategori * :</label>
									<div class="col-md-3 col-sm-3">
										<input class="form-control" type="text" readonly="readonly" id="kode_kategori" minlength="1" name="kode_kategori" value="<?php echo set_value('kode_kategori',isset($default['kode_kategori']) ? $default['kode_kategori'] : ''); ?>" data-type="kode_kategori" placeholder="Masukan Kode kategori" data-parsley-required="true" data-parsley-minlength="1"/>
									</div>
								</div>
	                    	<?php
	                    	}
	                    ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Nama kategori * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="nama" minlength="2" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" placeholder="Masukan nama kategori" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Parent kategori * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="parent" minlength="1" name="parent" value="<?php echo set_value('parent',isset($default['parent']) ? $default['parent'] : ''); ?>" data-type="parent" placeholder="Masukan parent kategori" data-parsley-required="true" data-parsley-minlength="1"/>
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Level kategori * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="level" minlength="1" name="level" value="<?php echo set_value('level',isset($default['level']) ? $default['level'] : ''); ?>" data-type="level" placeholder="Masukan level kategori" data-parsley-required="true" data-parsley-minlength="1"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3"></label>
							<div class="col-md-3 col-sm-3">
								<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
	                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
							</div>
						</div>
			        </form>
			    </div>
	</div>
</div>

<script type="text/javascript">	
	$("#level").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#parent").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });    
</script>