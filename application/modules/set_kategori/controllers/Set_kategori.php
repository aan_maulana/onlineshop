<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Set_kategori extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('set_kategori/Set_kategori_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'referensi';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Set Kategori';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "Set_kategori";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "referensi";
					$isi['namamenu'] = "Set Kategori";
					$isi['page'] = "Set_kategori";
					$isi['link'] = 'Set_kategori';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Set Kategori";
					$isi['judul'] = "Halaman Set Kategori";
					$isi['content'] = "kategori_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function get_data(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$aColumns = array('id','id','nama','kode_level','kode_parent','status','id');
	        $sIndexColumn = "id";
	        // pagings
	        $sLimit = "";
	        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
	            $sLimit = "LIMIT ".$this->kacang->anti( $_GET['iDisplayStart'] ).", ".
	                $this->kacang->anti( $_GET['iDisplayLength'] );
	        }
	        $numbering = $this->kacang->anti( $_GET['iDisplayStart'] );
	        $page = 1;
	        // ordering
	        if ( isset( $_GET['iSortCol_0'] ) ){
	            $sOrder = "ORDER BY  ";
	            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
	                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
	                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
	                        ".$this->kacang->anti( $_GET['sSortDir_'.$i] ) .", ";
	                }
	            }            
	            $sOrder = substr_replace( $sOrder, "", -2 );
	            if ( $sOrder == "ORDER BY" ){
	                $sOrder = "";
	            }
	        }
	        // filtering
	        $sWhere = "";
	        if ( $_GET['sSearch'] != "" ){
	            $sWhere = "WHERE (";
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti( $_GET['sSearch'] )."%' OR ";
	            }
	            $sWhere = substr_replace( $sWhere, "", -3 );
	            $sWhere .= ')';
	        }
	        // individual column filtering
	        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
	                if ( $sWhere == "" ){
	                    $sWhere = "WHERE ";
	                }
	                else{
	                    $sWhere .= " AND ";
	                }
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti($_GET['sSearch_'.$i])."%' ";
	            }
	        }
	        $rResult = $this->Set_kategori_model->data_set_kategori($aColumns, $sWhere, $sOrder, $sLimit);
	        $iFilteredTotal = 10;
	        $rResultTotal = $this->Set_kategori_model->data_set_kategori_total($sIndexColumn);
	        $iTotal = $rResultTotal->num_rows();
	        $iFilteredTotal = $iTotal;
	        $output = array(
	            "sEcho" => intval($_GET['sEcho']),
	            "iTotalRecords" => $iTotal,
	            "iTotalDisplayRecords" => $iFilteredTotal,
	            "aaData" => array()
	        );
	        foreach ($rResult->result_array() as $aRow){
	            $row = array();
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                /* General output */
	                if($i < 1)
	                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
	                else
	                    $row[] = $aRow[ $aColumns[$i] ];
	            }
	            $page++;
	            $output['aaData'][] = $row;
	        }
	        echo json_encode( $output );
	    }    
	}
	
	public function add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'Set_kategori';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['cek']	 = "add";
					$isi['kelas'] = "Set_kategori";
					$isi['namamenu'] = "Tambah Set Kategori";
					$isi['page'] = "Set_kategori";
					$isi['link'] = 'Set_kategori';
					$isi['actionhapus'] = 'hapus';
					$isi['action'] = "proses_add";
					$isi['tombolsimpan'] = 'Simpan';
					$isi['tombolbatal'] = 'Batal';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Tambah Set Kategori";
					$isi['judul'] = "Halaman Set Kategori";
					$isi['jk'] = "";
					$isi['content'] = "Set_kategori/form_set_kategori";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'Set_kategori';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){
		 			$this->form_validation->set_rules('nama','nama kategori','required|min_length[2]');
		 			$this->form_validation->set_rules('level','level kategori','required|min_length[1]');
		 			$this->form_validation->set_rules('parent','parent kategori','required|min_length[1]');
					if ($this->form_validation->run() == TRUE){
						$nama = $this->kacang->anti($this->input->post('nama'));
						$level = $this->kacang->anti($this->input->post('level'));
						$parent = $this->kacang->anti($this->input->post('parent'));				
						$simpan = array('nama_kategori'=>$nama,'kode_level'=>$level,'kode_parent'=>$parent,
										  'status'=>1
										  ); 				
							$this->db->insert('tbl_kategori',$simpan);
							redirect('Set_kategori','refresh');
						}	
	                }else{
                        $this->add();
	                }
				}else{
					redirect('error','refresh');
				}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'Set_kategori';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					if($jns=="aktif"){
						$data = array('status'=>'0');
					}else{
						$data = array('status'=>'1');
					}
					$this->db->where('id_kategori',$this->kacang->anti($id));	
					$this->db->update('tbl_kategori',$data);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function hapus($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'Set_kategori';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_produk',array('id_kategori'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "NotOk";
						}else{
							$this->db->where('id_kategori',$this->kacang->anti($kode));
							if($this->db->delete('tbl_kategori')){
								$data['say'] = "ok";
							}else{
								$data['say'] = "NotOk";
							}
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

	function hapusfoto($kode=null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$kode = $this->kacang->anti($kode);
			$ckdata = $this->db->get_where('tbl_slideshow',array('kode_banner'=>$kode))->result();
			foreach ($ckdata as $hehe) {
				$fotona = $hehe->gambar;
			}
			if($fotona!=""){
				if($fotona!="no.jpg"){
					unlink('assets/image/slider/'.$fotona);
				}
			}
		}	
	}

	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'Set_kategori';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_kategori',array('id_kategori'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "ok";
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}
	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'Set_kategori';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_kategori',array('id_kategori'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
							$isi['default']['kode_kategori'] 	= $key->id_kategori;
							$isi['default']['nama'] 			= $key->nama_kategori;
							$isi['default']['level'] 			= $key->kode_level;
							$isi['default']['parent'] 			= $key->kode_parent;
						}
						$this->session->set_userdata('kode_set_kategori',$kode);
						$isi['cek']	 = "edit";
						$isi['kelas'] = "Set_kategori";
						$isi['namamenu'] = "Edit Set Kategori";
						$isi['page'] = "Set_kategori";
						$isi['link'] = 'Set_kategori';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Set Kategori";
						$isi['judul'] = "Halaman Set Kategori";
						$isi['content'] = "Set_kategori/form_set_kategori";
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}

	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'Set_kategori';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('nama','nama kategori','required|min_length[2]');
		 			$this->form_validation->set_rules('level','level kategori','required|min_length[1]');
		 			$this->form_validation->set_rules('parent','parent kategori','required|min_length[1]');
					if ($this->form_validation->run() == TRUE){	
						$nama = $this->kacang->anti($this->input->post('nama'));
						$level = $this->kacang->anti($this->input->post('level'));
						$parent = $this->kacang->anti($this->input->post('parent'));
						$cekcode = $this->db->get_where('tbl_kategori',array('id_kategori'=>$this->session->userdata('kode_set_kategori')))->result();
						if(count($cekcode)>0){
							$edit = array('nama_kategori'=>$nama,'kode_level'=>$level,'kode_parent'=>$parent,
										  'status'=>1
										  );
								
							$this->db->where('id_kategori',$this->kacang->anti($this->session->userdata('kode_set_kategori')));
							$this->db->update('tbl_kategori',$edit);
							$this->session->unset_userdata('kode_set_kategori');
							redirect('Set_kategori','refresh');
						}	
	                }else{
						redirect(base_url().'Set_kategori/edit/'.$this->session->userdata('kode_set_kategori'),'refresh');
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
