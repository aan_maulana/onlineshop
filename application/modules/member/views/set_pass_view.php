<script type="text/JavaScript">
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- format penulisan '+nm+' salah...!\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' harus angka.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' harus angka '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' masih kosong...!\n'; }
  } if (errors) { alert('Oooopppzzz,,,,ada sedikit kesalahan pada :\n'+errors);
  document.MM_returnValue = (errors == ''); }
  else { document.MM_returnValue = (errors == ''); }
}
</script>
<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url()?>member">Member</a></li>
        <li><a href="<?php echo base_url()?>member/set_pass">Set Password</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!-- left side -->
        <!--Left Part Start -->
        <aside id="column-left" class="col-sm-3 hidden-xs">
          <!-- <h3 class="subtitle">Categories</h3>
          <div class="box-category">
            <ul id="cat_accordion">
              <li><a href="category.html">Fashion</a> <span class="down"></span>
                <ul>
                  <li><a href="category.html">Men</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Women</a></li>
                  <li><a href="category.html">Girls</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Boys</a></li>
                  <li><a href="category.html">Baby</a></li>
                  <li><a href="category.html">Accessories</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a class="active" href="category.html">Electronics</a> <span class="down"></span>
                <ul style="display:block;">
                  <li><a href="category.html">Laptops</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Desktops</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Cameras</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Mobile Phones</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">New Sub Categories</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">TV &amp; Home Audio</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories New</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">MP3 Players</a></li>
                </ul>
              </li>
              <li><a href="category.html">Shoes</a> <span class="down"></span>
                <ul>
                  <li><a href="category.html">Men</a></li>
                  <li><a href="category.html">Women</a> <span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories</a></li>
                    </ul>
                  </li>
                  <li><a href="category.html">Girls</a></li>
                  <li><a href="category.html">Boys</a></li>
                  <li><a href="category.html">Baby</a></li>
                  <li><a href="category.html">Accessories</a><span class="down"></span>
                    <ul>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">New Sub Categories</a></li>
                      <li><a href="category.html">Sub Categories</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="category.html">Watches</a> <span class="down"></span>
                <ul>
                  <li><a href="category.html">Men's Watches</a></li>
                  <li><a href="category.html">Women's Watches</a></li>
                  <li><a href="category.html">Kids' Watches</a></li>
                  <li><a href="category.html">Accessories</a></li>
                </ul>
              </li>
              <li><a href="category.html">Health &amp; Beauty</a> <span class="down"></span>
                <ul>
                  <li><a href="category.html">Perfumes</a></li>
                  <li><a href="category.html">Makeup</a></li>
                  <li><a href="category.html">Sun Care</a></li>
                  <li><a href="category.html">Skin Care</a></li>
                  <li><a href="category.html">Eye Care</a></li>
                  <li><a href="category.html">Hair Care</a></li>
                </ul>
              </li>
            </ul>
          </div> -->
          <!-- <h3 class="subtitle">Bestsellers</h3>
          <div class="side-item">
            <div class="product-thumb clearfix">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/apple_cinema_30-50x75.jpg" alt="Brand Fashion Cotton T-Shirt" title="Brand Fashion Cotton T-Shirt" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Brand Fashion Cotton T-Shirt</a></h4>
                <p class="price"><span class="price-new">$110.00</span> <span class="price-old">$122.00</span> <span class="saving">-10%</span></p>
              </div>
            </div>    
          </div> -->
          <h3 class="subtitle">Specials</h3>
          <div class="side-item">
            <?php 
              if(count($slide_rekomendasi)>0){  
                foreach ($slide_rekomendasi as $reko) {
                  $cn = array (' ');
                  $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                  $nm_produkn = strtolower(str_replace($dn,"",$reko->nama_produk));
                  $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                  $hargan = $reko->harga;
                  $stok   = $reko->stok;
                  ?>  
                    <div class="product-thumb clearfix">
                      <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($reko->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $reko->gbr_kecil; ?>" alt="<?php echo $reko->nama_produk; ?>" title="<?php echo $reko->nama_produk; ?>" class="img-responsive" /></a></div>
                      <div class="caption">
                        <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($reko->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $reko->nama_produk; ?></a></h4>
                        <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                      </div>
                    </div>
                  <?php
              }   
            }
            ?>
          </div>
        </aside>
        <!--Left Part End -->
        <!--  end left side -->

        <!--Middle Part Start-->
        <div id="content" class="col-sm-6">
          <div class="row">
            <form method="post" action="<?php echo base_url(); ?>member/update_pass">
              <h2 class="subtitle">Set Password Account Member</h2>
              <div class="form-group">
                  <label class="control-label" for="input-username">Username</label>
                  <input size="100" type="readonly" name="username" value="<?php echo set_value('',isset($default['username']) ? $default['username'] : ''); ?>" placeholder="username" id="input-username" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-passlama">Password Lama</label>
                  <input size="50" type="password" name="passlama" value="<?php echo set_value('passlama',isset($default['passlama']) ? $default['passlama'] : ''); ?>" placeholder="password lama" id="input-passlama" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-telp">Password Baru</label>
                  <input size="50" type="password" name="passbaru" value="<?php echo set_value('passbaru',isset($default['passbaru']) ? $default['passbaru'] : ''); ?>" placeholder="password baru" id="input-telp" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-telp">Ulangi Password </label>
                  <input size="50" type="password" name="ulangi" value="<?php echo set_value('ulangi',isset($default['ulangi']) ? $default['ulangi'] : ''); ?>" placeholder="ulangi password baru" id="input-telp" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <?php echo $gbr_captcha; ?>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-captcha">Kode Captcha</label>
                  <input type="text" name="captcha" placeholder="captcha" id="captcha" class="form-control">
                  <br>
                <input type="submit" onclick="MM_validateForm('username','','R','passlama','','R','passbaru','','R','ulangi','','R','captcha','','R');return document.MM_returnValue" value="Update Data" class="btn btn-primary">
                <input type="reset"  value="Reset Form" class="btn btn-primary">
            </div>
            </form>  
          </div>
        </div>
        <!--Middle Part End -->
        
        <!--Right Part Start -->
          <aside id="column-right" class="col-sm-3 hidden-xs">
          <h3 class="subtitle">Bestsellers</h3>
              <div class="side-item">
                       <?php 
                          if(count($slide_laris)>0){  
                            foreach ($slide_laris as $laris) {
                              $cn = array (' ');
                              $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                              $nm_produkn = strtolower(str_replace($dn,"",$laris->nama_produk));
                              $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                              $hargan = $laris->harga;
                              $stok   = $laris->stok;
                              ?>  
                                <div class="product-thumb clearfix">
                                  <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $laris->gbr_kecil; ?>" alt="<?php echo $laris->nama_produk; ?>" title="<?php echo $laris->nama_produk; ?>" class="img-responsive" /></a></div>
                                  <div class="caption">
                                    <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $laris->nama_produk; ?></a></h4>
                                    <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                                  </div>
                                  <div class="button-group">
                                    <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                                    <div class="add-to-links">
                                      <!-- <button type="button" data-toggle="tooltip" title="Add to Wish List" onClick=""><i class="fa fa-heart"></i></button>
                                      <button type="button" data-toggle="tooltip" title="Compare this Product" onClick=""><i class="fa fa-exchange"></i></button> -->
                                    </div>
                                  </div>
                                </div>
                              <?php
                          }   
                        }
                        ?> 
              </div>
          
              <h3 class="subtitle">produk terbaru</h3>
              <div class="side-item">
                  <?php 
                    if(count($slide_baru)>0){  
                      foreach ($slide_baru as $key) {
                        $c = array (' ');
                        $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                        $nm_produk = strtolower(str_replace($d,"",$key->nama_produk));
                        $link = strtolower(str_replace($c, '-', $nm_produk)); 
                        $harga = $key->harga;
                        ?>  
                          <div class="product-thumb clearfix">
                              <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $key->gbr_kecil; ?>" alt="<?php echo $key->nama_produk; ?>" title="<?php echo $key->nama_produk; ?>" class="img-responsive" /></a></div>
                              <div class="caption">
                                <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><?php echo $key->nama_produk; ?></a></h4>
                                <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($harga,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                              </div>
                          </div>        
                        <?php
                      }  
                    }
                  ?>
              </div>
            </aside>
        <!--Right Part End -->
      </div>
    </div>
  </div>