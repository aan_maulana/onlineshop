<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="login.html">Member</a></li>
        <li><a href="login.html">Login</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-9">
          <h1 class="title">Member Login</h1>
          <div class="row">
            <div class="col-sm-6">
              <h2 class="subtitle">Customer Baru</h2>
              <p><strong>Register Member</strong></p>
              <p>Dengan membuat account member pembeli dapat lebih cepat untuk proses transaksi pembelian, lebih up to date tentang order status, dapat melakukan cek history belanja.</p>
              <a href="register.html" class="btn btn-primary">Register Member</a> </div>
            <div class="col-sm-6">
            <form method="post" action="<?php echo base_url(); ?>member/kirimregister">
              <h2 class="subtitle">Daftar Member - LKLTSK Online Store</h2>
                <div class="form-group">
                  <label class="control-label" for="input-username">Username</label>
                  <input type="text" name="username" value="" placeholder="username" id="input-username" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Password</label>
                  <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-nama">nama</label>
                  <input type="text" name="nama" value="" placeholder="nama" id="input-nama" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-email">email</label>
                  <input type="email" name="email" value="" placeholder="email" id="input-email" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-alamat">alamat</label>
                  <textarea type="alamat" name="alamat" value="" placeholder="alamat" id="input-alamat" class="form-control"></textarea>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-telpon">telpon</label>
                  <input type="text" name="telpon" value="" placeholder="telpon" id="input-telpon" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-propinsi">propinsi</label>
                  <input type="text" name="propinsi" value="" placeholder="propinsi" id="input-propinsi" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-kodepos">kodepos</label>
                  <input type="text" name="kodepos" value="" placeholder="kodepos" id="input-kodepos" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-kota">kota</label>
                  <input type="text" name="kota" value="" placeholder="kota" id="input-kota" class="form-control">
                </div>
                <div class="form-group">
                  <?php echo $gbr_captcha; ?>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Kode Captcha</label>
                  <input type="text" name="captcha" value="" placeholder="captcha" id="captcha" class="form-control">
                  <br />
                  <input type="submit" value="Login" class="btn btn-primary">
                </div>
            </form>  
            </div>
          </div>
        </div>
        <!--Middle Part End -->
        <!--Right Part Start -->
        
        <!--Right Part End -->
      </div>
    </div>
  </div>