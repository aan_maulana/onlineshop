<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url();?>member">Member</a></li>
        <li><a href="<?php echo base_url();?>member/history">History</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h4 class="title">History Detail Transaksi - Anda Di Online Store</h4>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td class="text-center">Tanggal Transaksi</td>
                    <td class="text-center">Kode Transaksi</td>
                    <td class="text-center">Nama Produk</td>
                    <td class="text-center">Rating</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($history)>0){
                    $i = 0; 
                    foreach($history as $key)
                    {
                      $i++; 
                      $id = $key->kode_transaksi_detail;
                      $kode_transaksi = $key->kode_transaksi;
                      $nama_produk  = $key->nama_produk;
                      $kode_produk  = $key->kode_produk;
                      $harga = $key->harga;
                      $tgl = substr($key->kode_transaksi,6,2).'-'.substr($key->kode_transaksi,4,2)."-".substr($key->kode_transaksi,0,4);
                      $harga = $key->harga;
                      $jumlah = $key->jumlah;
                      $tot = $key->harga * $key->jumlah;
                      $session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
                      $pecah=explode("|",$session);
                      $user_id = $pecah[2];
                      $cekrating = $this->db->query("SELECT * FROM tbl_user_rating WHERE user_id = '$user_id' AND produk_id = '$kode_produk'");
                      if(count($cekrating->result())>0){
                        $rowx = $cekrating->row();
                        $ratingna = $rowx->rating;
                      }else{
                        $ratingna = "0";
                      }
                      ?>
                      <tr>
                        <td class="text-center" rowspan="2" ><?php echo $tgl; ?></td></td>
                        <td class="text-center" rowspan="2"><?php echo $kode_transaksi; ?></td>
                        <input type="hidden" id="id_transaksi" value="<?php echo $id;?>">
                        <td class="text-left"><?php echo $nama_produk; ?></td>
                        <td class="text-center" rowspan="2">
                            <span dir="ltr" class="inline">
                                  <input id="input-<?= $key->kode_produk; ?>" name="rating"
                                        value="<?php echo $ratingna;?>"
                                        data-disabled="false"
                                        class="rating"
                                        min="0" max="5" step="0.5" data-size="xs"
                                        accept="" data-symbol="&#xf005;" data-glyphicon="false"
                                        data-rating-class="rating-fa">
                              </span>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-left">Rp. <?php echo number_format($harga); ?> x <?php echo $jumlah; ?> = Rp. <?php echo number_format($tot,2,',','.'); ?></td>
                      </tr>
                      <?php
                    }
                }
                ?>                  
                </tbody>
              </table>
            </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>
 