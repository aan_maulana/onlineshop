<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url();?>member">Member</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h3 class="text-center">Selamat Datang Di Halaman Member Area Online Store .</h3>
          <div class="row">
            <div class="col-sm-4">
              <div class="well">
                <h4><u>Pengaturan Profil</u></h4>
                <p>Menu ini digunakan untuk melakukan perubahan update data pelanggan online store.</p>
                <a href="<?php echo base_url();?>member/set_profil" class="btn btn-primary">Mengubah   Data Profile</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                <h4><u>History Transaksi</u></h4>
                <p>Menu ini berupa catatan history pembelian pengguna di online store.</p>
                <a href="<?php echo base_url();?>member/history" class="btn btn-primary">Lihat History Transaksi</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                <h4><u>Konfirmasi Pembayaran</u></h4>
                <p>Silahkan Lakukan Konfirmasi Pembayaran Melalui Menu Ini .</p>
                <a href="<?php echo base_url();?>konfirmasi" class="btn btn-primary">Konfirmasi Pembayaran</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                <h4><u>Kirim Testimonial</u></h4>
                <p>Menu Ini Digunakan Untuk Kirim Testimonial Anda Selama Berbelanja Di Tempat Kami.</p>
                <a href="<?php echo base_url();?>konfirmasi" class="btn btn-primary">Kirim Testimoni</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                <h4><u>Kontak Kami</u></h4>
                <p>Silahkan Hubungi Kami Untuk Informasi Lebih Lanjut.</p>
                <a href="<?php echo base_url();?>contact_us" class="btn btn-primary">Hubungi Kami</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                <h4><u>Pengaturan Password</u></h4>
                <p>Menu halaman ini digunakan untuk ubah password login anda.</p>
                <a href="<?php echo base_url();?>member/set_pass" class="btn btn-primary">ubah Password Log In</a> </div>
            </div>
          </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>