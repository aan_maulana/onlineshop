<script type="text/JavaScript">
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- format penulisan '+nm+' salah...!\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' harus angka.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' harus angka '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' masih kosong...!\n'; }
  } if (errors) { alert('Oooopppzzz,,,,ada sedikit kesalahan pada :\n'+errors);
  document.MM_returnValue = (errors == ''); }
  else { document.MM_returnValue = (errors == ''); }
}
</script>
<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url()?>member">Member</a></li>
        <li><a href="<?php echo base_url()?>member/set_profil">Set Profil</a></li>
      </ul>
      <!-- Breadcrumb End-->
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <div class="row">
            <form method="post" action="<?php echo base_url(); ?>member/update_profil">
            <div class="col-sm-5">
              <h2 class="subtitle">Set Member Profile</h2>
              <div class="form-group">
                  <label class="control-label" for="input-nama">Nama Lengkap</label>
                  <input type="text" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" placeholder="nama lengkap" id="input-nama" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-email">Email</label>
                  <input type="text" name="email" value="<?php echo set_value('email',isset($default['email']) ? $default['email'] : ''); ?>" placeholder="nama email" id="input-email" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-telp">No Telp</label>
                  <input type="text" name="telpon" value="<?php echo set_value('telpon',isset($default['telpon']) ? $default['telpon'] : ''); ?>" placeholder="no telpon" id="input-telp" class="form-control input-sm">
              </div>
              <div class="form-group">
                  <label class="control-label" for="input-alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" name="alamat" placeholder="alamat"><?php echo set_value('alamat',isset($default['alamat']) ? $default['alamat'] : ''); ?></textarea>
              </div>
           </div>
            <div class="col-sm-4">
              <h2 class="subtitle">&nbsp;</h2>
                <div class="form-group">
                  <label class="control-label" for="input-propinsi">propinsi</label>
                  <input type="text" name="propinsi" value="<?php echo set_value('propinsi',isset($default['propinsi']) ? $default['propinsi'] : ''); ?>" placeholder="nama propinsi" id="input-propinsi" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-kota">kota</label>
                  <input type="text" name="kota" value="<?php echo set_value('kota',isset($default['kota']) ? $default['kota'] : ''); ?>" placeholder="nama kota" id="input-kota" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-kodepos">kode pos</label>
                  <input type="text" name="kodepos" value="<?php echo set_value('kodepos',isset($default['kodepos']) ? $default['kodepos'] : ''); ?>" placeholder="nama kodepos" id="input-kodepos" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-option">Tanggal Lahir</label>
                  <?php
                      $lahir = explode("-",$birthdate);
                          echo "<select class='input-sm' name='tgl'>";
                            for($a=1;$a<=31;$a++)
                            {
                              if($lahir[0]==$a)
                            {
                                echo "<option value=$a selected>$a</option>";
                            }
                            else
                            {
                                echo "<option value=$a>$a</option>";
                            }
                            }
                            echo "</select> - ";

                            echo "<select class='input-sm' name='bln'>";
                            for($b=1;$b<=12;$b++)
                            {
                              if($lahir[1]==$b)
                            {
                                echo "<option value=$b selected>$b</option>";
                            }
                            else
                            {
                                echo "<option value=$b>$b</option>";
                            }
                            }
                            echo "</select> - ";
                            echo "<select class='input-sm' name='thn'>";
                            for($c=1950;$c<=date('Y');$c++)
                            {
                              if($lahir[2]==$c)
                            {
                                echo "<option value=$c selected>$c</option>";
                            }
                            else
                            {
                                echo "<option value=$c>$c</option>";
                            }
                            }
                            echo "</select>";
                      ?>

                </div>
                <div class="form-group">
                  <?php echo $gbr_captcha; ?>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-captcha">Kode Captcha</label>
                  <input type="text" name="captcha" placeholder="captcha" id="captcha" class="form-control">
                  <br>
                <input type="submit" onclick="MM_validateForm('nama','','R','email','','RisEmail','alamat','','R','telpon','','RisNum','propinsi','','R','kota','','R','captcha','','R');return document.MM_returnValue" value="Update Data" class="btn btn-primary">
                <input type="reset"  value="Reset Form" class="btn btn-primary">
            </div>
            </form>  
          </div>
        <!--Middle Part End -->
        <!--Right Part Start -->
          <aside id="column-right" class="col-sm-3 hidden-xs">
          <h3 class="subtitle">Bestsellers</h3>
              <div class="side-item">
                       <?php 
                          if(count($slide_laris)>0){  
                            foreach ($slide_laris as $laris) {
                              $cn = array (' ');
                              $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                              $nm_produkn = strtolower(str_replace($dn,"",$laris->nama_produk));
                              $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                              $hargan = $laris->harga;
                              $stok   = $laris->stok;
                              ?>  
                                <div class="product-thumb clearfix">
                                  <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $laris->gbr_kecil; ?>" alt="<?php echo $laris->nama_produk; ?>" title="<?php echo $laris->nama_produk; ?>" class="img-responsive" /></a></div>
                                  <div class="caption">
                                    <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $laris->nama_produk; ?></a></h4>
                                    <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                                  </div>
                                  <div class="button-group">
                                    <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                                    <div class="add-to-links">
                                      <!-- <button type="button" data-toggle="tooltip" title="Add to Wish List" onClick=""><i class="fa fa-heart"></i></button>
                                      <button type="button" data-toggle="tooltip" title="Compare this Product" onClick=""><i class="fa fa-exchange"></i></button> -->
                                    </div>
                                  </div>
                                </div>
                              <?php
                          }   
                        }
                        ?> 
              </div>
          
              <h3 class="subtitle">produk terbaru</h3>
              <div class="side-item">
                  <?php 
                    if(count($slide_baru)>0){  
                      foreach ($slide_baru as $key) {
                        $c = array (' ');
                        $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                        $nm_produk = strtolower(str_replace($d,"",$key->nama_produk));
                        $link = strtolower(str_replace($c, '-', $nm_produk)); 
                        $harga = $key->harga;
                        ?>  
                          <div class="product-thumb clearfix">
                              <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $key->gbr_kecil; ?>" alt="<?php echo $key->nama_produk; ?>" title="<?php echo $key->nama_produk; ?>" class="img-responsive" /></a></div>
                              <div class="caption">
                                <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><?php echo $key->nama_produk; ?></a></h4>
                                <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($harga,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                              </div>
                          </div>        
                        <?php
                      }  
                    }
                  ?>
              </div>
            </aside> 
        <!--Right Part End -->
      </div>
    </div>
  </div>