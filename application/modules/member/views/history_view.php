<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url();?>member">Member</a></li>
        <li><a href="<?php echo base_url();?>member/history">History</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h4 class="title">History Transaksi - Anda Di Online Store</h4>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td class="text-center">Tanggal Transaksi</td>
                    <td class="text-left">Jumlah Produk Yang Dibeli</td>                
                  </tr>
                </thead>
                <tbody>
                  <?php 
                      if(count($history->result_array())>0){
                        foreach ($history->result_array() as $in ) 
                        {
                         echo "
                                <tr>
                                    <td class='text-center'><a href='".base_url()."member/dethistory/".substr($in['kode_transaksi'],0,8)."'>".substr($in['kode_transaksi'],6,2)."-".substr($in['kode_transaksi'],4,2)."-".substr($in['kode_transaksi'],0,4)."</a></td>
                                    <td class='text-left'>".$in['jm']." Produk</td>
                                </tr>
                            ";
                        }
                      }else{
                        echo "<tr><td class='text-center' colspan='2'>Maaf, belum ada transaksi selama anda berbelanja di website kami.</td></tr>";
                      }  
                  ?>
                </tbody>
              </table>
            </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>