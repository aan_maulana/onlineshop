<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="login.html">Member</a></li>
        <li><a href="login.html">Login</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-9">
          <h1 class="title">Member Login</h1>
          <div class="row">
            <div class="col-sm-6">
              <h2 class="subtitle">Customer Baru</h2>
              <p><strong>Register Member</strong></p>
              <p>Dengan membuat account member pembeli dapat lebih cepat untuk proses transaksi pembelian, lebih up to date tentang order status, dapat melakukan cek history belanja.</p>
              <a href="register.html" class="btn btn-primary">Register Member</a> </div>
            <div class="col-sm-6">
            <form method="post" action="<?php echo base_url(); ?>member/aksilogin">
              <h2 class="subtitle">Login Pelanggan Terdaftar</h2>
              <p><strong>I am a returning customer</strong></p>
                <div class="form-group">
                  <label class="control-label" for="input-email">Username</label>
                  <input type="text" name="username" value="" placeholder="username" id="input-username" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Password</label>
                  <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                </div>
                <div class="form-group">
                  <?php echo $gbr_captcha; ?>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Kode Captcha</label>
                  <input type="text" name="captcha" value="" placeholder="captcha" id="captcha" class="form-control">
                  <br>
                  <a href="#">Lupa Password</a></div>
                <input type="submit" value="Login" class="btn btn-primary">
            </form>  
            </div>
          </div>
        </div>
        <!--Middle Part End -->
        <!--Right Part Start -->
        <!-- <aside id="column-right" class="col-sm-3 hidden-xs">
          <h3 class="subtitle">Account</h3>
          <div class="list-group">
            <ul class="list-item">
              <li><a href="login.html">Login</a></li>
              <li><a href="register.html">Register</a></li>
              <li><a href="#">Forgotten Password</a></li>
              <li><a href="#">My Account</a></li>
              <li><a href="#">Address Books</a></li>
              <li><a href="wishlist.html">Wish List</a></li>
              <li><a href="#">Order History</a></li>
              <li><a href="#">Downloads</a></li>
              <li><a href="#">Reward Points</a></li>
              <li><a href="#">Returns</a></li>
              <li><a href="#">Transactions</a></li>
              <li><a href="#">Newsletter</a></li>
              <li><a href="#">Recurring payments</a></li>
            </ul>
          </div>
        </aside> -->
        <!--Right Part End -->
      </div>
    </div>
  </div>