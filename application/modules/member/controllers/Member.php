<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('captcha','date','text_helper'));
		$this->load->library('email');
		if(!isset($_SESSION)){
		    session_start();
		}
	}
	function index()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!="")
		{
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];	
			$judul = '';
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = $judul."- Member online store login form ";
			$this->load->view('home/top_menu',$data);
			$this->load->view('member_view',$data);
			$this->load->view('home/footer_menu');
		}else{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
		}
	}
	function login()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "Log In Member - Online Store, Toko Online Store Termurah dan Terlengkap di Indonesia - Peace Online Store";
			$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '200',
			'img_height' => 60,
			'expiration' => 90
			);
			$cap = create_captcha($vals);
			$datamasuk = array(
				'captcha_time' => $cap['time'],
				'ip_address' => $this->input->ip_address(),
				'word' => $cap['word']
				);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$query = $this->db->insert_string('captcha', $datamasuk);
			$this->db->query($query);
			$data['gbr_captcha'] = $cap['image'];
			$this->load->view('home/top_menu',$data);
			$this->load->view('login_view'	);
			$this->load->view('home/footer_menu');
		}
	}
	function logout()
	{
		session_destroy();
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."'>";
	}
	function aksilogin()
	{
		$username = $this->kacang->anti(($this->input->post('username')));
		$pwd = $this->kacang->anti(($this->input->post('password')));
		$hasil = $this->home_model->data_login_member($username,$pwd);
		$expiration = time()-9000;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
		{
			?>
			<script type="text/javascript">
			alert("Captcha salah. Ulangi lagi...!!!");			
			</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
		}
		else
		{
			if (!ctype_alnum($username) OR !ctype_alnum($pwd)){
				?>
				<script type="text/javascript">
				alert("Protected....!!!");			
				</script>
				<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
			}
			else{
				if (count($hasil->result_array())>0){
					$sql_hapus  = "delete FROM captcha";
					$query = $this->db->query($sql_hapus);
					foreach($hasil->result() as $items){
						$session_username=$items->username_user."|".$items->nama."|".$items->kode_user."|".$items->email;
					}
					$_SESSION['username_online_shop']=$session_username;
					?>
						<script type="text/javascript">
							alert('login berhasil');			
						</script>
					<?php
					echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
				}
				else{
					?>
					<script type="text/javascript">
					alert("Username atau Password Yang Anda Masukkan Salah atau Anda Belum Mengaktifkan Link Aktivasi Yang Telah Kami Kirimkan Via Email..!!!");			
					</script>
					<?php
					echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
				}
			}
		}
	}
	function set_profil()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["kd_user"]=$pecah[2];
			$data["nama"]=$pecah[1];
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['slide_laris'] = $this->home_model->tampil_slide_produk_terlaris_kiri(5)->result();
			$data['slide_baru'] = $this->home_model->tampil_produk(5)->result();
			$det_member = $this->home_model->pilih_member($data["kd_user"])->result();
	        	$i = 0;
	        	foreach ($det_member as $key) {
	          		$i++;
	          		$data['default']['nama']  		= $key->nama;
	          		$data['default']['email']  		= $key->email;
	          		$data['default']['alamat'] 		= $key->alamat;
	          		$data['default']['telpon'] 		= $key->telpon;
	          		$data['default']['propinsi'] 	= $key->propinsi;
	          		$data['default']['kota']	 	= $key->kota;
	          		$data['default']['kodepos']	 	= $key->kode_pos;
	        		$data['birthdate']				= $key->tgl_lahir;
	        	}
			$data['judul'] = "- Form Set Profile Member -";
			// $isi['default']['nama'] = '';
			$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '200',
			'img_height' => 60,
			'expiration' => 90
			);
			$cap = create_captcha($vals);
			$datamasuk = array(
				'captcha_time' => $cap['time'],
				'ip_address' => $this->input->ip_address(),
				'word' => $cap['word']
				);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$query = $this->db->insert_string('captcha', $datamasuk);
			$this->db->query($query);
			$data['gbr_captcha'] = $cap['image'];
			$this->load->view('home/top_menu',$data);
			$this->load->view('set_profile_view');
			$this->load->view('home/footer_menu');
		}
		else{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
		}
	}
	function update_profil()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session==""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data["email"]=$pecah[3];
			$data['judul'] = "Log In Member - Online Store, Toko online store termurah di indonesia";
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$det_member = $this->home_model->pilih_member($data["kd_user"])->result();
			$datapesan['nama'] 		= $this->kacang->anti(($this->input->post('nama')));
			$datapesan['email'] 	= $this->kacang->anti(($this->input->post('email')));
			$datapesan['alamat'] 	= $this->kacang->anti(($this->input->post('alamat')));
			$datapesan['telpon'] 	= $this->kacang->anti(($this->input->post('telpon')));
			$datapesan['propinsi'] 	= $this->kacang->anti(($this->input->post('propinsi')));
			$datapesan['kota'] 		= $this->kacang->anti(($this->input->post('kota')));
			$datapesan['kodepos'] 	= $this->kacang->anti(($this->input->post('kodepos')));
			$tgl = $this->kacang->anti($this->input->post('tgl'));
			$bln = $this->kacang->anti($this->input->post('bln'));
			$thn = $this->kacang->anti($this->input->post('thn'));
			$datapesan['tgl_lahir'] = $tgl.'-'.$bln.'-'.$thn;
			// echo '<script type="text/javascript">alert("Data has been submitted to ' . $datapesan['nama'] . '");</script>';
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
			if(isset($_POST['captcha']) && ($_POST['captcha']!=null) ){
			$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
			$query = $this->db->query($sql, $binds);
			$row = $query->row();
				$data['pesan'] = "";
				if ($row->count == 0)
				{
					$data['pesan'] = "Kode Captcha yang anda masukkan tidak Valid...!!!";
				}
				else
				{
					if($datapesan['nama']=="" && $datapesan['email']=="" && $datapesan['alamat']=="" && $datapesan['telpon']=="" && $datapesan['propinsi']=="" && $datapesan['kota']=="" && $datapesan['tgl_lahir']=="" && $datapesan['kodepos']=="")
					{
						$data['pesan'] = "Field belum lengkap. Mohon diisi dengan selengkap-lengkapnya.";
					}
					else
					{
						$sql_hapus  = "delete FROM captcha";
						$query = $this->db->query($sql_hapus);
						if($datapesan['email']==$data["email"])
						{
							$q_update = "update tbl_user set nama='".$datapesan['nama']."',email='".$datapesan['email']."',alamat='".$datapesan['alamat']."',telpon='".$datapesan['telpon']."',propinsi='".$datapesan['propinsi']."',kota='".$datapesan['kota']."',tgl_lahir='".$datapesan['tgl_lahir']."',kode_pos='".$datapesan['kodepos']."' where kode_user='".$pecah[2]."'";
							$this->home_model->update_profil_member($q_update);
							$data['pesan'] = "Berhasil memperbaharui data profil anda.";
							?>
								<script>
								alert('Berhasil memperbaharui data profil anda.');
								</script>
							<?php
							echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
						}
						else
						{
							$q = $this->home_model->cek_email($datapesan['email']);
							if (count($q->result())>0){
								$data['pesan'] =  "Email yang anda masukkan sudah terpakai. Silahkan pilih yang lainnya";
							}
							else
							{
								$q_update = "update tbl_user set nama='".$datapesan['nama']."',email='".$datapesan['email']."',alamat='".$datapesan['alamat']."',telpon='".$datapesan['telpon']."',propinsi='".$datapesan['propinsi']."',kota='".$datapesan['kota']."',tgl_lahir='".$datapesan['tgl_lahir']."' where kode_user='".$pecah[2]."'";
								$this->home_model->update_profil_member($q_update);
								$data['pesan'] = "Berhasil memperbaharui data profil anda.";
								?>
									<script>
									alert('Berhasil memperbaharui data profil anda.');
									</script>
								<?php
								echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
							}
						}
					}
				}
			}else{
				$data['pesan'] = "Kode Captcha yang anda masukkan tidak Valid...!!!";
			}
			$this->load->view('home/top_menu',$data);
			$this->load->view('update_member_result');
			$this->load->view('home/footer_menu');
		}
	}
	function history()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session==""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data['judul'] = "History Transaksi - Online Store, Toko online store termurah di indonesia";
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$page=$this->uri->segment(3);
			$limit=15;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$data['history'] = $this->home_model->tampil_semua_history($data["kd_user"],$limit,$offset);
			$tot_hal = $this->home_model->hitung_isi_1tabel('tbl_transaksi_header','group by substring(kode_transaksi,1,8)');
			$config['base_url'] = base_url() . 'member/history/';
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 3;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$this->load->view('home/top_menu',$data);
			$this->load->view('history_view');
			$this->load->view('home/footer_menu');
		}
	}
	function dethistory()
	{
		$kode='';
		if ($this->uri->segment(3) === FALSE)
		{
    			$kode='';
		}
		else
		{
    			$kode = $this->uri->segment(3);
		}
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session==""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data['judul'] = "History Transaksi - Online Store, Toko online store termurah di indonesia";
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$page=$this->uri->segment(4);
			$limit=15;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			$data['history'] = $this->home_model->tampil_det_history($data["kd_user"],$kode,$limit,$offset)->result();
			$tot_hal = $this->home_model->hitung_isi_1tabel("tbl_transaksi_detail","where kode_transaksi like '%$kode%'");
			$history = $this->home_model->tampil_det_history($data["kd_user"],$kode,$limit,$offset);
			// update rating
				// if(count($history->result_array())>0){
				// 	foreach($history->result_array() as $in)
				// 	{
				// 		// $tot = $in['harga']*$in['jumlah'];
				// 		// 	echo "<tr>
				// 		// 			<td class='td-keranjang' width='80' rowspan=2>".substr($in['kode_transaksi'],6,2)."-".substr($in['kode_transaksi'],4,2)."-".substr($in['kode_transaksi'],0,4)."</td>
				// 		// 			<td class='td-keranjang' rowspan=2>".$in['kode_transaksi']."</td>
				// 		// 			<td class='td-keranjang'>".$in['nama_produk']."</td>
				// 		// 			<td id='".$in['kode_transaksi_detail']."' class='td-keranjang star' rowspan='2'>tes</td>
				// 		// 		</tr>
				// 		// 		<tr><td class='td-keranjang'>Rp. ".number_format($in['harga'],2,',','.')." x ".$in['jumlah']." = Rp. ".number_format($tot,2,',','.')."</td>
				// 		// 		</tr>";
				// 		$kode_produk = $in['kode_produk'];
				// 		$data["kode_produk"] = $in['kode_produk'];
				// 		$data["is_rated"]   = $this->home_model->get_user_numrate($kode_produk,$data["kd_user"]);
				// 	}
				// }
			// end rating
			$config['base_url'] = base_url() . 'member/dethistory/'.$kode;
				$config['total_rows'] = $tot_hal->num_rows();
				$config['per_page'] = $limit;
				$config['uri_segment'] = 4;
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$this->load->view('home/top_menu',$data);
			$this->load->view('history_det_view');
			$this->load->view('home/footer_menu');
		}
	}

	function set_pass()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["default"]['username'] = $pecah[0]; 
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data['judul'] = "History Transaksi - Online Store, Toko online store termurah di indonesia";
			$data['slide_laris'] = $this->home_model->tampil_slide_produk_terlaris_kiri(5)->result();
			$data['slide_baru'] = $this->home_model->tampil_produk(5)->result();
			$data['slide_rekomendasi'] = $this->home_model->tampil_slide_produk(6)->result();
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '200',
			'img_height' => 60,
			'expiration' => 90
			);
			$cap = create_captcha($vals);
			$datamasuk = array(
				'captcha_time' => $cap['time'],
				'ip_address' => $this->input->ip_address(),
				'word' => $cap['word']
				);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$query = $this->db->insert_string('captcha', $datamasuk);
			$this->db->query($query);
			$data['gbr_captcha'] = $cap['image'];
			$this->load->view('home/top_menu',$data);
			$this->load->view('set_pass_view');
			$this->load->view('home/footer_menu');
		}
		else{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
		}
	}
	function update_pass()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session==""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data['judul'] = "History Transaksi - Online Store, Toko online store termurah di indonesia";
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$datapesan['username'] = $this->kacang->anti($this->input->post('username'));
			$datapesan['passlama'] = $this->kacang->anti($this->input->post('passlama'));
			$datapesan['passbaru'] = $this->kacang->anti($this->input->post('passbaru'));
			$datapesan['ulangi']   = $this->kacang->anti($this->input->post('ulangi'));
			$bersih = md5($datapesan['passbaru']);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
			$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
			$query = $this->db->query($sql, $binds);
			$row = $query->row();
			$data['pesan'] = "";
			if ($row->count == 0)
			{
				$data['pesan'] = "Kode Captcha yang anda masukkan tidak Valid...!!!";
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/set_pass'>";
			}
			else
			{
				if($datapesan['passlama']=="" && $datapesan['passbaru']=="" && $datapesan['ulangi']=="")
				{
					$data['pesan'] = "Field belum lengkap. Mohon diisi dengan selengkap-lengkapnya.";
				}
				else
				{
					$sql_hapus  = "delete FROM captcha";
					$query = $this->db->query($sql_hapus);
					$cek = $this->home_model->data_login_member($datapesan['username'],$datapesan['passlama']);
					if($datapesan['ulangi']==$datapesan['passbaru']){
						if(count($cek->result())>0){
							$q_update = "update tbl_user set pass_user='".$bersih."' where kode_user='".$pecah[2]."'";
							$this->home_model->update_profil_member($q_update);
							$data['pesan'] = "Berhasil memperbaharui data password anda.";
							?>
								<script>
								alert('Berhasil memperbaharui data passowrd anda.');
								</script>
							<?php
							echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
						}
						else{
							$data['pesan'] = "Password Lama Salah. Gagal memperbaharui data password anda.";
							?>
								<script>
								alert('Password Lama Salah. Gagal memperbaharui data password anda.');
								</script>
							<?php
							echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/set_pass'>";
						}
					}
					else{
						$data['pesan'] = "Password Tidak Sama/Sinkron. Gagal memperbaharui data password anda.";
						?>
							<script>
							alert('Password Tidak Sama/Sinkron. Gagal memperbaharui data password anda.');
							</script>
						<?php
						echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/set_pass'>";
					}
				}
			}
			$this->load->view('home/top_menu',$data);
			$this->load->view('update_pass_result');
			$this->load->view('home/footer_menu');
		}
	}
	function daftar()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "Daftar Member - LKLTSK Online Store, Toko Online Store Termurah dan Terlengkap di indonesia";
			$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '200',
			'img_height' => 60,
			'expiration' => 90
			);
			$cap = create_captcha($vals);
			$datamasuk = array(
				'captcha_time' => $cap['time'],
				'ip_address' => $this->input->ip_address(),
				'word' => $cap['word']
				);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$query = $this->db->insert_string('captcha', $datamasuk);
			$this->db->query($query);
			$data['gbr_captcha'] = $cap['image'];
			$this->load->view('home/top_menu',$data);
			$this->load->view('register_view');
			$this->load->view('home/footer_menu');
		}
	}
	function kirimregister()
	{
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}
		else{
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "Daftar Member - LKLTSK Online Store, Toko Online Store Termurah dan Terlengkap di indonesia";
			$datapesan['username_user'] = $this->kacang->anti($this->input->post('username'));
			$datapesan['pass_user'] = md5($this->input->post('password'));
			$datapesan['nama'] = $this->kacang->anti($this->input->post('nama'));
			$datapesan['email'] = $this->kacang->anti($this->input->post('email'));
			$datapesan['alamat'] = $this->kacang->anti($this->input->post('alamat'));
			$datapesan['telpon'] = $this->kacang->anti($this->input->post('telpon'));
			$datapesan['propinsi'] = $this->kacang->anti($this->input->post('propinsi'));
			$datapesan['kota'] = $this->kacang->anti($this->input->post('kota'));
			$datapesan['kode_pos'] = $this->kacang->anti($this->input->post('kodepos'));
			// $tgl = $this->input->post('tgl');
			// $bln = $this->input->post('bln');
			// $thn = $this->input->post('thn');
			// $datapesan['tgl_lahir'] = $tgl.'-'.$bln.'-'.$thn;
			$datapesan['stts'] = 0;
			$datapesan['kode_aktivasi'] = md5($datapesan['username_user']);
			$expiration = time()-900;
			$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
			$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
			$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
			$query = $this->db->query($sql, $binds);
			$row = $query->row();
			$data['pesan'] = "";
			if ($row->count == 0)
			{
				$data['pesan'] = "Kode Captcha yang anda masukkan tidak Valid...!!!";
			}
			else
			{
					$q = $this->sandal_model->cek_username($datapesan['username_user'],$datapesan['email']);
					if (count($q->result())>0){
						$data['pesan'] =  "Username atau Email yang anda masukkan sudah terpakai. Silahkan pilih yang lainnya";
					}
					else{
						$sql_hapus  = "delete FROM captcha";
						$query = $this->db->query($sql_hapus);
							$this->sandal_model->register_member($datapesan);
						//$format_pesan = "Nama : ".$datapesan['nama']."".$this->email->clear()." Email : ".$datapesan['email']."".$this->email->clear()." Telpon : ".$datapesan['telpon']."".$this->email->clear()." Alamat : ".$datapesan['alamat']."".$this->email->clear()."
						//Kota : ".$datapesan['kota']."".$this->email->clear()." Negara : ".$datapesan['negara']."".$this->email->clear()." Pesan : ".$datapesan['pesan'];
						$this->email->from("aan.h.maulana13@gmail.com", "Admin online Store LKLTSK");
						$this->email->to($datapesan['email']);
						$this->email->set_mailtype('html');
						$this->email->subject('Aktivasi Keanggotaan - LKLTSK Online Store');
						$this->email->message('Terima kasih telah menjadi member di website kami. Klik link berikut ini untuk mengaktifkan akun anda.
						<br>' . base_url() . "member/aktivasi/" . $datapesan['kode_aktivasi']);
						$ml = true;
						if($ml==TRUE)
						{
							$data['pesan'] = "Email aktivasi telah dikirimkan ke email anda. Silahkan login ke akun email anda, periksa pada folder inbox atau pesan dan klik link aktivasi.";
							?>
							<script>
							alert('Email aktivasi telah dikirimkan ke email anda. Silahkan login ke akun email anda, periksa pada folder inbox atau pesan dan klik link aktivasi.');
							</script>
							<?php
							echo "<meta http-equiv='refresh' content='0; url=".base_url()."'>";
						}
						else
						{
							$data['pesan'] = "Gagal membuat akun. Silahkan ulangi kembali.";
							?>
							<script>
							alert('Gagal membuat akun. Silahkan ulangi kembali.');
							</script>
							<?php
							echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/daftar'>";
						}
				}
			}
			$this->load->view('home/top_menu',$data);
			$this->load->view('register_view');
			$this->load->view('home/footer_menu');
		}
	}	
	public function create_rate(){
		$score = $this->input->post("score");
		$pid = $this->input->post("pid");
		$id = $this->input->post('id_transaksi');
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		$pecah=explode("|",$session);
		$user_id = $pecah[2];
		if($score == 0){
			$this->db->where('user_id',$user_id);
			$this->db->where('produk_id',$pid);
			$this->db->delete('tbl_user_rating');
		}else{
			$ckdata = $this->db->get_where('tbl_user_rating',array('user_id'=>$user_id,'produk_id'=>$pid));
			if(count($ckdata->result())>0){
				$simpan = array('user_id'=>$user_id,
					'produk_id'=>$pid,
					'rating'=>$score);
				$this->db->where('user_id',$user_id);
				$this->db->where('produk_id',$pid);
				$this->db->update('tbl_user_rating',$simpan);
			}else{
				$simpan = array('user_id'=>$user_id,
					'produk_id'=>$pid,
					'rating'=>$score);
				$this->db->insert('tbl_user_rating',$simpan);
			}
		}
	}
}
