<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url();?>kategori"><?php echo $this->uri->segment(1); ?></a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Left Part Start -->
        <aside id="column-left" class="col-sm-3 hidden-xs">
          <h3 class="subtitle">Categories</h3>
          <div class="box-category">
            <ul id="cat_accordion">
              <?php
                if(count($menu)>0){
                  foreach ($menu as $scroll) {
                    $nme_link1 =  $scroll->id_kategori.'-'.$scroll->nama_kategori;
                    $Id1     = strtolower(str_replace(" ", "-", $nme_link1));
                    $sub     = $this->home_model->menu_kategori('1',$scroll->id_kategori)->result();
                    ?>
                      <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $Id1;?>"><?php echo $scroll->nama_kategori; ?></a> 
                          <?php if(count($sub)>0){
                            ?><span class="down"></span>                            
                              <ul>
                                 <?php 
                                   foreach ($sub as $subside) { 
                                      $nme_link2 = $subside->id_kategori.'-'.$subside->nama_kategori;
                                      $id2       = strtolower(str_replace(" ", "-", $nme_link2));
                                      $sub2      = $this->home_model->menu_kategori('2',$subside->id_kategori)->result();  
                                    ?>
                                      <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $id2;?>"><?php echo $subside->nama_kategori; ?></a>
                                          <?php if (count($sub2)>0) {
                                                ?>
                                                   <span class="down"></span>
                                                   <ul>
                                                      <?php
                                                          foreach ($sub2 as $subside2) {
                                                              $nme_link3  = $subside2->id_kategori.'-'.$subside->nama_kategori;
                                                              $id3        = strtolower(str_replace(" ", "-", $nme_link3));
                                                              $sub3       = $this->home_model->menu_kategori('3',$subside->id_kategori)->result();
                                                            ?>
                                                              <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $id3;?>"><?php echo $subside2->nama_kategori; ?></a></li> 
                                                            <?php   
                                                          } 
                                                      ?>
                                                   </ul> 
                                                <?php
                                                } 
                                          ?>
                                      </li>                  
                                    <?php
                                   }
                                 ?>
                              </ul>
                          <?php } ?>


                      </li>
                    <?php
                  }
                }   
              ?>
            </ul>
          </div>
          <h3 class="subtitle">By Brands</h3>
          <div class="box-category">
            <ul id="cat_accordion">
              <?php
                if(count($perbrand)>0){
                  foreach ($perbrand as $scroll) {
                    ?>
                      <li><a href="<?php echo base_url();?>brand/get_produk/<?php echo $scroll->brand;?>"><?php echo $scroll->brand; ?></a></li>
                    <?php
                  }
                }   
              ?>
            </ul>
          </div>  



          <!-- <div class="side-item">
            <?php 
                if(count($kategori)>0){  
                  foreach ($kategori as $laris) {
                    $cn = array (' ');
                    $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                    $nm_produkn = strtolower(str_replace($dn,"",$laris->nama_produk));
                    $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                    $hargan = $laris->harga;
                    $stok   = $laris->stok;
                    ?>  
                        <form method="post" action="<?php echo base_url();?>keranjang/tambah_barang">
                        <input type="hidden" name="kdproduk" value="<?php echo $laris->kode_produk;?>"/>
                        <input type="hidden" name="quantity" value="1"/>
                        <input type="hidden" name="hargana" value="<?php echo $laris->harga; ?>"/>
                        <input type="hidden" name="nmproduk" value="<?php echo $laris->nama_produk;?>"/>
                          <div class="product-thumb clearfix">
                            <div class="image"><a href="<?php echo base_url();?>assets/product/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $laris->gbr_kecil; ?>" alt="<?php echo $laris->nama_produk; ?>" title="<?php echo $laris->nama_produk; ?>" class="img-responsive" /></a></div>
                            <div>
                              <div class="caption">
                                <h4><a href="<?php echo base_url();?>assets/product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"> <?php echo $laris->nama_produk; ?> </a></h4>
                                <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span>
                              </div>
                            </div>
                          </div>
                        </form>
                    <?php
                }   
              }
              ?>
          </div> -->

          <!-- <div class="banner owl-carousel owl-theme" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper owl-origin" style="width: 1052px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(-263px, 0px, 0px); transform-origin: 394.5px center 0px; perspective-origin: 394.5px center;"><div class="owl-item owl-fade-out" style="width: 263px; position: relative; left: 263px;"><div class="item"> <a href="#"><img src="image/banner/small-banner1-265x350.jpg" alt="small banner" class="img-responsive"></a> </div></div><div class="owl-item owl-fade-in" style="width: 263px;"><div class="item"> <a href="#"><img src="image/banner/small-banner-265x350.jpg" alt="small banner1" class="img-responsive"></a> </div></div></div></div>
          </div> -->
        </aside>
        <!--Left Part End -->
        <!--Middle Part Start-->
        <div id="content" class="col-sm-9">
          <!-- <h3 class="title">Kategori <?php 
                            $kode = $this->uri->segment(3);
                            echo  explode("-",$kode)[1];?> <?php echo explode("-", $kode)[2]; ?> </h3> -->
         <div class="row products-category">
         <?php 
           if(count($brand_content) > 0 ) {
              foreach ($brand_content as $data) {
                $stts = "";
                $mati = "";
                $stok = $data->stok;
                if($stok > 0 ){
                    $tss = '<span style="margin:0px auto; padding:0px; font-size:12px;"><b>Ada</b></span>';
                    $mati = ""; 
                }else{
                    $tss = '<span style="margin:0px auto; padding:0px; font-size:12px; color:red;"><b>Habis</b></span>';
                    $mati = "disabled";
                } 
                $c = array (' ');
                $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $s = strtolower(str_replace($d,"",$data->nama_produk));
                $linkdata = strtolower(str_replace($c, '-', $s));
                ?> 
                        <form method="post" action="<?php echo base_url();?>keranjang/tambah_barang">
                        <input type="hidden" name="kdproduk" value="<?php echo $data->kode_produk;?>"/>
                        <input type="hidden" name="quantity" value="1"/>
                        <input type="hidden" name="hargana" value="<?php echo $data->harga; ?>"/>
                        <input type="hidden" name="nmproduk" value="<?php echo $data->nama_produk;?>"/>
                        <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
                          <div class="product-thumb">
                            <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($data->kode_produk)?>-<?php echo $linkdata; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $data->gbr_kecil; ?>" alt="<?php echo $data->nama_produk; ?>" title="<?php echo $data->nama_produk; ?>" class="img-responsive"></a></div>
                            <div>
                              <div class="caption">
                                <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($data->kode_produk)?>-<?php echo $linkdata; ?>"><?php echo $data->nama_produk; ?></a></h4>
                                <p class="price"><span class="price-new">Harga Rp. <?php echo number_format($data->harga,2,',','.'); ?></span></p>
                              </div>
                              <div class="button-group">
                                <button class="btn-primary" type="submit" onclick=""><span>Beli</span></button>
                              </div>
                            </div>
                          </div>
                        </div>
                        </form>
                <?php
              }
           }else{
              echo "Maaf, belum ada produk pada kategori ini. <br>Silahkan melihat-lihat koleksi produk kami pada kategori yang lainnya.";
           }
         ?>
         </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>