<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		
	    if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    } 
	}

	function index()
	{	
		$this->get_produk();				
	}

	function get_produk(){
			$page=rawurldecode($this->kacang->anti($this->uri->segment(3)));
	      	$limit=15;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$data['brand_content'] = $this->home_model->tampil_brand_content($page)->result();
			$data['perbrand'] = $this->home_model->tampil_brand()->result();
			// $tot_hal = $this->home_model->hitung_isi_1tabel('tbl_produk','where id_kategori in ('.$ubh_kode.')');
			// $desk_kat = $this->home_model->hitung_isi_1tabel('tbl_kategori','where id_kategori='.$p_kode[0].'');
	
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "- LKLTSK Online Store Fashion Termurah Indonesia - ";

			$this->load->view('home/top_menu',$data);
			$this->load->view('brand_view_main');
			$this->load->view('home/footer_menu');
	}

}
