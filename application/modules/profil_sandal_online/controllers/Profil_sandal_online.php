<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil_sandal_online extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	    if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    } 
	}

	public function index()
	{	
		$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
		$data['slide_data'] = $this->home_model->tampil_banner()->result();
		$data['slide_produk'] = $this->home_model->tampil_slide_produk(10)->result();
		$data['slide_baru'] = $this->home_model->tampil_produk(20)->result();		
			
		$data['slide_pria'] = $this->home_model->tampil_produk_pertipe('pria',15)->result();
		$data['slide_wanita'] = $this->home_model->tampil_produk_pertipe('wanita',15)->result();		
		$data['slide_girls'] = $this->home_model->tampil_produk_pertipe('girls',15)->result();
		$data['slide_boys'] = $this->home_model->tampil_produk_pertipe('boys',15)->result();


		$this->load->view('home/top_menu',$data);	
		$this->load->view('profil_view');
		$this->load->view('home/footer_menu');
	}
}
