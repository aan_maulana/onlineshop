  <!-- top menu  -->

  <!-- end menu  -->

  <div id="container">
    
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-xs-12">
          <!-- Slideshow Start-->
          <div class="slideshow single-slider owl-carousel">
            <?php
              if(count($slide_data)>0){
                foreach ($slide_data as $key) {
                    $gambar = $key->gambar;
                    ?>
                    <div class="item"> <a href="#"><img class="img-responsive" src="<?php echo base_url()?>assets/image/slider/<?php echo $gambar; ?>" alt="banner 2" /></a> </div>
                  <?php
                }
              }
            ?>  
          </div>
          <!-- Slideshow End-->
          <!-- Banner Start-->
          
          <!-- <div class="marketshop-banner">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-3-300x300.jpg" alt="Sample Banner 2" title="Sample Banner 2" /></a></div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-1-300x300.jpg" alt="Sample Banner" title="Sample Banner" /></a></div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-2-300x300.jpg" alt="Sample Banner 3" title="Sample Banner 3" /></a></div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-4-300x300.jpg" alt="Sample Banner 4" title="Sample Banner 4" /></a></div>
            </div>
          </div> -->
          
          <!-- Banner End-->
          <!-- Product Tab Start -->
          <div id="product-tab" class="product-tab">
  <ul id="tabs" class="tabs">
    <li><a href="#tab-featured">Slide Produk</a></li>
    <li><a href="#tab-latest">Terbaru</a></li>
    <li><a href="#tab-bestseller">Bestseller</a></li>
    <!-- <li><a href="#tab-special">Special</a></li> -->
  </ul>
  <div id="tab-featured" class="tab_content">
      <div class="owl-carousel product_carousel_tab">                     
          <?php 
            if(count($slide_produk)>0){  
              foreach ($slide_produk as $key) {
                $c = array (' ');
                $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produk = strtolower(str_replace($d,"",$key->nama_produk));
                $link = strtolower(str_replace($c, '-', $nm_produk)); 
                $harga = $key->harga;
                ?>  
                  <div class="product-thumb clearfix">
                    <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $key->gbr_kecil; ?>" alt="<?php echo $key->nama_produk; ?>" title="<?php echo $key->nama_produk; ?>" class="img-responsive" /></a></div>
                    <div class="caption">
                      <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($key->kode_produk)?>-<?php echo $link; ?>"><?php echo $key->nama_produk; ?></a></h4>
                      <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($harga,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                    </div>
                    <div class="button-group">
                      <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                      <div class="add-to-links">
                      </div>
                    </div>
                  </div>
                <?php
              }  
            }
            ?>
      </div>
  </div>

  <div id="tab-latest" class="tab_content">
    <div class="owl-carousel product_carousel_tab">
            <?php 
            if(count($slide_baru)>0){  
              foreach ($slide_baru as $new) {
                $cn = array (' ');
                $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produkn = strtolower(str_replace($dn,"",$new->nama_produk));
                $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                $hargan = $new->harga;
                $stok   = $new->stok;
                ?>  
                  <div class="product-thumb clearfix">
                    <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($new->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $new->gbr_kecil; ?>" alt="<?php echo $new->nama_produk; ?>" title="<?php echo $new->nama_produk; ?>" class="img-responsive" /></a></div>
                    <div class="caption">
                      <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($new->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $new->nama_produk; ?></a></h4>
                      <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                    </div>
                    <div class="button-group">
                      <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                      <div class="add-to-links">
                        <!-- <button type="button" data-toggle="tooltip" title="Add to Wish List" onClick=""><i class="fa fa-heart"></i></button>
                        <button type="button" data-toggle="tooltip" title="Compare this Product" onClick=""><i class="fa fa-exchange"></i></button> -->
                      </div>
                    </div>
                  </div>
                <?php
            }   
          }
          ?>
    </div>
  </div>

  <div id="tab-bestseller" class="tab_content">
    <div class="owl-carousel product_carousel_tab">
          <?php 
            if(count($slide_laris)>0){  
              foreach ($slide_laris as $laris) {
                $cn = array (' ');
                $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produkn = strtolower(str_replace($dn,"",$laris->nama_produk));
                $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                $hargan = $laris->harga;
                $stok   = $laris->stok;
                ?>  
                  <div class="product-thumb clearfix">
                    <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $laris->gbr_kecil; ?>" alt="<?php echo $laris->nama_produk; ?>" title="<?php echo $laris->nama_produk; ?>" class="img-responsive" /></a></div>
                    <div class="caption">
                      <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $laris->nama_produk; ?></a></h4>
                      <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span> <!-- <span class="price-old">$122.00</span> --> <span class="saving">-20%</span> </p>
                    </div>
                    <div class="button-group">
                      <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                      <div class="add-to-links">
                        <!-- <button type="button" data-toggle="tooltip" title="Add to Wish List" onClick=""><i class="fa fa-heart"></i></button>
                        <button type="button" data-toggle="tooltip" title="Compare this Product" onClick=""><i class="fa fa-exchange"></i></button> -->
                      </div>
                    </div>
                  </div>
                <?php
            }   
          }
          ?>        
    </div>
  </div>
  <div id="tab-special" class="tab_content">
    <div class="owl-carousel product_carousel_tab">
                 
    </div>
  </div>
  
</div>    <!-- Product Tab Start -->
          <!-- Banner Start -->
          
          <!-- <div class="marketshop-banner">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-4-600x250.jpg" alt="2 Block Banner" title="2 Block Banner" /></a></div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><a href="#"><img src="<?php echo base_url()?>assets/image/banner/sample-banner-5-600x250.jpg" alt="2 Block Banner 1" title="2 Block Banner 1" /></a></div>
            </div>
          </div> -->
          
          <!-- Banner End -->
          <!-- Categories Product Slider Start-->
          <div class="category-module" id="latest_category">
            <h3 class="subtitle">Fashion - <a class="viewall" href="category.tpl">view all</a></h3>
            <div class="category-module-content">
              <ul id="sub-cat" class="tabs">
                <li><a href="#tab-cat1">Men</a></li>
                <li><a href="#tab-cat2">Women</a></li>
                <li><a href="#tab-cat3">Girls</a></li>
                <li><a href="#tab-cat4">Boys</a></li>
                <!-- <li><a href="#tab-cat5">Baby</a></li> -->
                <!-- <li><a href="#tab-cat6">Accessories</a></li> -->
              </ul>
              <div id="tab-cat1" class="tab_content">
                <div class="owl-carousel latest_category_tabs">
                  <?php 
                  foreach ($slide_pria as $sp) {
                    $cn = array (' ');
                    $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                    $nm_produk = strtolower(str_replace($dn,"",$sp->nama_produk));
                    $link = strtolower(str_replace($cn, '-', $nm_produk));
                    $hargan = $sp->harga;
                    $stok   = $sp->stok;
                  ?>
                    <div class="product-thumb">
                      <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sp->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $sp->gbr_kecil; ?>" alt="<?php echo $sp->nama_produk; ?>" title="<?php echo $sp->nama_produk; ?>" class="img-responsive" /></a></div>
                      <div class="caption">
                        <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sp->kode_produk)?>-<?php echo $link; ?>"><?php echo $sp->nama_produk; ?></a></h4>
                        <p class="price"> <span class="price-new">Harga Rp.  <?php echo number_format($hargan,2,',','.'); ?></span></p>
                      </div>
                      <div class="button-group">
                        <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                        <!-- <div class="add-to-links">
                          <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                          <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                        </div> -->
                      </div>
                    </div>
                   <?php 
                  }  
                 ?> 
                </div>
              </div>
              <div id="tab-cat2" class="tab_content">
                <div class="owl-carousel latest_category_tabs">
                    <?php 
                      foreach ($slide_wanita as $sw) {
                        $cn = array (' ');
                        $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                        $nm_produk = strtolower(str_replace($dn,"",$sw->nama_produk));
                        $link = strtolower(str_replace($cn, '-', $nm_produk));
                        $hargan = $sw->harga;
                        $stok   = $sw->stok;
                      ?>
                        <div class="product-thumb">
                          <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $sw->gbr_kecil; ?>" alt="<?php echo $sw->nama_produk; ?>" title="<?php echo $sw->nama_produk; ?>" class="img-responsive" /></a></div>
                          <div class="caption">
                            <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><?php echo $sw->nama_produk; ?></a></h4>
                            <p class="price"> <span class="price-new">Harga Rp.  <?php echo number_format($hargan,2,',','.'); ?></span></p>
                          </div>
                          <div class="button-group">
                            <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                            <!-- <div class="add-to-links">
                              <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                              <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                            </div> -->
                          </div>
                        </div>
                       <?php 
                      }  
                     ?>  
                </div>
              </div>
              <div id="tab-cat3" class="tab_content">
                <div class="owl-carousel latest_category_tabs">
                  
                  <?php 
                      foreach ($slide_girls as $sw) {
                        $cn = array (' ');
                        $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                        $nm_produk = strtolower(str_replace($dn,"",$sw->nama_produk));
                        $link = strtolower(str_replace($cn, '-', $nm_produk));
                        $hargan = $sw->harga;
                        $stok   = $sw->stok;
                      ?>
                        <div class="product-thumb">
                          <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $sw->gbr_kecil; ?>" alt="<?php echo $sw->nama_produk; ?>" title="<?php echo $sw->nama_produk; ?>" class="img-responsive" /></a></div>
                          <div class="caption">
                            <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><?php echo $sw->nama_produk; ?></a></h4>
                            <p class="price"> <span class="price-new">Harga Rp.  <?php echo number_format($hargan,2,',','.'); ?></span></p>
                          </div>
                          <div class="button-group">
                            <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                            <!-- <div class="add-to-links">
                              <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                              <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                            </div> -->
                          </div>
                        </div>
                       <?php 
                      }  
                     ?>  

                </div>
              </div>
              <div id="tab-cat4" class="tab_content">
                <div class="owl-carousel latest_category_tabs">
                  
                  <?php 
                      foreach ($slide_boys as $sw) {
                        $cn = array (' ');
                        $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                        $nm_produk = strtolower(str_replace($dn,"",$sw->nama_produk));
                        $link = strtolower(str_replace($cn, '-', $nm_produk));
                        $hargan = $sw->harga;
                        $stok   = $sw->stok;
                      ?>
                        <div class="product-thumb">
                          <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><img src="<?php echo base_url()?>assets/produk/<?php echo $sw->gbr_kecil; ?>" alt="<?php echo $sw->nama_produk; ?>" title="<?php echo $sw->nama_produk; ?>" class="img-responsive" /></a></div>
                          <div class="caption">
                            <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($sw->kode_produk)?>-<?php echo $link; ?>"><?php echo $sw->nama_produk; ?></a></h4>
                            <p class="price"> <span class="price-new">Harga Rp.  <?php echo number_format($hargan,2,',','.'); ?></span></p>
                          </div>
                          <div class="button-group">
                            <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                            <!-- <div class="add-to-links">
                              <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                              <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                            </div> -->
                          </div>
                        </div>
                       <?php 
                      }  
                     ?>  

                </div>
              </div>
            </div>
          </div>

          <!-- Categories Product Slider End-->
          
          <!-- Categories Product Slider Start -->
          <!-- <h3 class="subtitle">Western Wear - <a class="viewall" href="category.html">view all</a></h3> -->
          <!-- <div class="owl-carousel latest_category_carousel">
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/iphone_6-220x330.jpg" alt="Hair Care Cream for Men" title="Hair Care Cream for Men" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Hair Care Cream for Men</a></h4>
                <p class="price"> $134.00 </p>
                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/nikon_d300_5-220x330.jpg" alt="Hair Care Products" title="Hair Care Products" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Hair Care Products</a></h4>
                <p class="price"> <span class="price-new">$66.80</span> <span class="price-old">$90.80</span> <span class="saving">-27%</span> </p>
                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/nikon_d300_4-220x330.jpg" alt="Bed Head Foxy Curls Contour Cream" title="Bed Head Foxy Curls Contour Cream" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Bed Head Foxy Curls Contour Cream</a></h4>
                <p class="price"> $88.00 </p>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href=""><img src="<?php echo base_url()?>assets/image/product/macbook_5-220x330.jpg" alt="Shower Gel Perfume for Women" title="Shower Gel Perfume for Women" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Shower Gel Perfume for Women</a></h4>
                <p class="price"> <span class="price-new">$95.00</span> <span class="price-old">$99.00</span> <span class="saving">-4%</span> </p>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick="cart.add('61');"><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick="wishlist.add('61');"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick="compare.add('61');"><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/macbook_4-220x330.jpg" alt="Perfumes for Women" title="Perfumes for Women" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Perfumes for Women</a></h4>
                <p class="price"> $85.00 </p>
                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/macbook_3-220x330.jpg" alt="Make Up for Naturally Beautiful Better" title="Make Up for Naturally Beautiful Better" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Make Up for Naturally Beautiful Better</a></h4>
                <p class="price"> $123.00 </p>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
            <div class="product-thumb">
              <div class="image"><a href="product.html"><img src="<?php echo base_url()?>assets/image/product/macbook_2-220x330.jpg" alt="Pnina Tornai Perfume" title="Pnina Tornai Perfume" class="img-responsive" /></a></div>
              <div class="caption">
                <h4><a href="product.html">Pnina Tornai Perfume</a></h4>
                <p class="price"> $110.00 </p>
              </div>
              <div class="button-group">
                <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                <div class="add-to-links">
                  <button type="button" data-toggle="tooltip" title="Add to wishlist" onClick=""><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="Add to compare" onClick=""><i class="fa fa-exchange"></i></button>
                </div>
              </div>
            </div>
          </div> -->
          <!-- Categories Product Slider End -->
          
          <!-- Brand Logo Carousel Start-->
          
          <!-- <div id="carousel" class="owl-carousel nxt">
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/apple_logo-100x100.jpg" alt="Palm" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/canon_logo-100x100.jpg" alt="Sony" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/apple_logo-100x100.jpg" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/canon_logo-100x100.jpg" alt="Apple" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/apple_logo-100x100.jpg" alt="HTC" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/canon_logo-100x100.jpg" alt="Hewlett-Packard" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/apple_logo-100x100.jpg" alt="brand" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="<?php echo base_url()?>assets/image/product/canon_logo-100x100.jpg" alt="brand1" class="img-responsive" /></a> </div>
          </div> -->
         
          <!-- Brand Logo Carousel End -->
        </div>
        <!--Middle Part End-->
      </div>
    </div>
  </div>
  <!-- Feature Box Start-->

    <!-- <div class="container">
      <div class="custom-feature-box row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature-box fbox_1">
            <div class="title">Free Shipping</div>
            <p>Free shipping on order over $1000</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature-box fbox_2">
            <div class="title">Free Return</div>
            <p>Free return in 24 hour after purchasing</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature-box fbox_3">
            <div class="title">Gift Cards</div>
            <p>Give the special perfect gift</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature-box fbox_4">
            <div class="title">Reward Points</div>
            <p>Earn and spend with ease</p>
          </div>
        </div>
      </div>
    </div> -->
    
    <!-- Feature Box End-->
 