<div id="container">
<div class="container">
  <!-- Breadcrumb Start-->
  <ul class="breadcrumb">
    <li><a href="index.html"><i class="fa fa-home"></i></a></li>
    <li><a href="cart.html">Shopping Cart</a></li>
  </ul>
  <!-- Breadcrumb End-->
  <div class="row">
    <!--Middle Part Start-->
    <?php echo form_open('keranjang/update_keranjang'); ?>
    <div id="content" class="col-sm-12">
      <h1 class="title">Shopping Cart</h1>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <!-- <td class="text-center">Gambar</td> -->
              <td class="text-left">Nama Produk</td>

              <td class="text-left">QTY</td>
              <td class="text-right">Harga</td>
              <td class="text-right">Sub Total</td>
            </tr>
          </thead>
          <tbody>
                <?php $i = 1; ?>
                <?php foreach ($this->cart->contents() as $items): ?>
                  <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                    <tr <?php if($i>=1){ echo 'class="alt"'; }?>>
                      <!-- <td class="text-center"><a href="product.html"><img src="<?php echo base_url();?>assets/produk/<?php echo $items['options']['gbkecil']; ?>" data-image="<?php echo base_url();?>assets/produk/<?php echo $items['options']['gbkecil']; ?>" title="<?php echo $items['name']; ?>"></a></td> -->
                      <td class="text-left"><a href="product.html"><?php echo $items['name']; ?></a><br>
                      </td>

                      <td class="text-left">
                        <div class="input-group btn-block quantity">
                          <input type="text" name="quantity[]" value="<?php echo $items['qty']; ?>"  class="form-control">
                          <span class="input-group-btn">
                          <div class="pull-right">
                            <a href="<?php echo base_url(); ?>keranjang/remove_cart/<?php echo $items['rowid']; ?>" data-original-title="Remove" class="btn btn-danger"><strong>X</strong></a>
                          </div>
                          </span>
                        </div>
                      </td>
                      <td class="text-right"><?php echo  number_format($items['price'],2,",",".") ?></td>
                      <td class="text-right">Rp <?php
                                            $harga=number_format($items['subtotal'],2,",",".");
                                            echo $harga;
                                    ?>
                      </td>
                    </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-4 col-sm-offset-8">
          <table class="table table-bordered">
            <tr>
              <td class="text-right"><strong>Total Belanja:</strong></td>
              <td class="text-right">Rp <?php $harga_tot=number_format($this->cart->total(),2,",","."); echo $harga_tot; ?></td>
            </tr>
          </table>
        </div>
      </div>
        <div class="buttons">
          <div class="pull-left"><a href="<?php echo base_url(); ?>kategori" class="btn btn-default">Continue Shopping</a></div>
          <span class="input-group-btn">
            <div class="pull-right"><a href="<?php echo base_url(); ?>checkout" class="btn btn-primary">Checkout</a></div>
            <div class="pull-right">
                <button type="submit" data-toggle="update keranjang" title="" class="btn btn-update" data-original-title="update keranjang">Update</button>
            </div>
          </span>
        </div>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>
</div>
</div>
