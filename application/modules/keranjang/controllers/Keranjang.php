<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keranjang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	    if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    } 
	}

	function index()
	{

		$judul = '';
		$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
		$data['judul'] = $judul."- Online Distro Pakaian";

		$this->load->view('home/top_menu',$data);
		$this->load->view('keranjang/keranjang_view',$data);
		$this->load->view('home/footer_menu');
	}

	function tambah_barang()
	{	
		$kdproduk = $this->input->post('kdproduk');
		$qty = $this->input->post('quantity');
		$price = $this->input->post('hargana');
		$nmproduk = $this->input->post('nmproduk');

		$product = $this->home_model->cari_produk($kdproduk);		
		if ($product->num_rows() > 0){

			$row = $product->row();

			$harga 			= $row->harga;
			$nama_produk 	= $row->nama_produk;
			$image 			= $row->gbr_kecil; 
			
			$data = array(
			'id'      	=> $kdproduk,
			'qty'     	=> $qty,
		    'price'   	=> $harga,
			'name'    	=> $nama_produk,
			'options'   => array('gbkecil'=>$image)
			);

			$this->cart->insert($data);	
		}
		
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang/'>";
	}

	function add_item($kdproduk=null)
	{	
		$kdproduk = $this->input->post('kdproduk');
		// $qty = $this->input->post('quantity');
		// $price = $this->input->post('hargana');
		// $nmproduk = $this->input->post('nmproduk');

		$product = $this->home_model->cari_produk($kdproduk);		
		if ($product->num_rows() > 0){
			$row = $product->row();
			$harga 			= $row->harga;
			$nama_produk 	= $row->nama_produk;
			$image 			= $row->gbr_kecil; 
			
			$data = array(
			'id'      	=> $kdproduk,
			'qty'     	=> 1,
		    'price'   	=> $harga,
			'name'    	=> $nama_produk,
			'options'   => array('gbkecil'=>$image)
			);

			$this->cart->insert($data);	
		}
		
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang/'>";
	}

	function update_keranjang()
	{
		$total = $this->cart->total_items();
		$item = $this->input->post('rowid');
		$qty = $this->input->post('quantity');
		for($i=0;$i < count($item); $i++)
		{
			$data = array(
			'rowid' => $item[$i],
			'qty'   => $qty[$i]);
			$this->cart->update($data);
		}

		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang/'>";
	}


	function remove_cart($kode)
	{
		$id='';
		if ($this->uri->segment(3) === FALSE)
		{
    			$id='';
		}
		else
		{
    			$id = $this->uri->segment(3);
		}
		$data = array(
			'rowid' => $kode,
			'qty'   => 0);
			$this->cart->update($data);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang/'>";
	}
}
