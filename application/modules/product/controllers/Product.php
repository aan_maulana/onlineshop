<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	    if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    } 
	}
	function index()
	{
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."'>";
	}
	function detail()
	{
		$kode='';		
		if ($this->uri->segment(3) === FALSE)
		{
    			$kode='';
		}
		else
		{
    			$kode = $this->uri->segment(3);
		}
		$p_kode = explode("-",$kode);
		$data['detail_produk'] = $this->home_model->tampil_detail_produk($p_kode[0])->result();
		$judul = "";
		$kd_kategori = "";
		$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
		$data['judul'] = $judul."- Grosir Baju Online Termurah";
		$data['slide_laris'] = $this->home_model->tampil_slide_produk_terlaris_kiri(5)->result();
		$data['slide_baru'] = $this->home_model->tampil_produk(5)->result();
		/*Show Rekomendasi*/
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!=""){
			$pecah=explode("|",$session);
			$data["nama"]=$pecah[1];
	        $user_id = $pecah[2];
	        $query = $this->db->query("SELECT * FROM `tbl_user_rating` ORDER BY `user_id`");
	        $ratings = array();
	        foreach ($query->result_array() as $row) {
	          	$ratings[$row['user_id']][$row['produk_id']] = $row['rating'];
	        }
	        $recommendations = $this->getRecommendation($ratings, $user_id);
	        if(!empty($recommendations)){
	          	$data['rekomen'] = $this->getProduk($recommendations[0], $recommendations[1]);
	        }
	        $data['cek'] = "login";
	        
		}
		$this->load->view('home/top_menu',$data);
		$this->load->view('product/produk_view',$data);
		$this->load->view('home/footer_menu');
	}
	public function rekomendasi(){
	    $user_id = "6";
		$query = $this->db->query("SELECT * FROM `user_rating` ORDER BY `user_id`")->result_array();
		$ratings = array();
		foreach ($query as $row) {
			$ratings[$row['user_id']][$row['movie_id']] = $row['rating'];
		}
		$recommendations = $this->getRecommendation($ratings, $user_id);
		$html = '';
		if(!empty($recommendations)){
			$movies = $this->getMovies($recommendations[0], $recommendations[1]);
		
			foreach($movies[0] as $movie){
				$html .= '
					<div class="span12" style="margin-top: 10px;">
						<h4 style="cursor:pointer;" title="Click to see who has this movie." class="has-movie" data-id="'.$movie[1].'">'.$movie[0].'</h4><span>Based on likes of: ';
				
				foreach($movies[1][$movie[0]] as $user){
					$html .= $user.', ';
				}
				
				$html = rtrim($html, " ,");
				$html .= '</span></div>';
			}
		}
		echo $html;
	}
	function getUser(){
		$query = $this->db->query("SELECT `kode_user`, `nama` FROM `tbl_user`");
		$user = array();
		foreach ($query->result_array() as $row) {
			$user[$row['kode_user']] = $row['nama'];
		}
		return $user;
	}	
	function getProduk($r, $u){
		$users = $this->getUser();
		$related = array();
		$produk = array();
		foreach($r as $i){
			$query = $this->db->query("SELECT `kode_produk`, `nama_produk` FROM `tbl_produk` WHERE `kode_produk` = '$i'");
			foreach ($query->result_array() as $row) {
				$produk[] = array($row['nama_produk'], $row['kode_produk']);
				foreach($u[$i] as $id){
					$related[$row['nama_produk']][] = $users[$id];
				}
			}
		}
		return array($produk,$related);
	}
	function getRecommendation($prefs, $person, $n = 5){
		$totals = array();
		$simSums = array();
		$based_on_users = array();
		foreach($prefs as $other=>$val){
		        if($other == $person) continue;
		        $sim = $this->similarity($prefs, $person, $other);
		        if($sim <= 0) continue;
		        foreach($prefs[$other] as $item => $rating){
		                if(!in_array($item, array_keys($prefs[$person]))){
		                        $totals[$item] = 0;
		                        $totals[$item] += $prefs[$other][$item]*$sim;
		                        $simSums[$item] = 0;
		                        $simSums[$item] += $sim;
		                        $based_on_users[$item][] = $other;
		                }
		        }
	 	}
		$rankings = array();
		foreach($totals as $item => $total){
			$rankings[$total] = $item;
		}
		krsort($rankings);
		$list = array();
		foreach($rankings as $total => $item){
			$list[] = $item;
		}
		return array($list, $based_on_users);
	}
	function similarity($prefs, $p1, $p2){
		$si = array();
		if(!array_key_exists($p1, $prefs)) return 0;
		foreach($prefs[$p1] as $item => $value){	
	        	if(array_key_exists($item, $prefs[$p2])){
	                	$si[$item] = 1;
		        }
		}
		$n = sizeof($si);
		if($n == 0)
			return 0;
			#For numerator sigma(x*y) - (sigma(x)* sigma(y)/n)
		#              sigma(x*y) - ( sum1 * sum2 / n)
		$sum1 = 0;
		$sum2 = 0;
		$pSum = 0;
		$sum1sq = 0;
		$sum2sq = 0;
		foreach($si as $it => $rate){
			$sum1 += $prefs[$p1][$it];
			$sum2 += $prefs[$p2][$it];        	
			$pSum += $prefs[$p1][$it] * $prefs[$p2][$it];
			$sum1sq += $prefs[$p1][$it] * $prefs[$p1][$it];
			$sum2sq += $prefs[$p2][$it] * $prefs[$p2][$it];
		}
			#For denominator underoot( ( sigma(x^2)-((sigma(x))^2/n) ) * ( sigma(y^2)-((sigma(y)^2)/n) ) )
		#                underoot( ( sum1sq-(sum^2/n) ) * ( sum2sq-((sum2^2)/n) ) )
		$num = $pSum - ($sum1 * $sum2 / $n);
		$den = sqrt(($sum1sq - (($sum1 * $sum1) / $n)) * ($sum2sq - (($sum2 * $sum2) / $n)));
		if($den == 0)
			return 0;
			$sim_corr = $num/$den;
		return $sim_corr;
	}
}
