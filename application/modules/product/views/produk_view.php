<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/js/swipebox/src/css/swipebox.min.css" />
<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo base_url();?>" itemprop="url"><span itemprop="Home"><i class="fa fa-home"></i></span></a></li>
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo base_url();?>kategori" itemprop="url" title="Kategori"><span itemprop="Kategori">Kategori</span></a></li>
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo base_url();?>produk" itemprop="url" title="Product"><span itemprop="Product">Product</span></a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">

        <!--Middle Part Start-->
        <div id="content" class="col-sm-9">
          <div itemscope="" itemtype="http://schema.org/Product">
            <?php
            if(count($detail_produk)>0){
              foreach ($detail_produk as $new) {
                $kdproduk   = $new->kode_produk;
                $cn         = array (' ');
                $dn         = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produkn = strtolower(str_replace($dn,"",$new->nama_produk));
                $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                $hargan = $new->harga;
                $stok   = $new->stok;
                $desk   = $new->deskripsi;
                $gkecil = $new->gbr_kecil;
                $gbesar = $new->gbr_besar;
                ?>
                <h1 class="title" itemprop="name"><?php echo ucwords($nm_produkn); ?></h1>
                <div class="row product-info">
                  <div class="col-sm-6">
                    <div class="image"><div style="height:350px;width:350px;" class="zoomWrapper"><img class="img-responsive" itemprop="image" id="zoom_01" src="<?php echo base_url()?>assets/produk/detail/<?php echo $gbesar; ?>" title="<?php echo $nm_produkn; ?>" alt="<?php echo $nm_produkn; ?>" data-zoom-image="<?php echo base_url()?>assets/produk/detail/<?php echo $gbesar; ?>" style="position: absolute;"></div></div>
                    <div class="center-block text-center"><span class="zoom-gallery"><i class="fa fa-search"></i> Click image for Gallery</span></div>
                    <div class="image-additional" id="gallery_01">
                          <a class="thumbnail" href="#" data-zoom-image="<?php echo base_url()?>assets/produk/detail/<?php echo $gbesar; ?>" data-image="<?php echo base_url();?>assets/produk/detail/<?php echo $gbesar;?>" title="<?php echo $nm_produkn; ?>"> <img src="<?php echo base_url()?>assets/produk/detail/<?php echo $gkecil; ?>" title="<?php echo $nm_produkn; ?>" alt="<?php echo $nm_produkn; ?>"></a>
                          <a class="thumbnail" href="#" data-zoom-image="<?php echo base_url();?>assets/produk/detail/<?php echo $gbesar; ?>" data-image="<?php echo base_url();?>assets/produk/detail/<?php echo $gbesar;?>" title="<?php echo $nm_produkn; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $gkecil; ?>" title="<?php echo $nm_produkn; ?>" alt="<?php echo $nm_produkn; ?>"></a>
                          <a class="thumbnail" href="#" data-zoom-image="<?php echo base_url();?>assets/produk/detail/<?php echo $gbesar;?>" data-image="<?php echo base_url();?>assets/produk/detail/<?php echo $gbesar;?>" title="<?php echo $nm_produkn; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $gkecil; ?>" title="<?php echo $nm_produkn; ?>" alt="<?php echo $nm_produkn; ?>"></a>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <ul class="list-unstyled description">
                      <!-- <li><b>Brand:</b> <a href="#"><span itemprop="brand">Apple</span></a></li> -->
                      <li><b>Product Code:</b> <span itemprop="mpn"><?php echo $kdproduk; ?></span></li>
                      <?php
                        if($stok>0){
                         ?>
                            <li><b>Stock Barang:</b> <span class="instock">Tersedia</span></li>
                          <?php
                        }else{
                          ?>
                            <li><b>Stock Barang:</b> <span class="nostock">Tidak Tersedia</span></li>
                          <?php
                        }
                      ?>
                    </ul>
                    <ul class="price-box">
                      <li class="price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"> <span itemprop="price">Rp. <?php echo number_format($hargan,2,',','.'); ?><span itemprop="availability" content="In Stock"></span></span></li>
                    </ul>
                    <div id="product">
                      <!-- <h3 class="subtitle">Available Options</h3> -->
                      <!-- <div class="form-group required">
                        <label class="control-label">Color</label>
                        <select class="form-control" id="input-option200" name="option[200]">
                          <option value=""> --- Please Select --- </option>
                          <option value="4">Black </option>
                          <option value="3">Silver </option>
                          <option value="1">Green </option>
                          <option value="2">Blue </option>
                        </select>
                      </div> -->
                      <div class="cart">
                        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>keranjang/tambah_barang">
                          <div>
                            <div class="qty">
                              <label class="control-label" for="input-quantity">Qty</label>
                              <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control">
                              <input type="hidden" name="kdproduk" id="kdproduk" value="<?php echo $kdproduk; ?>">
                              <input type="hidden" name="hargana" id="hargana" value="<?php echo $hargan; ?>">
                              <input type="hidden" name="nmproduk" id="nmproduk" value="<?php echo $nm_produkn; ?>">
                              <a class="qtyBtn plus" href="javascript:void(0);">+</a><br>
                              <a class="qtyBtn mines" href="javascript:void(0);">-</a>
                              <div class="clear"></div>
                            </div>
                            <button type="submit" id="button-cart" class="btn btn-primary btn-sm">Add to Cart</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <hr>
                    <!-- AddThis Button BEGIN -->
                    <div class="addthis_toolbox addthis_default_style"> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_google_plusone" g:plusone:size="medium"></a> <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal" pi:pinit:url="http://www.addthis.com/features/pinterest" pi:pinit:media="http://www.addthis.com/cms-content/images/features/pinterest-lg.png"></a> <a class="addthis_counter addthis_pill_style"></a> </div>
                    <script id="facebook-jssdk" src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.4"></script><script id="twitter-wjs" src="https://platform.twitter.com/widgets.js"></script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-514863386b357649"></script>
                    <!-- AddThis Button END -->
                  </div>
                </div>


                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab-description" data-toggle="tab">Deskripsi Produk</a></li>
                  <!-- <li><a href="#tab-specification" data-toggle="tab">Specification</a></li> -->
                  <!-- <li><a href="#tab-review" data-toggle="tab">Reviews (2)</a></li> -->
                </ul>
                <div class="tab-content">
                  <div itemprop="description" id="tab-description" class="tab-pane active">
                    <div>
                        <?php echo $desk; ?>
                    </div>
                  </div>
                </div>
                <?php
              }
            }else{
              ?>
                <div id="content" class="col-sm-12">
                  <h1 class="title-404 text-center">404</h1>
                  <p class="text-center lead">Maaf !<br>
                    Produk Yang Anda Cari Tidak Ditemukan! </p>
                </div>
              <?php
            }
            ?>
            <h3 class="subtitle">Recommended Products</h3>
            <div class="owl-carousel related_pro owl-theme" style="opacity: 1; display: block;">
                
                <?php
                $html = "";
                foreach($rekomen[0] as $row){
                  $kode = $row['1'];
                  $ckbarang = $this->db->query("SELECT * FROM tbl_produk WHERE kode_produk = '$kode'");
                  $x = $ckbarang->row();
                  $harga = "Harga Rp. " . number_format($x->harga,2,',','.');
                  $stok = number_format($x->stok,2,',','.');
                  $foto = $x->gbr_kecil;
                  $c = array (' ');
                  $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                  $s = strtolower(str_replace($d,"",$row['0']));
                  $linkdata = strtolower(str_replace($c, '-', $s));
                  $html .='<form method="post" action="'.base_url().'keranjang/tambah_barang"><div class="owl-wrapper-outer">
                  <input type="hidden" name="kdproduk" value="'.$row['1'].'"/>
                  <input type="hidden" name="quantity" value="1"/>
                  <input type="hidden" name="hargana" value="'.$x->harga.'"/>
                  <input type="hidden" name="nmproduk" value="'.$row['0'].'"/>
                  <div class="owl-wrapper" style="width: 2040px; left: 0px; display: block;">
                    <div class="owl-item" style="width: 170px;">
                      <div class="product-thumb">
                        <div class="image">
                          <a href="'.base_url().'product/detail/'.strtolower($row['1']).'-'.$linkdata.'">
                            <img src="'.base_url().'assets/produk/detail/'.$foto.'" alt="'.$row['0'].'" title="'.$row['0'].'" class="img-responsive">
                          </a>
                        </div>
                        <div class="caption">
                          <h4><a href="'.base_url().'product/detail/'.strtolower($row['1']).'-'.$linkdata.'">'.$row['0'].'</a></h4>
                          <p class="price"> <span class="price-new">'.$harga.'</span> </p>
                        </div>
                        <div class="button-group">
                          <button class="btn-primary" type="submit" onclick=""><span>Beli</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div></form>';
                  // $html .= '
                  //   <div class="span12" style="margin-top: 10px;">
                  //     <h4 style="cursor:pointer;" title="Click to see who has this movie." class="has-movie" data-id="'.$row[1].'">'.$row[0].'</h4><span>Based on likes of: ';
                  
                  //       foreach($rekomen[1][$row[0]] as $user){
                  //         $html .= $user.', ';
                  //       }
                        
                  //       $html = rtrim($html, " ,");
                  //       $html .= '</span></div>';
                }
                echo $html;
                ?>
                


              </div>
          </div>
        </div>
        <!--Middle Part End -->
        <!--Right Part Start -->
        <aside id="column-right" class="col-sm-3 hidden-xs">
          <h3 class="subtitle">Bestsellers</h3>
          <div class="side-item">
          <?php
            if(count($slide_laris)>0){
              foreach ($slide_laris as $laris) {
                $cn = array (' ');
                $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produkn = strtolower(str_replace($dn,"",$laris->nama_produk));
                $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                $hargan = $laris->harga;
                $stok   = $laris->stok;
                ?>
                  <div class="product-thumb clearfix">
                    <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $laris->gbr_kecil; ?>" alt="<?php echo $laris->nama_produk; ?>" title="<?php echo $laris->nama_produk; ?>" class="img-responsive" /></a></div>
                    <div class="caption">
                      <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($laris->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $laris->nama_produk; ?></a></h4>
                      <p class="price"><span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span></p>
                    </div>
                  </div>
                <?php
            }
          }
          ?>
          </div>
          <div class="list-group">
            <h3 class="subtitle">Custom Content</h3>
            <p>This is a CMS block edited from admin. You can insert any content (HTML, Text, Images) Here. </p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          </div>
          <h3 class="subtitle">produk terbaru</h3>
          <div class="side-item">
            <?php
            if(count($slide_baru)>0){
              foreach ($slide_baru as $new) {
                $cn = array (' ');
                $dn = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
                $nm_produkn = strtolower(str_replace($dn,"",$new->nama_produk));
                $linkn = strtolower(str_replace($cn, '-', $nm_produkn));
                $hargan = $new->harga;
                $stok   = $new->stok;
                ?>
                  <div class="product-thumb clearfix">
                    <div class="image"><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($new->kode_produk)?>-<?php echo $linkn; ?>"><img src="<?php echo base_url()?>assets/produk/detail/<?php echo $new->gbr_kecil; ?>" alt="<?php echo $new->nama_produk; ?>" title="<?php echo $new->nama_produk; ?>" class="img-responsive"></a></div>
                    <div class="caption">
                      <h4><a href="<?php echo base_url();?>product/detail/<?php echo strtolower($new->kode_produk)?>-<?php echo $linkn; ?>"><?php echo $new->nama_produk; ?></a></h4>
                      <p class="price"> <span class="price-new">Harga Rp. <?php echo number_format($hargan,2,',','.'); ?></span></p>
                    </div>
                  </div>
                <?php
            }
          }
          ?>
          </div>
        </aside>
        <!--Right Part End -->
      </div>
    </div>
  </div>
