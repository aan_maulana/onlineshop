<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prod_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    /*==================================== data_produk ============================================*/


    function data_produk($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                SELECT tp.kode_produk AS kode_produk,tp.nama_produk AS nama,tk.nama_kategori AS nama_kategori,
                        tp.stok AS stok,tp.harga AS harga,tp.stts AS stts,tp.tipe_produk AS tipe
                        FROM tbl_produk tp
                        JOIN tbl_kategori tk ON tp.id_kategori = tk.id_kategori
                        ORDER BY tp.nama_produk ASC
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }

    function data_produk_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT tp.kode_produk AS kode_produk,tp.nama_produk AS nama,tk.nama_kategori AS nama_kategori,
                        tp.stok AS stok,tp.harga AS harga,tp.stts AS stts,tp.tipe_produk AS tipe
                        FROM tbl_produk tp
                        JOIN tbl_kategori tk ON tp.id_kategori = tk.id_kategori
                        ORDER BY tp.nama_produk ASC  
            ) A
        ");
        return $query;
    }   

}