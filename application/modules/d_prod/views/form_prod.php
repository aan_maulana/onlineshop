<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/duit.js"></script>
<div class="row">
    <div class="col-md-12">
    	

		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <ul class="nav nav-pills">
				<li class="active"><a href="#nav-pills-tab-1" data-toggle="tab" aria-expanded="true">Produk</a></li>
				<li class=""><a href="#nav-pills-tab-2" data-toggle="tab" aria-expanded="false">Detail Pic Produk</a></li>
			</ul>

		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    <div class="tab-content">
		    <div class="tab-pane fade active in" id="nav-pills-tab-1">
			    <div class="panel-body panel-form">
			        <form class="form-horizontal form-bordered" action="<?php echo base_url();?>d_prod/<?php echo $action;?>" method="post" data-parsley-validate="true" enctype="multipart/form-data" >
	                    <?php
	                    if($cek==='add'){
	                    	?> 		
	                    		<div class="form-group">
									<label class="control-label col-md-3 col-sm-3">Kode Produk * :</label>
									<div class="col-md-3 col-sm-3">
										<input class="form-control" type="text" id="kode_produk" minlength="2" name="kode_produk" value="<?php echo set_value('kode_produk',isset($default['kode_produk']) ? $default['kode_produk'] : ''); ?>" data-type="kode_produk" placeholder="Masukan Kode Produk" data-parsley-required="true" data-parsley-minlength="2"/>
									</div>
								</div>
	                    	<?php
	                    	}
	                    ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Nama Produk * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="nama" minlength="2" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" placeholder="Masukan Nama Produk" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Brand Produk * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="brand" minlength="2" name="brand" value="<?php echo set_value('brand',isset($default['brand']) ? $default['brand'] : ''); ?>" data-type="brand" placeholder="Masukan brand Produk" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3">Kategori * :</label>
	                        <div class="col-md-3 col-sm-3">
	                            <?PHP echo form_dropdown('jnskategori',$option_kategori,isset($default['nama_kategori']) ? $default['nama_kategori'] : '','id="jnskategori" data-size="20" data-parsley-required="true" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
	                        </div>
	                    </div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Harga Produk * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="harga" minlength="2" name="harga" value="<?php echo set_value('harga',isset($default['harga']) ? $default['harga'] : ''); ?>" data-type="harga" placeholder="Masukan harga Produk" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">stok Produk * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="stok" minlength="1" name="stok" value="<?php echo set_value('stok',isset($default['stok']) ? $default['stok'] : ''); ?>" data-type="stok" placeholder="Masukan stok Produk" data-parsley-required="true" data-parsley-minlength="1"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">tipe Produk * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="tipe" minlength="1" name="tipe" value="<?php echo set_value('tipe',isset($default['tipe']) ? $default['tipe'] : ''); ?>" data-type="tipe" placeholder="Masukan tipe Produk" data-parsley-required="true" data-parsley-minlength="1"/>
							</div>
						</div>
						<div class="form-group">                                        
							<label class="control-label col-md-3 col-sm-3">desc produk * :</label>                                        
							<div class="col-md-5 col-sm-5">                                           
								 <textarea class="form-control" data-parsley-group="wizard-step-2" data-parsley-required="true" id="desc" name="desc" rows="3"><?php echo set_value('desc',isset($default['desc']) ? $default['desc'] : ''); ?></textarea>
							</div>                                    
						</div>
						<?php
	                    if($cek=='edit'){
	                        ?>
	                        <div class="form-group">
	                            <label class="control-label col-md-3 col-sm-3">Foto Aset :</label>
	                            <div class="col-md-2 col-sm-2">
	                            <?php
	                                if($foto==""){
	                                    $fotox = "no.png";
	                                }else{
	                                    $fotox = $foto;
	                                }
	                            ?>	
	                                <a class="fancybox" href="<?php echo base_url()?>assets/produk/detail/<?php echo $fotox?>" style="width:150px;text-align:center;height:180px;">
	                                    <img class="fancybox" src="<?php echo base_url();?>assets/produk/detail/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
	                                </a>
	                            </div>
	                        </div>
	                        <?php   
	                    }
	                    ?>
						<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3">Foto Kecil :</label>
	                        <div class="col-md-3 col-sm-3">
	                            <input name="MAX_FILE_SIZE" value="1024000" type="hidden">
	                            <input type="file" id="foto" name="foto" />  
	                        </div>
	                    </div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3"></label>
							<div class="col-md-3 col-sm-3">
								<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
	                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
							</div>
						</div>
			        </form>
			    </div>
		    </div>
		    <div class="tab-pane fade" id="nav-pills-tab-2">
		    	<div class="panel-body panel-form">
			        <form class="form-horizontal form-bordered" action="<?php echo base_url();?>d_prod/<?php echo $action;?>" method="post" data-parsley-validate="true" enctype="multipart/form-data" >
	                    <?php
	                    if($cek==='add'){
	                    	?> 		
	                    		<div class="form-group">
									<label class="control-label col-md-3 col-sm-3">Kode Produk * :</label>
									<div class="col-md-3 col-sm-3">
										<input class="form-control" type="text" id="kode_produk" minlength="2" name="kode_produk" value="<?php echo set_value('kode_produk',isset($default['kode_produk']) ? $default['kode_produk'] : ''); ?>" data-type="kode_produk" placeholder="Masukan Kode Produk" data-parsley-required="true" data-parsley-minlength="2"/>
									</div>
								</div>
	                    	<?php
	                    	}
	                    ?>
						<?php
	                    if($cek=='edit'){
	                        ?>
	                        <div class="form-group">
	                            <label class="control-label col-md-3 col-sm-3">Foto Aset :</label>
	                            <div class="col-md-2 col-sm-2">
	                            <?php
	                                if($foto==""){
	                                    $fotox = "no.png";
	                                }else{
	                                    $fotox = $foto;
	                                }
	                            ?>	
	                                <a class="fancybox" href="<?php echo base_url()?>assets/produk/detail/<?php echo $fotox?>" style="width:150px;text-align:center;height:180px;">
	                                    <img class="fancybox" src="<?php echo base_url();?>assets/produk/detail/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
	                                </a>
	                            </div>
	                        </div>
	                        <?php   
	                    }
	                    ?>
						<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3">Foto Kecil :</label>
	                        <div class="col-md-3 col-sm-3">
	                            <input name="MAX_FILE_SIZE" value="1024000" type="hidden">
	                            <input type="file" id="foto" name="foto" />  
	                        </div>
	                    </div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3"></label>
							<div class="col-md-3 col-sm-3">
								<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
	                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
							</div>
						</div>
			        </form>
			    </div> 
		    </div>
		    </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	 jQuery(document).ready(function(){
	    jQuery('#harga').priceFormat({
	        prefix: '',
	        centsSeparator: ',',
	        thousandsSeparator: ','
	    });
	});	
	 $("#stok").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });  

 	  
</script>