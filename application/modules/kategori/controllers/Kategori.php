<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		
	    if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    } 
	}

	function index()
	{	
		$this->get_produk();				
	}

	function get_produk(){
			
			$page=$this->uri->segment(3);
	      	$limit=15;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;

			$data['kategori'] = $this->home_model->tampil_produk_per_page($offset,$limit)->result();
			$data['perbrand'] = $this->home_model->tampil_brand()->result();
			// $tot_hal = $this->home_model->hitung_isi_1tabel('tbl_produk','where id_kategori in ('.$ubh_kode.')');
			// $desk_kat = $this->home_model->hitung_isi_1tabel('tbl_kategori','where id_kategori='.$p_kode[0].'');

			$config['base_url'] = base_url() . 'kategori/get_produk/';
        	$config['total_rows'] = $this->home_model->hitung_data_produk();
        	$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['full_tag_open'] = '<div class="col-sm-6 text-left"><ul class="pagination">';
            $config['full_tag_close'] = '</ul></div>';	
	    	$config['first_link'] = '&laquo; Awal';
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir &raquo;';
	        $config['last_tag_open']  = '<li>';
	        $config['last_tag_close'] = '</li>';
			
			$config['next_link']     = 'Next &rarr;';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	      
	        $config['prev_link'] =      '&larr; Previous';
	        $config['prev_tag_open'] =  '<li>';
	        $config['prev_tag_close'] = '<li>';
	      
	        $config['cur_tag_open']   = '<li class="active"><a href="">';
	        $config['cur_tag_close']  = '</a></li>';
	        $config['num_tag_open']   = '<li>';
	        $config['num_tag_close']  = '</li>';
       		$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;	

			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "- Online Store Fashion Termurah Indonesia - ";

			$this->load->view('home/top_menu',$data);
			$this->load->view('kategori_view_main');
			$this->load->view('home/footer_menu');
	}

	function produk()
	{
		$kode='';		
		if ($this->uri->segment(3) === FALSE)
		{
    			$kode='';
		}
		else
		{
    			$kode = $this->uri->segment(3);
		}		

		$p_kode = explode("-",$kode);
		
		$ubh_kode = explode("-",$kode)[0]; 

		$page=$this->uri->segment(4);
      	$limit=15;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;

		$data['kategori'] = $this->home_model->tampil_produk_per_kategori($ubh_kode,$offset,$limit)->result();
		$tot_hal = $this->home_model->hitung_isi_1tabel('tbl_produk','where id_kategori in ('.$ubh_kode.')');
		$desk_kat = $this->home_model->hitung_isi_1tabel('tbl_kategori','where id_kategori='.$p_kode[0].'');
		$data['perbrand'] = $this->home_model->tampil_brand()->result();

		$config['base_url'] = base_url() . 'kategori/produk/'.$kode;
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $limit;
			$config['uri_segment'] = 4;
			$config['full_tag_open'] = '<div class="col-sm-6 text-left"><ul class="pagination">';
            $config['full_tag_close'] = '</ul></div>';	
	    	$config['first_link'] = '&laquo; Awal';
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir &raquo;';
	        $config['last_tag_open']  = '<li>';
	        $config['last_tag_close'] = '</li>';
			
			$config['next_link']     = 'Next &rarr;';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	      
	        $config['prev_link'] =      '&larr; Previous';
	        $config['prev_tag_open'] =  '<li>';
	        $config['prev_tag_close'] = '<li>';
	      
	        $config['cur_tag_open']   = '<li class="active"><a href="">';
	        $config['cur_tag_close']  = '</a></li>';
	        $config['num_tag_open']   = '<li>';
	        $config['num_tag_close']  = '</li>';
       		$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

			$judul = "";
			$data['link'] = "";

			foreach($desk_kat->result() as $dp)
			{
				$judul = 'Kategori '.$dp->nama_kategori;
				$data['link'] = $kode;
				$data['nama_kategori'] = $dp->nama_kategori;
			}

		$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
		$data['judul'] = $judul."- Online Store Fashion Termurah Indonesia";

		$this->load->view('home/top_menu',$data);
		$this->load->view('kategori_view');
		$this->load->view('home/footer_menu');
	}


	

}
