<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profileadm extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'profileadm';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "info";
					$isi['namamenu'] = "profileadm";
					$isi['page'] = "profileadm";
					$isi['link'] = 'profileadm';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Informasi Pribadi";
					$isi['judul'] = "Halaman Informasi Pribadi";
					$isi['content'] = "profileadm_view";
					$data = $this->db->get_where('tbl_spr_admn',array('kode_spr_admn'=>$this->session->userdata('kode')))->result();
					foreach ($data as $huin) {
						$isi['kode_spr_admn'] = $huin->kode_spr_admn;
						$isi['default']['nama'] = $huin->nama_admn;
						$isi['default']['hp'] = $huin->no_telp;
						$isi['default']['tgl_lahir'] = date('d-m-Y',strtotime($huin->tgl_lahir));
						$isi['default']['mail'] = $huin->email;
						$isi['foto'] = $huin->foto;
					}
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function change(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'profileadm';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "info";
					$isi['namamenu'] = "profileadm";
					$isi['page'] = "profileadm";
					$isi['link'] = 'profileadm';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Informasi Pribadi";
					$isi['judul'] = "Halaman Informasi Pribadi";
					$isi['content'] = "change_view";
					$data = $this->db->get_where('view_login_admin',array('kode'=>$this->session->userdata('kode')))->result();
					foreach ($data as $huin) {
						$isi['default']['kode'] = $huin->password;
						$isi['default']['nip'] = $huin->username;
					}
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function edit_data(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'profileadm';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$foto = str_replace(" ", "_", $_FILES['foto']['name']);
					$tmpName = $_FILES['foto']['tmp_name'];
					if($tmpName!=''){
					   	$config['file_name'] = $foto;
						$config['upload_path'] = 'assets-admin/foto/pegawai';
						$config['allowed_types'] = 'gif|jpg|png|bmp';
						$config['max_size'] = '14000';
						$config['max_width'] = '6000';
				        $config['max_height'] = '6000';
				        $config['overwrite'] = TRUE;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if ($this->upload->do_upload('foto')){
							$data = array('upload_data' => $this->upload->data());
							$simpan = array('nama_admn'=>$this->kacang->anti($this->input->post('nama')),
								'no_telp'=>$this->kacang->anti($this->input->post('hp')),
								'email'=>$this->kacang->anti($this->input->post('mail')),
								'tgl_lahir'=>$this->kacang->anti(date('Y-m-d',strtotime($this->input->post('tgl_lahir')))),
								'foto'=>$foto);
						}else{
						?>
						<script type="text/javascript">
			                alert("Pastikan Type File gif || jpg || bmp || png dan ukuran file maksimal 14000 kb");
			                window.location.href="<?php echo base_url();?>profileadm";
						</script>
						<?php
						}
					}else{
						$simpan = array('nama_admn'=>$this->kacang->anti($this->input->post('nama')),
							'no_telp'=>$this->kacang->anti($this->input->post('hp')),
							'email'=>$this->kacang->anti($this->input->post('mail')),
							'tgl_lahir'=>$this->kacang->anti(date('Y-m-d',strtotime($this->input->post('tgl_lahir')))));
					}
					$this->db->where("kode_spr_admn",$this->kacang->anti($this->session->userdata('kode')));
					$this->db->update('tbl_spr_admn',$simpan);
					redirect($_SERVER['HTTP_REFERER']);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function edit_pass(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'profileadm';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$simpan = array('username_admn'=>$this->kacang->anti($this->input->post('nip')),
							'text'=>$this->kacang->anti($this->input->post('password')),
							'pass_admn'=>$this->kacang->anti(md5($this->input->post('password'))));
					$this->db->where("kode_spr_admn",$this->kacang->anti($this->session->userdata('kode')));
					$this->db->update('tbl_spr_admn',$simpan);
					redirect('profileadm','refresh');
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
