<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-validation-2">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body panel-form">
                <form class="form-horizontal form-bordered" action="<?php echo base_url();?>profileadm/edit_data" method="post" enctype="multipart/form-data" data-parsley-validate="true">
                   
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Foto Profile :</label>
                        <div class="col-md-2 col-sm-2">
                        <?php
                            if($foto==""){
                                $fotox = "no.jpg";
                            }else{
                                $fotox = $foto;
                            }
                        ?>
                            <img src="<?php echo base_url();?>assets-admin/foto/pegawai/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Nama Pegawai * :</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control" type="text" id="nama" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" data-parsley-required="true"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Tanggal Lahir * :</label>
                        <div class="col-md-2 col-sm-2"> 
                            <div class="input-group date" id="datepicker-default" data-date-format="dd-mm-yyyy">
                                <input type="text" class="form-control" name="tgl_lahir" value="<?php echo set_value('tgl_lahir',isset($default['tgl_lahir']) ? $default['tgl_lahir'] : ''); ?>" data-type="tgl_lahir" data-parsley-required="true"/>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">No Hp * :</label>
                        <div class="col-md-2 col-sm-2">
                            <input class="form-control" style="text-align:right" type="text" id="hp" minlength="1" name="hp" value="<?php echo set_value('hp',isset($default['hp']) ? $default['hp'] : ''); ?>" data-type="hp" data-parsley-required="true" data-parsley-type="number" data-parsley-minlength="1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Email * :</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control" type="text" id="mail" name="mail" value="<?php echo set_value('mail',isset($default['mail']) ? $default['mail'] : ''); ?>" data-type="mail" data-parsley-required="true" data-parsley-type="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Foto Profile :</label>
                        <div class="col-md-3 col-sm-3">
                            <input name="MAX_FILE_SIZE" value="1024000" type="hidden">
                            <input type="file" id="foto" name="foto" />  
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3"></label>
                        <div class="col-md-3 col-sm-3">
                            <label><a href="<?php echo base_url();?>profileadm/change">Ganti Password ?</a></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-Kode Karyawan-3"></label>
                        <div class="col-md-3 col-sm-3">
                            <button type="submit" class="btn btn-success btn-sm">Tombol</button>
                            <button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
