<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model {
 public function __construct()
	{
	  parent::__construct();
	  // loader::database();    // Connect to current database setting.
	}


	function menu_kategori($kd_level,$kd_parent)
	{
		$q = $this->db->query("SELECT * from tbl_kategori where kode_level='$kd_level' and kode_parent='$kd_parent' and status ='1' order by tbl_kategori.nama_kategori asc");
		return $q;
	}

	function tampil_banner()
	{
		$q = $this->db->query("SELECT * from tbl_slideshow where stts='1' order by kode_banner DESC");
		return $q;
	}

	function tampil_slide_produk($limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori
		order by RAND() LIMIT $limit");
		return $q;
	}

	function tampil_produk($limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori
		order by kode_produk DESC LIMIT $limit");
		return $q;
	}

	function tampil_slide_produk_terlaris_kiri($limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori
		order by dibeli DESC LIMIT $limit");
		return $q;
	}

	function tampil_produk_pertipe($tipe,$limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk  where
		tbl_produk.tipe_produk = '$tipe' order by tbl_produk.id_kategori DESC LIMIT $limit");
		return $q;
	}

	function tampil_produk_per_page($offset,$limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori
		order by tbl_produk.id_kategori DESC LIMIT $offset, $limit");
		return $q;
	}

	function tampil_brand_content($kode)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori Where tbl_produk.brand = '$kode' order by tbl_produk.brand ASC");
		return $q;
	}

	function tampil_brand()
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori
							   group by tbl_produk.brand order by tbl_produk.brand ASC ");
		return $q;
	}

	function tampil_produk_per_kategori($kategori,$offset,$limit)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori where
		tbl_produk.id_kategori in ($kategori) order by tbl_produk.id_kategori DESC LIMIT $offset,$limit");
		return $q;
	}

	function hitung_data_produk(){
		return $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori")->num_rows();
	}

	function hitung_isi_1tabel($tabel,$seleksi)
	{
		$q = $this->db->query("SELECT * from $tabel $seleksi");
		return $q;
	}

	function tampil_detail_produk($kode)
	{
		$q = $this->db->query("SELECT * from tbl_produk left join tbl_kategori on tbl_produk.id_kategori=tbl_kategori.id_kategori where
		kode_produk='$kode'");
		return $q;
	}

	function cari_produk($kdproduk)
	{
		$result = $this->db->get_where('tbl_produk', array('kode_produk' => $kdproduk));
		return $result;
	}

	function pilih_member($kode)
	{
		$query=$this->db->query("select * from tbl_user where kode_user='$kode'");
		return $query;
	}

	function update_profil_member($upd)
	{
		$query=$this->db->query($upd);
	}


	function data_login_member($user,$pass)
	{
		$user_bersih=$this->kacang->anti(stripslashes(strip_tags(htmlspecialchars($user,ENT_QUOTES))));
		$pass_bersih=md5($this->kacang->anti(stripslashes(strip_tags(htmlspecialchars($pass,ENT_QUOTES)))));
		$query=$this->db->query("select * from tbl_user where username_user='$user_bersih' and pass_user='$pass_bersih' and stts=1");
		return $query;
	}

	function cek_email($email)
	{
		$query=$this->db->query("select * from tbl_user where email='$email'");
		return $query;
	}

	function tampil_semua_history($kd,$limit,$offset)
	{
		$query=$this->db->query("SELECT substring(a.kode_transaksi,1,8) as tgl, count(kode_produk) as jm, a.kode_transaksi FROM `tbl_transaksi_header` as a left join tbl_transaksi_detail as b on a.kode_transaksi=b.kode_transaksi where kode_user='$kd' group by tgl LIMIT $offset,$limit");
		return $query;
	}

	function tampil_det_history($kd_usr,$kd,$limit,$offset)
	{
		$query=$this->db->query("SELECT * FROM `tbl_transaksi_header` as a left join tbl_transaksi_detail as b on a.kode_transaksi=b.kode_transaksi where kode_user='$kd_usr' and b.kode_transaksi like '%$kd%' order by b.kode_transaksi ASC LIMIT $offset,$limit");
		return $query;
	}

	function simpan_pesanan($datainput)
	{
		$q = $this->db->query($datainput);
	}

	function update_dibeli($kd,$bl)
	{
		$query=$this->db->query("update tbl_produk set dibeli=dibeli+$bl where kode_produk='$kd'");
	}

	function cek_kode($tgl)
	{
		$query=$this->db->query("SELECT MAX(kode_transaksi) as kd FROM tbl_transaksi_header WHERE kode_transaksi like '%$tgl%'");
		return $query;
	}
}
