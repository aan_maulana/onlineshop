<footer id="footer">
    <div class="fpart-first">
      <div class="container">
        <div class="row">
          
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Information</h5>
            <ul>
              <li><a href="about-us.html">About Us</a></li>
              <li><a href="about-us.html">Delivery Information</a></li>
              <li><a href="about-us.html">Privacy Policy</a></li>
              <li><a href="about-us.html">Terms &amp; Conditions</a></li>
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Customer Service</h5>
            <ul>
              <li><a href="contact-us.html">Contact Us</a></li>
              <li><a href="#">Returns</a></li>
              <li><a href="sitemap.html">Site Map</a></li>
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Extras</h5>
            <ul>
              <li><a href="#">Brands</a></li>
              <li><a href="#">Gift Vouchers</a></li>
              <li><a href="#">Affiliates</a></li>
              <li><a href="#">Specials</a></li>
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Member Area</h5>
            <ul>
              <li><a href="<?php echo base_url();?>member">My Account</a></li>
              <li><a href="<?php echo base_url();?>member/history">History Transaksi</a></li>
              <!-- <li><a href="#">Wish List</a></li> -->
              <!-- <li><a href="#">Newsletter</a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="fpart-second">
      <div class="container">
        <div id="powered" class="clearfix">
          <!-- <div class="powered_text pull-left flip">
            <p>Marketshop Ecommerce Template © 2016 | Template By <a href="http://harnishdesign.net" target="_blank">Harnish Design</a></p>
          </div> -->
          <div class="social pull-right flip"> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/socialicons/facebook.png" alt="Facebook" title="Facebook"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/socialicons/twitter.png" alt="Twitter" title="Twitter"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/socialicons/google_plus.png" alt="Google+" title="Google+"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/socialicons/pinterest.png" alt="Pinterest" title="Pinterest"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/socialicons/rss.png" alt="RSS" title="RSS"> </a> </div>
        </div>
        <!-- <div class="bottom-row">
          <div class="custom-text text-center">
            <p>This is a CMS block. You can insert any content (HTML, Text, Images) Here.<br> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
          <div class="payments_types"> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_paypal.png" alt="paypal" title="PayPal"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_american.png" alt="american-express" title="American Express"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_2checkout.png" alt="2checkout" title="2checkout"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_maestro.png" alt="maestro" title="Maestro"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_discover.png" alt="discover" title="Discover"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo base_url()?>assets/image/payment/payment_mastercard.png" alt="mastercard" title="MasterCard"></a> </div>
        </div> -->
      </div>
    </div>
    <div id="back-top"><a data-toggle="tooltip" title="Back to Top" href="javascript:void(0)" class="backtotop"><i class="fa fa-chevron-up"></i></a></div>
  </footer>
   <!--Footer Start-->
  
  <!--Footer End-->
  <!-- Twitter Side Block Start -->
  <div id="twitter_footer" class="twit-right sort-order-1">
    <div class="twitter_icon"><i class="fa fa-twitter"></i></div>
    <a class="twitter-timeline" href="https://twitter.com/" data-chrome="nofooter noscrollbar transparent" data-theme="light" data-tweet-limit="2" data-related="twitterapi,twitter" data-aria-polite="assertive" data-widget-id="347621595801608192">Tweets by @</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  </div>
  <!-- Twitter Side Block End -->
  <!-- Facebook Side Block Start -->
  <div id="facebook" class="fb-right sort-order-2">
    <div class="facebook_icon"><i class="fa fa-facebook"></i></div>
    <div class="fb-page" data-href="https://www.facebook.com/harnishdesign/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true" data-show-posts="false">
      <div class="fb-xfbml-parse-ignore">
        <blockquote cite="https://www.facebook.com/harnishdesign/"><a href="https://www.facebook.com/harnishdesign/">Harnish Design</a></blockquote>
      </div>
    </div>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  </div>
  <!-- Facebook Side Block End -->
</div>
<!-- JS Part Start-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.dcjqaccordion.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/swipebox/lib/ios-orientationchange-fix.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/swipebox/src/js/jquery.swipebox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/app/detail-zoom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/rating/js/star-rating.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        $('.rating').on('rating.change', function (event, value, caption) {
            var rate_id = $(this).prop('id');
            var pure_id = rate_id.substring(6);
            var id = jQuery("#id_transaksi").val()
            $.post('<?= base_url()?>member/create_rate', {score: value, pid: pure_id,id_transaksi: id},
                function (data) {
                    $('#' + rate_id).rating('refresh', {
                        rtl:false,
                        showClear: false,
                        showCaption: false,
                        disabled: true
                    });
                });
            console.log(pure_id);
        });
    }); 
</script>
<!-- JS Part End-->

</body>
</html>