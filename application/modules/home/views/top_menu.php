<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="image/favicon.png" rel="icon" />
<title>LKLTSK Online Store Tasikmalaya</title>
<meta name="description" content="Responsive and clean html template design for any kind of ecommerce webshop">
<!-- CSS Part Start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/stylesheet-skin2.css" />
<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Droid+Sans' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/rating/css/star-rating.css" media="all"  type="text/css"/>

<!-- CSS Part End-->
</head>
<body>
<div class="wrapper-wide">
  <div id="header">
    <!-- Top Bar Start-->
    <nav id="top" class="htop">
      <div class="container">
        <div class="row"> <span class="drop-icon visible-sm visible-xs"><i class="fa fa-align-justify"></i></span>
          <div class="pull-left flip left-top">
            <div class="links">
              <ul>
                <li class="mobile"><i class="fa fa-phone"></i>+91 9898777656</li>
                <li class="email"><a href="mailto:info@marketshop.com"><i class="fa fa-envelope"></i>info@marketshop.com</a></li>
                <!-- <li class="wrap_custom_block hidden-sm hidden-xs"><a>Custom Block<b></b></a>
                  <div class="dropdown-menu custom_block">
                    <ul>
                      <li>
                        <table>
                          <tbody>
                            <tr>
                              <td><img alt="" src="<?php echo base_url() ?>assets/image/banner/cms-block.jpg"></td>
                              <td><img alt="" src="<?php echo base_url() ?>assets/image/banner/responsive.jpg"></td>
                            </tr>
                            <tr>
                              <td><h4>CMS Blocks</h4></td>
                              <td><h4>Responsive Template</h4></td>
                            </tr>
                            <tr>
                              <td>This is a CMS block. You can insert any content (HTML, Text, Images) Here.</td>
                              <td>This is a CMS block. You can insert any content (HTML, Text, Images) Here.</td>
                            </tr>
                            <tr>
                              <td><strong><a class="btn btn-default btn-sm" href="#">Read More</a></strong></td>
                              <td><strong><a class="btn btn-default btn-sm" href="#">Read More</a></strong></td>
                            </tr>
                          </tbody>
                        </table>
                      </li>
                    </ul>
                  </div>
                </li> -->
              </ul>
            </div>
            <!-- <div id="language" class="btn-group">
              <button class="btn-link dropdown-toggle" data-toggle="dropdown"> <span> <img src="<?php echo base_url()?>assets/image/flags/gb.png" alt="English" title="English">English <i class="fa fa-caret-down"></i></span></button>
              <ul class="dropdown-menu">
                <li>
                  <button class="btn btn-link btn-block language-select" type="button" name="GB"><img src="<?php echo base_url()?>assets/image/flags/gb.png" alt="English" title="English" /> English</button>
                </li>
                <li>
                  <button class="btn btn-link btn-block language-select" type="button" name="GB"><img src="<?php echo base_url()?>assets/image/flags/ar.png" alt="Arabic" title="Arabic" /> Arabic</button>
                </li>
              </ul>
            </div>
            <div id="currency" class="btn-group">
              <button class="btn-link dropdown-toggle" data-toggle="dropdown"> <span> $ USD <i class="fa fa-caret-down"></i></span></button>
              <ul class="dropdown-menu">
                <li>
                  <button class="currency-select btn btn-link btn-block" type="button" name="EUR">€ Euro</button>
                </li>
                <li>
                  <button class="currency-select btn btn-link btn-block" type="button" name="GBP">£ Pound Sterling</button>
                </li>
                <li>
                  <button class="currency-select btn btn-link btn-block" type="button" name="USD">$ US Dollar</button>
                </li>
              </ul>
            </div> -->
          </div>
          <div id="top-links" class="nav pull-right flip">
            <ul>
              <?php if(empty($_SESSION['username_online_shop'])){ ?>
                <li><a href="<?php echo base_url();?>member/login">Login</a></li>
                <li><a href="<?php echo base_url();?>member/daftar">Register</a></li>
              <?php
              }
              else{
              ?>
                <li><a href="<?php echo base_url();?>member">Member Area</a></li>
                <li><a href="<?php echo base_url();?>member/logout">Logout</a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- Top Bar End-->
    <!-- Header Start-->
    <header class="header-row">
      <div class="container">
        <div class="table-container">
          <!-- Logo Start -->
          <div class="col-table-cell col-lg-6 col-md-6 col-sm-12 col-xs-12 inner">
            <div id="logo"><a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url()?>assets/image/logo.png" title="MarketShop" alt="MarketShop" /></a></div>
          </div>
          <!-- Logo End -->
          <!-- Mini Cart Start-->


          <div class="col-table-cell col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div id="cart">
              <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle">
              <span class="cart-icon pull-left flip"></span>
              <?php $i = 1; ?>
              <?php foreach ($this->cart->contents() as $items): ?>
                  <?php echo form_hidden('rowid[]', $items['rowid']); ?>

              <?php endforeach; ?>
              <span id="cart-total"><?php echo count($this->cart->contents()); ?> item(s) - Rp.  <?php $harga_tot=number_format($this->cart->total(),2,",","."); echo $harga_tot; ?> </span></button>
              <ul class="dropdown-menu">
                <!-- <li>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="Xitefun Causal Wear Fancy Shoes" alt="Xitefun Causal Wear Fancy Shoes" src="<?php echo base_url()?>assets/image/product/sony_vaio_1-50x75.jpg"></a></td>
                        <td class="text-left"><a href="product.html">Xitefun Causal Wear Fancy Shoes</a></td>
                        <td class="text-right">x 1</td>
                        <td class="text-right">$902.00</td>
                        <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                      </tr>
                      <tr>
                        <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="Aspire Ultrabook Laptop" alt="Aspire Ultrabook Laptop" src="<?php echo base_url()?>assets/image/product/samsung_tab_1-50x75.jpg"></a></td>
                        <td class="text-left"><a href="product.html">Aspire Ultrabook Laptop</a></td>
                        <td class="text-right">x 1</td>
                        <td class="text-right">$230.00</td>
                        <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                      </tr>
                    </tbody>
                  </table>
                </li>
                <li> -->
                <li>
                  <table class="table">
                    <tbody>
                      <?php $i = 1; ?>
                      <?php foreach ($this->cart->contents() as $items): ?>
                        <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                        <tr>
                          <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="<?php echo $items['name']; ?>" alt="<?php echo $items['name']; ?>" src="<?php echo base_url()?>assets/image/product/samsung_tab_1-50x75.jpg"></a></td>
                          <td class="text-left"><a href="product.html"><?php echo $items['name']; ?></a></td>
                          <td class="text-right"><?php echo $items['qty']; ?></td>
                          <td class="text-right"><?php echo number_format($items['subtotal'],2,",",".");?></td>
                          <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="location.href='<?php echo base_url();?>keranjang/remove_cart/<?php echo $items['rowid']; ?>'" type="button"><i class="fa fa-times"></i></button></td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </li>
                <li>

                  <div>
                    <table class="table table-bordered">
                      <tbody>
                          <tr>
                          <td class="text-right"><strong>Total</strong></td>
                          <td class="text-right">Rp <?php $harga_tot=number_format($this->cart->total(),2,",","."); echo $harga_tot; ?></td>
                        </tr>
                    </table>
                    <p class="checkout"><a href="<?php echo base_url();?>keranjang" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> View Cart</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>checkout" class="btn btn-danger"><i class="fa fa-share"></i> Checkout</a></p>
                  </tbody>
                  </div>
                </li>
              </ul>

            </div>
          </div>


          <!-- Mini Cart End-->
          <!-- Search Start-->
          <div class="col-table-cell col-lg-3 col-md-3 col-sm-6 col-xs-12 inner">
            <div id="search" class="input-group">
              <input id="filter_name" type="text" name="search" value="" placeholder="Search" class="form-control input-lg" />
              <button type="button" class="button-search"><i class="fa fa-search"></i></button>
            </div>
          </div>
          <!-- Search End-->
        </div>
      </div>
    </header>
    <!-- Header End-->
    <!-- Main Menu Start-->

      <nav id="menu" class="navbar">
        <div class="navbar-header"> <span class="visible-xs visible-sm"> Menu <b></b></span></div>
        <div class="container">
        <div class="collapse navbar-collapse navbar-ex1-collapse">

          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="dropdown"><a href="<?php echo base_url();?>kategori">shop</a>
              <div class="dropdown-menu">
              <ul>
              <?php
                if(count($menu)>0){
                  foreach ($menu as $mn) {
                      $nm_link = $mn->id_kategori.'-'.$mn->nama_kategori;
                      $Id1     = strtolower(str_replace(" ", "-", $nm_link));
                      $sub     = $this->home_model->menu_kategori('1',$mn->id_kategori)->result();
                      ?>
                      <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $Id1;?>"><?php echo $mn->nama_kategori; ?></a>
                        <?php
                          foreach ($sub as $submenu) {
                            $nm_link2 =  $submenu->id_kategori.'-'.$submenu->nama_kategori;
                            $Id2      =  strtolower(str_replace(" ", "-", $nm_link2));
                            $sub2     =  $this->home_model->menu_kategori('2',$submenu->id_kategori)->result();
                        ?>

                        <?php if(count($sub)>0){?>
                          <div class="dropdown-menu">
                            <ul>
                              <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $Id2;?>"><?php echo $submenu->nama_kategori; ?>
                                    <?php
                                      if(count($sub2)>0){
                                        echo '<span>&rsaquo;</span>';
                                      }else{
                                        echo'';
                                      }
                                     ?>
                                  </a>
                                    <?php
                                      if(count($sub2)>0){
                                      ?>
                                       <div class="dropdown-menu">
                                        <ul>
                                         <?php
                                          foreach ($sub2 as $childsub) {
                                             $nm_link3 =  $submenu->id_kategori.'-'.$submenu->nama_kategori;
                                             $Id3      =  strtolower(str_replace(" ", "-", $nm_link3));
                                             ?>
                                                <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $Id3;?>"><?php echo $childsub->nama_kategori; ?></a></li>
                                             <?php
                                              if(count($sub2)>0){
                                                echo '<span>&rsaquo;</span>';
                                                }else{
                                                  echo'';
                                                }
                                           }
                                         ?>
                                        </ul>
                                      </div>
                                      <?php
                                      }
                                  ?>
                            </li>
                          <?php
                          }
                          ?>
                            </ul>
                          </div>
                        <?php } ?>
                    </li>
                    <?php
                  }
                }
              ?>
              </ul>
              </div>
            </li>
            <li><a href="<?php echo base_url();?>">About Us</a></li>
            <li><a href="<?php echo base_url();?>lookbook">Lookbook</a></li>
            <li><a href="<?php echo base_url();?>contactus">Contact Us</a></li>
            <!-- <li class="custom-link-right"><a href="#" target="_blank"> Buy Now!</a></li> -->
          </ul>
        </div>
        </div>
      </nav>

    <!-- Main Menu End-->
  </div>
