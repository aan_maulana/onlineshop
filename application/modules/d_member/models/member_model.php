<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    /*==================================== data_member ============================================*/


    function data_member($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                SELECT ts.kode_user AS kode_user,ts.nama AS nama,ts.email AS email,
                        ts.alamat AS alamat,ts.telpon AS telpon,ts.propinsi AS propinsi,ts.kota AS kota
                        ,ts.kode_pos AS kode_pos,ts.tgl_lahir AS tgl_lahir,ts.stts AS stts
                        FROM tbl_user ts
                        ORDER BY ts.nama ASC
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }

    function data_member_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT ts.kode_user AS kode_user,ts.nama AS nama,ts.email AS email,
                        ts.alamat AS alamat,ts.telpon AS telpon,ts.propinsi AS propinsi,ts.kota AS kota
                        ,ts.kode_pos AS kodepos,ts.tgl_lahir AS tgl_lahir,ts.stts AS stts
                        FROM tbl_user ts
                        ORDER BY ts.nama ASC
            ) A
        ");
        return $query;
    }   

}