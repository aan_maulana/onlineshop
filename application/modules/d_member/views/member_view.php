<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>

<script>
$(document).ready(function() {

    TableManageResponsive.init();
    var host = window.location.host;
    $BASE_URL = 'http://'+host+'/';  
    $('#data-Member').dataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp = $('td:eq(0)', nRow).text();
            var temp = temp.split('|');
            var no = temp[0]+".";
            var kode = temp[1];
            var nama = $('td:eq(1)', nRow).text();
            var email = $('td:eq(2)', nRow).text();
            var alamat = $('td:eq(3)', nRow).text();
            var telpon = $('td:eq(4)', nRow).text();
            var propinsi = $('td:eq(5)', nRow).text();
            var kota = $('td:eq(6)', nRow).text();
            var kode_pos = $('td:eq(7)', nRow).text();
            var tgl_lahir = $('td:eq(8)', nRow).text();
            var stts = $('td:eq(9)', nRow).text();
            // var fotox = '<a class="fancybox" href="<?php echo base_url();?>assets-admin/foto/pegawai/'+foto+'" style="width:80px;text-align:center;height:80px;" title="'+nama+'"><img src="<?php echo base_url();?>assets-admin/foto/pegawai/'+foto+'" style="width:71px;" alt=""></a>'
            var action = '<center><a href="javascript:void(0)" onclick="hapus('+"'"+kode+"'"+',\'Data Member\',\'d_member\',\'hapus\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Hapus Data"><i class="icon-remove icon-white"></i></a> '      +      ' <a href="javascript:void(0)" onclick="edit('+"'"+kode+"'"+',\'Data Member\',\'d_member\',\'edit\')" data-toggle="tooltip" class="btn btn-warning btn-sm" title="Edit Data"><i class="icon-pencil icon-white"></i></a></center>';
            if(stts=="1"){
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\','+"'"+kode+"'"+',\'Data Member\',\'d_member\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-info btn-sm" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>';
            }else{
                var status = '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\','+"'"+kode+"'"+',\'Data Member\',\'d_member\',\'ubah_status\')" data-toggle="tooltip" class="btn btn-danger btn-sm" title="Status NonAktif"><i class="fa fa-lock icon-white"></i></a>';
            }
            $('td:eq(0)', nRow).html(no);
            $('td:eq(1)', nRow).html(kode);
            $('td:eq(2)', nRow).html(nama);
            $('td:eq(3)', nRow).html(email);
            $('td:eq(4)', nRow).html(tgl_lahir);
            $('td:eq(5)', nRow).html(telpon);
            $('td:eq(6)', nRow).html(propinsi);
            $('td:eq(7)', nRow).html(kota);
            $('td:eq(8)', nRow).html(kode_pos);
            $('td:eq(9)', nRow).html(status);
            $('td:eq(10)', nRow).html(action);
            $('td:eq(0),td:eq(1),td:eq(3),td:eq(4),td:eq(8)', nRow).css('text-align','center');
        },
        "bAutoWidth": false,
        "aoColumns": [
            { "sWidth": "1%" },
            { "sWidth": "5%" },
            { "sWidth": "20%" },
            { "sWidth": "10%" },
            { "sWidth": "7%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "5%" },
            { "sWidth": "5%" },
            { "sWidth": "4%" }
        ],
        
        "bProcessing": false,
        "bServerSide": true,
        "responsive":false,
        "sAjaxSource": $BASE_URL+"d_member/get_data"
    });
    $('#data-nasabah').each(function(){
        var datatable = $(this);
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    });
});
</script>    
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="<?php echo base_url();?><?php echo $link;?>/add" title="Tambah <?php echo $halaman;?>" class="btn btn-primary btn-xs m-r-5"><i class="icon-plus-sign"></i> Tambah Data</a>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table id="data-Member" class="table table-striped table-bordered nowrap" width="100%">
                        <thead> 
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">kode</th>
                                <th style="text-align:center" width="10%">Nama Konsumen</th>
                                <th style="text-align:center" width="10%">Email Konsumen</th>
                                <th style="text-align:center" width="10%">Tanggal lahir</th>
                                <th style="text-align:center" width="20%">Telpon</th>
                                <th style="text-align:center" width="20%">Propinsi</th>
                                <th style="text-align:center" width="10%">Kota</th>
                                <th style="text-align:center" width="10%">Kode Pos</th>
                                <th style="text-align:center" width="9%">Status</th>
                                <th style="text-align:center" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>