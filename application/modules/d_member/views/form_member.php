<link href="<?php echo base_url();?>assets-admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets-admin/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/duit.js"></script>

<script src="<?php echo base_url();?>assets-admin/tinymce/js/tinymce/tinymce.dev.js"></script>
<script src="<?php echo base_url();?>assets-admin/tinymce/js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="<?php echo base_url();?>assets-admin/tinymce/js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="<?php echo base_url();?>assets-admin/tinymce/js/tinymce/plugins/wordcount/plugin.js"></script>
<script src="<?php echo base_url();?>assets-admin/tinymce/js/tinymce/plugins/spellchecker/plugin.dev.js"></script>

<div class="row">
    <div class="col-md-12">
    	

		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    <div class="tab-content">
			    <div class="panel-body panel-form">
			        <form class="form-horizontal form-bordered" role="form" class="form-inline" action="<?php echo base_url();?>d_member/<?php echo $action;?>" method="post" data-parsley-validate="true" enctype="multipart/form-data" >
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Username * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="username" minlength="2" name="username" value="<?php echo set_value('username',isset($default['username']) ? $default['username'] : ''); ?>" data-type="username" placeholder="Masukan username member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="control-label col-md-3 col-sm-3">password * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="password"  name="password" value="<?php echo set_value('password',isset($default['password']) ? $default['password'] : ''); ?>" data-type="password" placeholder="Masukan password member"> Jika tidak diganti, dikosongkan saja.
							</div>
						</div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Nama member * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="nama" minlength="2" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" placeholder="Masukan Nama member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Email * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="email" minlength="2" name="email" value="<?php echo set_value('email',isset($default['email']) ? $default['email'] : ''); ?>" data-type="email" placeholder="Masukan email member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">telpon * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="telpon" minlength="2" name="telpon" value="<?php echo set_value('telpon',isset($default['telpon']) ? $default['telpon'] : ''); ?>" data-type="telpon" placeholder="Masukan telpon member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">                                        
							<label class="control-label col-md-3 col-sm-3">Tanggal Lahir * :<br/><small><b>* Format dd-mm-yyyy</b></small></label>                                        
							<div class="col-md-3 col-sm-3">                                             
								<div class="input-group date" id="datepicker-default" data-date-format="dd-mm-yyyy">                                                
									<input type="text" class="form-control" data-parsley-group="wizard-step-2" name="tgl_lahir" value="<?php echo set_value('tgl_lahir',isset($default['tgl_lahir']) ? $default['tgl_lahir'] : ''); ?>" data-type="tgl_lahir" data-parsley-required="true"/><span class="input-group-addon"><i class="fa fa-calendar"></i></span>                                            
								</div>                                        
							</div>                                    
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">propinsi * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="propinsi" minlength="2" name="propinsi" value="<?php echo set_value('propinsi',isset($default['propinsi']) ? $default['propinsi'] : ''); ?>" data-type="propinsi" placeholder="Masukan propinsi member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">kota * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="kota" minlength="2" name="kota" value="<?php echo set_value('kota',isset($default['kota']) ? $default['kota'] : ''); ?>" data-type="kota" placeholder="Masukan kota member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">kode_pos * :</label>
							<div class="col-md-3 col-sm-3">
								<input class="form-control" type="text" id="kode_pos" minlength="2" name="kode_pos" value="<?php echo set_value('kode_pos',isset($default['kode_pos']) ? $default['kode_pos'] : ''); ?>" data-type="kode_pos" placeholder="Masukan kode_pos member" data-parsley-required="true" data-parsley-minlength="2"/>
							</div>
						</div>
						<div class="form-group">                                        
							<label class="control-label col-md-3 col-sm-3">alamat * :</label>                                        
							<div class="col-md-5 col-sm-5">                                           
								 <textarea class="form-control" data-parsley-group="wizard-step-2"  id="alamat" name="alamat" rows="3"><?php echo set_value('alamat',isset($default['alamat']) ? $default['alamat'] : ''); ?></textarea>
							</div>                                    
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3"></label>
							<div class="col-md-3 col-sm-3">
								<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
	                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
							</div>
						</div>
			        </form>
			    </div>
		    </div>
		</div>
	</div>
</div>

<script>
	tinymce.init({
		selector: "textarea#alamat",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker toc",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor importcss colorpicker textpattern codesample"
		],
		external_plugins: {
			//"moxiemanager": "/moxiemanager-php/plugin.js"
		},
		content_css: "css/development.css",
		add_unload_trigger: false,

		toolbar: "insertfile undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table codesample",

		image_advtab: true,
		image_caption: true,

		style_formats: [
			{title: 'Bold text', format: 'h1'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		template_replace_values : {
			username : "Jack Black"
		},

		template_preview_replace_values : {
			username : "Preview user name"
		},

		link_class_list: [
			{title: 'Example 1', value: 'example1'},
			{title: 'Example 2', value: 'example2'}
		],

		image_class_list: [
			{title: 'Example 1', value: 'example1'},
			{title: 'Example 2', value: 'example2'}
		],

		templates: [
			{title: 'Some title 1', description: 'Some desc 1', content: '<strong class="red">My content: {$username}</strong>'},
			{title: 'Some title 2', description: 'Some desc 2', url: 'development.html'}
		],

		setup: function(ed) {
			/*ed.on(
				'Init PreInit PostRender PreProcess PostProcess BeforeExecCommand ExecCommand Activate Deactivate ' +
				'NodeChange SetAttrib Load Save BeforeSetContent SetContent BeforeGetContent GetContent Remove Show Hide' +
				'Change Undo Redo AddUndo BeforeAddUndo', function(e) {
				console.log(e.type, e);
			});*/
		},

		spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}
	});

	if (!window.console) {
		window.console = {
			log: function() {
				tinymce.$('<div></div>').text(tinymce.grep(arguments).join(' ')).appendTo(document.body);
			}
		};
	}
</script>
<script type="text/javascript">
	 jQuery(document).ready(function(){
	    jQuery('#harga').priceFormat({
	        prefix: '',
	        centsSeparator: ',',
	        thousandsSeparator: ','
	    });
	});	
	$("#kode_pos").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#telpon").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });  

 	  
</script>