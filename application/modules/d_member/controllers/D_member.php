<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class D_member extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('d_member/member_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'admember';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Data Member';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "d_member";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "admember";
					$isi['namamenu'] = "Data Member";
					$isi['page'] = "d_member";
					$isi['link'] = 'd_member';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Data Member";
					$isi['judul'] = "Halaman Data Member";
					$isi['content'] = "member_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}

	public function get_data(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$aColumns = array('kode_user','nama','email','alamat','telpon','propinsi','kota','kode_pos','tgl_lahir','stts','kode_user');
	        $sIndexColumn = "kode_user";
	        // pagings
	        $sLimit = "";
	        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
	            $sLimit = "LIMIT ".$this->kacang->anti( $_GET['iDisplayStart'] ).", ".
	                $this->kacang->anti( $_GET['iDisplayLength'] );
	        }
	        $numbering = $this->kacang->anti( $_GET['iDisplayStart'] );
	        $page = 1;
	        // ordering
	        if ( isset( $_GET['iSortCol_0'] ) ){
	            $sOrder = "ORDER BY  ";
	            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
	                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
	                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
	                        ".$this->kacang->anti( $_GET['sSortDir_'.$i] ) .", ";
	                }
	            }            
	            $sOrder = substr_replace( $sOrder, "", -2 );
	            if ( $sOrder == "ORDER BY" ){
	                $sOrder = "";
	            }
	        }
	        // filtering
	        $sWhere = "";
	        if ( $_GET['sSearch'] != "" ){
	            $sWhere = "WHERE (";
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti( $_GET['sSearch'] )."%' OR ";
	            }
	            $sWhere = substr_replace( $sWhere, "", -3 );
	            $sWhere .= ')';
	        }
	        // individual column filtering
	        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
	                if ( $sWhere == "" ){
	                    $sWhere = "WHERE ";
	                }
	                else{
	                    $sWhere .= " AND ";
	                }
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti($_GET['sSearch_'.$i])."%' ";
	            }
	        }
	        $rResult = $this->member_model->data_member($aColumns, $sWhere, $sOrder, $sLimit);
	        $iFilteredTotal = 10;
	        $rResultTotal = $this->member_model->data_member_total($sIndexColumn);
	        $iTotal = $rResultTotal->num_rows();
	        $iFilteredTotal = $iTotal;
	        $output = array(
	            "sEcho" => intval($_GET['sEcho']),
	            "iTotalRecords" => $iTotal,
	            "iTotalDisplayRecords" => $iFilteredTotal,
	            "aaData" => array()
	        );
	        foreach ($rResult->result_array() as $aRow){
	            $row = array();
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                /* General output */
	                if($i < 1)
	                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
	                else
	                    $row[] = $aRow[ $aColumns[$i] ];
	            }
	            $page++;
	            $output['aaData'][] = $row;
	        }
	        echo json_encode( $output );
	    }    
	}
	
	public function add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_member';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['cek']	 = "add";
					$isi['kelas'] = "d_member";
					$isi['namamenu'] = "Tambah Data Member";
					$isi['page'] = "d_member";
					$isi['link'] = 'd_member';
					$isi['actionhapus'] = 'hapus';
					$isi['action'] = "proses_add";
					$isi['tombolsimpan'] = 'Simpan';
					$isi['tombolbatal'] = 'Batal';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Tambah Data Member";
					$isi['judul'] = "Halaman Data Member";
					$isi['content'] = "d_member/form_member";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_member';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('username','username','required|min_length[2]');
		 			$this->form_validation->set_rules('password','password','required|min_length[2]');
		 			$this->form_validation->set_rules('nama','nama member','required|min_length[2]');
		 			$this->form_validation->set_rules('email','email','required');
		 			$this->form_validation->set_rules('telpon','telpon','required');
		 			$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required|min_length[2]');
		 			$this->form_validation->set_rules('propinsi','propinsi','required');
		 			$this->form_validation->set_rules('kota','kota','required');
		 			$this->form_validation->set_rules('kode_pos','Kode Pos','required');
		 			$this->form_validation->set_rules('alamat','alamat','required');
					if ($this->form_validation->run() == TRUE){
						$username 	= $this->kacang->anti($this->input->post('username'));
						$password 	= $this->kacang->anti($this->input->post('password'));
						$nama 		= $this->kacang->anti($this->input->post('nama'));
						$email 		= $this->kacang->anti($this->input->post('email'));
						$telpon 	= $this->kacang->anti($this->input->post('telpon'));
						$tgl_lahir  = $this->kacang->anti(date('Y-m-d',strtotime($this->input->post('tgl_lahir'))));
						$propinsi 	= $this->kacang->anti($this->input->post('propinsi'));					
						$kota 		= $this->kacang->anti($this->input->post('kota'));
						$kode_pos 	= $this->kacang->anti($this->input->post('kode_pos'));
						$alamat 	= $this->kacang->anti($this->input->post('alamat'));
													
						$cekcode = $this->db->get_where('tbl_user',array('username_user'=>$username))->result();
						if(count($cekcode)>0){
							?> 
								<script type="text/javascript">
					 				alert("Data Tidak Bisa disimpan Username telah dipakai Sebelumnya, silahkan Isi dengan username lain !");
					 				window.location.href = "<?php echo base_url();?>d_member/add";
				 				</script>
							<?php
						}else{
							
									$simpan = array('username_user'=>$username,'pass_user'=>$password,'nama'=>$nama,'email'=>$email,'telpon'=>$telpon,
													'tgl_lahir'=>$tgl_lahir,'propinsi'=>$propinsi,'kota'=>$kota,'kode_pos'=>$kode_pos,'stts'=>1,'alamat'=>$alamat
										  );                 
							$this->db->insert('tbl_user',$simpan);
							redirect('d_member','refresh');
						}
	                }else{
                        $this->add();
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_member';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					if($jns=="aktif"){
						$data = array('stts'=>'0');
					}else{
						$data = array('stts'=>'1');
					}
					$this->db->where('kode_user',$this->kacang->anti($id));	
					$this->db->update('tbl_user',$data);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function hapus($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_member';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_transaksi_header',array('kode_user'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "NotOk";
						}else{
							$this->db->where('kode_user',$this->kacang->anti($kode));
							if($this->db->delete('tbl_user')){	
								$data['say'] = "ok";
							}else{
								$data['say'] = "NotOk";
							}
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

	function hapusfoto($kode=null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$ckdata = $this->db->get_where('tbl_memberuk',array('kode_memberuk'=>$kode))->result();
			foreach ($ckdata as $hehe) {
				$fotona = $hehe->gbr_kecil;
			}
			if($fotona!=""){
				if($fotona!="no.jpg"){
					unlink('assets/memberuk/detail/' . $fotona);
				}
			}
		}	
	}

	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'd_member';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_user',array('kode_user'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "ok";
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}
	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_member';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_user',array('kode_user'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
						}
						$isi['default']['kode_user'] 	= $key->kode_user;
						$isi['default']['username'] 	= $key->username_user;
						$isi['default']['nama'] 		= $key->nama;
						$isi['default']['email'] 		= $key->email;
						$isi['default']['alamat'] 		= $key->alamat;
						$isi['default']['telpon'] 		= $key->telpon;
						$isi['default']['propinsi'] 	= $key->propinsi;
						$isi['default']['kota'] 		= $key->kota;
						$isi['default']['kode_pos'] 	= $key->kode_pos;
						$isi['default']['tgl_lahir'] 	= $key->tgl_lahir;
						$isi['default']['stts'] 		= $key->stts;
						$isi['default']['tgl_lahir'] = date('d-m-Y',strtotime($key->tgl_lahir));
						$this->session->set_userdata('seskode_memberform',$kode);
						$isi['cek']	 = "edit";
						$isi['kelas'] = "d_member";
						$isi['namamenu'] = "Edit Data Member";
						$isi['page'] = "d_member";
						$isi['link'] = 'd_member';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Data Member";
						$isi['judul'] = "Halaman Data Member";
						$isi['content'] = "d_member/form_member";
						
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'd_member';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('username','username','required|min_length[2]');
		 			$this->form_validation->set_rules('nama','nama member','required|min_length[2]');
		 			$this->form_validation->set_rules('email','email','required');
		 			$this->form_validation->set_rules('telpon','telpon','required');
		 			$this->form_validation->set_rules('propinsi','propinsi','required');
		 			$this->form_validation->set_rules('kota','kota','required');
		 			$this->form_validation->set_rules('kode_pos','Kode Pos','required');
					if ($this->form_validation->run() == TRUE){	
						$username 	= $this->kacang->anti($this->input->post('username'));
						$password 	= $this->kacang->anti($this->input->post('password'));
						$nama 		= $this->kacang->anti($this->input->post('nama'));
						$email 		= $this->kacang->anti($this->input->post('email'));
						$telpon 	= $this->kacang->anti($this->input->post('telpon'));
						$propinsi 	= $this->kacang->anti($this->input->post('propinsi'));					
						$kota 		= $this->kacang->anti($this->input->post('kota'));
						$kode_pos 	= $this->kacang->anti($this->input->post('kode_pos'));
						$alamat 	= $this->kacang->anti($this->input->post('alamat'));



						if(empty($password)){
								$edit = array('username_user'=>$username,'nama'=>$nama,'email'=>$email,'alamat'=>$alamat,
										  'telpon'=>$telpon,'propinsi'=>$propinsi,'kota'=>$kota,'kode_pos'=>$kode_pos,'alamat'=>$alamat
										  );
						}else{
							$edit = array('username_user'=>$username,'pass_user'=>$password,'nama'=>$nama,'email'=>$email,'alamat'=>$alamat,
										  'telpon'=>$telpon,'propinsi'=>$propinsi,'kota'=>$kota,'kode_pos'=>$kode_pos,'alamat'=>$alamat
										  );
						}
									
								
							$this->db->where('kode_user',$this->kacang->anti($this->session->userdata('seskode_memberform')));
							$this->db->update('tbl_user',$edit);
							$this->session->unset_userdata('seskode_memberform');
							redirect('d_member','refresh');
	                }else{
						redirect(base_url().'d_member/edit/'.$this->session->userdata('seskode_memberform'),'refresh');
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
