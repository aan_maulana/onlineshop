<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('captcha','date','text_helper','form'));
		$this->load->library('email');
		 if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    }
	}

	function index()
	{

		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session!="")
		{
			$ubh_kode = '';
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			// $data['kategori'] = $this->home_model->tampil_produk_per_kategori($ubh_kode,$offset,$limit)->result();
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "- Online Store Fashion Termurah Indonesia";
			$data['perbrand'] = $this->home_model->tampil_brand()->result();
			$data['det_member'] = $this->home_model->pilih_member($pecah[2])->result();

			$this->load->view('home/top_menu',$data);
			$this->load->view('checkout_view');
			$this->load->view('home/footer_menu');
		}else{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/login'>";
		}
	}

	function update_keranjang()
	{
		$total = $this->cart->total_items();
		$item = $this->input->post('rowid');
		$qty = $this->input->post('quantity');
		for($i=0;$i < count($item); $i++)
		{
			$data = array(
			'rowid' => $item[$i],
			'qty'   => $qty[$i]);
			$this->cart->update($data);
		}

		echo "<meta http-equiv='refresh' content='0; url=".base_url()."checkout/'>";
	}

	function remove_cart($kode)
	{
		$id='';
		if ($this->uri->segment(3) === FALSE)
		{
    			$id='';
		}
		else
		{
    			$id = $this->uri->segment(3);
		}
		$data = array(
			'rowid' => $kode,
			'qty'   => 0);
			$this->cart->update($data);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."checkout/'>";
	}

	function tambah_barang()
	{	
		$kdproduk = $this->input->post('kdproduk');
		$qty = $this->input->post('quantity');
		$price = $this->input->post('hargana');
		$nmproduk = $this->input->post('nmproduk');

		$product = $this->home_model->cari_produk($kdproduk);		
		if ($product->num_rows() > 0){

			$row = $product->row();

			$harga 			= $row->harga;
			$nama_produk 	= $row->nama_produk;
			$image 			= $row->gbr_kecil; 
			
			$data = array(
			'id'      	=> $kdproduk,
			'qty'     	=> $qty,
		    'price'   	=> $harga,
			'name'    	=> $nama_produk,
			'options'   => array('gbkecil'=>$image)
			);

			$this->cart->insert($data);	
		}
		
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."checkout/'>";
	}

	function kirim_invoice(){
		$session=isset($_SESSION['username_online_shop']) ? $_SESSION['username_online_shop']:'';
		if($session==""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."member'>";
		}else{
			$ubh_kode = '';
			$pecah=explode("|",$session);
			$data["username"]=$pecah[0];
			$data["nama"]=$pecah[1];
			$data["kd_user"]=$pecah[2];
			$data["email"]=$pecah[3];
			$data['menu'] = $this->home_model->menu_kategori('0','0')->result();
			$data['judul'] = "Kirim Pesanan - LKLTASIK online store, Toko Clothing Online Termurah dan Terlengkap di Indonesia - LKL TASIK";			
			$data['perbrand'] = $this->home_model->tampil_brand()->result();
			$data['det_member'] = $this->home_model->pilih_member($pecah[2])->result();
			$tgl_skr = date('Ymd');
			$cek_kode = $this->home_model->cek_kode($tgl_skr);
			$kode_trans = "";
			$kode_trans = "";
			foreach($cek_kode->result() as $ck)
			{
				if($ck->kd==NULL)
				{
					$kode_trans = $tgl_skr.'001';
				}
				else
				{
					$kd_lama = $ck->kd;
					$kode_trans = $kd_lama+1;
				}
			}

			$datapesan['kode_user'] 		= $pecah[2];
			$datapesan['nama_penerima'] 	= $this->kacang->anti($this->input->post('namapen'));
			$datapesan['email_penerima'] 	= $this->kacang->anti($this->input->post('emailpen'));
			$datapesan['alamat_penerima'] 	= $this->kacang->anti($this->input->post('alamatpen'));
			$datapesan['propinsi_penerima'] = $this->kacang->anti($this->input->post('propinsipen'));
			$datapesan['telpon_penerima'] 	= $this->kacang->anti($this->input->post('telponpen'));
			$datapesan['kota_penerima'] 	= $this->kacang->anti($this->input->post('kotapen'));
			$datapesan['kodepos_penerima'] 	= $this->kacang->anti($this->input->post('kodepospen'));
			
			$datapesan['nama_pembeli'] 		= $this->kacang->anti($this->input->post('namapem'));
			$datapesan['email_pembeli'] 	= $this->kacang->anti($this->input->post('emailpem'));
			$datapesan['alamat_pembeli'] 	= $this->kacang->anti($this->input->post('alamatpem'));
			$datapesan['propinsi_pembeli'] 	= $this->kacang->anti($this->input->post('propinsipem'));
			$datapesan['telpon_pembeli'] 	= $this->kacang->anti($this->input->post('telponpem'));
			$datapesan['kota_pembeli'] 		= $this->kacang->anti($this->input->post('kotapem'));
			$datapesan['kodepos_pembeli'] 	= $this->kacang->anti($this->input->post('kodepospem'));

			$datapesan['metode'] 			= $this->kacang->anti($this->input->post('metode'));
			$datapesan['paket'] 			= $this->kacang->anti($this->input->post('paket'));
			$datapesan['bank'] 				= $this->kacang->anti($this->input->post('bank'));
			$datapesan['pesan'] 			= $this->kacang->anti($this->input->post('pesan'));

			if ($datapesan['pesan']=="") {
				$datapesan['pesan'] = "-";
			}else{
				$datapesan['pesan'] 			= $this->kacang->anti($this->input->post('pesan'));				
			}  	

			$data['pesan'] = "";
			if($datapesan['nama_penerima']=="" || $datapesan['email_penerima']=="" || $datapesan['alamat_penerima']=="" || $datapesan['propinsi_penerima']=="" || $datapesan['kota_penerima']=="" || $datapesan['kodepos_penerima']=="" || $datapesan['telpon_penerima']=="" )
			{
				$data['pesan'] = 'Field belum lengkap. Mohon diisi dengan selengkap-lengkapnya Silahkan kembali ke menu <a href="'.base_url().'checkout">checkout</a>';  
				$this->load->view('home/top_menu',$data);
				$this->load->view('hasil_checkout',$data);
				$this->load->view('home/footer_menu');
 			}else{
 				$isi_psn = 'Terima kasih telah berbelanja di Harmonis Grosir Sandal Online. Berikut detail produk yang anda beli :<br><br>	
							Detail Data Pembeli<br>
							Kode Transaksi : '.$kode_trans.'<br>
							Nama Pembeli : '.$datapesan['nama_pembeli'].'<br>
							Email Pembeli : '.$datapesan['email_pembeli'].'<br>
							Alamat Pembeli : '.$datapesan['alamat_pembeli'].'<br>
							No. Telpon Pembeli : '.$datapesan['telpon_pembeli'].'<br>
							Propinsi Pembeli : '.$datapesan['propinsi_pembeli'].'<br>
							Kota Pembeli : '.$datapesan['kota_pembeli'].'<br>
							Kode Pos Pembeli : '.$datapesan['kodepos_pembeli'].'<br><br>
												
												
							Detail Data Penerima<br>
							Kode Transaksi : '.$kode_trans.'<br>
							Nama Penerima : '.$datapesan['nama_penerima'].'<br>
							Email Penerima : '.$datapesan['email_penerima'].'<br>
							Alamat Penerima : '.$datapesan['alamat_penerima'].'<br>
							No. Telpon Penerima : '.$datapesan['telpon_penerima'].'<br>
							Propinsi Penerima : '.$datapesan['propinsi_penerima'].'<br>
							Kota Penerima : '.$datapesan['kota_penerima'].'<br>
							Kode Pos Penerima : '.$datapesan['kodepos_penerima'].'<br><br>
												
												
							Bank Tujuan : '.$datapesan['bank'].'<br>
							Metode Pembayaran : '.$datapesan['metode'].'<br>
							Paket Pengiriman : '.$datapesan['paket'].'<br>
							Pesan : '.$datapesan['pesan'].'<br><br>
							';

				$isi_psn .='<table style="border:1px solid #000;" border="1" cellpadding=0>';
				$isi_psn .='<tr><td>Kode Produk</td><td>Nama Produk</td><td>Harga</td><td>Jumlah</td><td>Subtotal</td></tr>';
							foreach($this->cart->contents() as $items)	{
								$isi_psn .= '<tr><td>'.$items["id"].'</td><td>'.$items["name"].'</td><td>Rp.'.$this->cart->format_number($items["price"]).'</td><td>'.$items["qty"].'</td><td>Rp.'.$this->cart->format_number($items["subtotal"]).'</td></tr>
											';
							}
				$isi_psn .= '<tr><td>Total Belanja (belum biaya kirim): </td><td colspan=4>Rp.'.$this->cart->format_number($this->cart->total()).'</td></tr>';
				$isi_psn .='</table><br>';
				$isi_psn .='Harga di atas belum termasuk biaya kirim. Kami akan mengirimkan total yang harus anda bayar ke email anda dalam jangka waktu 1x24 jam.<br>';
				$isi_psn .='Salam, Harmonis Grosir Sandal';
					
					$this->email->set_mailtype('html');
					$this->email->from("aan.h.maulana@gmail.com", "Admin Online Store LKLTASIK");
					$this->email->to($data["email"]);
					$this->email->subject('Detail Pesanan/Belanja - Member Online Store LKLTASIK');
					$this->email->message($isi_psn);	
					//$hsl = $this->email->send();
					
					$this->email->clear();
					
					$this->email->from($data["email"],$data["nama"]);
					$this->email->to("aan.h.maulana@gmail.com");
					$this->email->subject('Detail Pesanan/Belanja - Member Admin Online Store LKLTASIK');
					$this->email->message($isi_psn);	
					//$hsl2 = $this->email->send();
					$hsl2 = true;

					if($hsl2==TRUE)
					{
						$q = "insert into tbl_transaksi_header(kode_transaksi,kode_user,nama_penerima,email_penerima,alamat_penerima,propinsi,kota,kodepos,telpon,metode,paket_kirim,bank,pesan) values('".$kode_trans."','".$data["kd_user"]."','".$datapesan['nama_penerima']."','".$datapesan['email_penerima']."','".$datapesan['alamat_penerima']."','".$datapesan['propinsi_penerima']."','".$datapesan['kota_penerima']."','".$datapesan['kodepos_penerima']."','".$datapesan['telpon_penerima']."','".$datapesan['metode']."','".$datapesan['paket']."','".$datapesan['bank']."','".$datapesan['pesan']."')";
						$this->home_model->simpan_pesanan($q);
						foreach($this->cart->contents() as $items)
						{
							$this->home_model->simpan_pesanan("insert into tbl_transaksi_detail (kode_transaksi,kode_produk,nama_produk,harga,jumlah) values('".$kode_trans."','".$items['id']."','".$items['name']."','".$items['price']."','".$items['qty']."')");
							$this->home_model->update_dibeli($items['id'],$items['qty']);
						}
						$this->cart->destroy();
						?>
						<script type="text/javascript">
						alert("Pesanan anda telah terkirim, kami akan segera memprosesnya dalam waktu 1x24 jam. Silahkan cek email anda beberapa saat lagi untuk melihat rincian detail pembayaran.\n Terima Kasih");			
						</script>
						<?php
						echo "<meta http-equiv='refresh' content='0; url=".base_url()."member/'>";
					}else{
						$data['pesan'] = "Gagal mengirim detail pesanan/belanja.";
						$this->load->view('home/top_menu',$data);
						$this->load->view('hasil_checkout',$data);
						$this->load->view('home/footer_menu');
					}						
 			}


		}


	}
}
