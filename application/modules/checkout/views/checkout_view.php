<script type="text/javascript">
function setPaymentInfo(isChecked)
{
  with (window.document.frmCheckout) {
    if (isChecked) {
      namapen.value = namapem.value;
      emailpen.value = emailpem.value;
      alamatpen.value = alamatpem.value;
      telponpen.value = telponpem.value;
      propinsipen.value = propinsipem.value;
      kotapen.value = kotapem.value;      
      kodepospen.value = kodepospem.value;
      
      namapem.readOnly  = true;
      emailpem.readOnly  = true;
      alamatpem.readOnly  = true;
      telponpem.readOnly  = true;
      propinsipem.readOnly  = true;
      kotapem.readOnly  = true; 
      kodepospem.readOnly  = true;  
    } else {
      namapem.readOnly  = false;
      emailpem.readOnly  = false;
      alamatpem.readOnly  = false;
      telponpem.readOnly  = false;
      propinsipem.readOnly  = false;
      kotapem.readOnly  = false; 
      kodepospem.readOnly  = false;

      namapen.value = "";
      emailpen.value = "";
      alamatpen.value = "";
      telponpen.value = "";
      propinsipen.value = "";
      kotapen.value = ""; 
      kodepospen.value = "";  
    }
  }
}
</script>
<div id="container">
  <div class="container">
    <!-- Breadcrumb Start-->
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
      <li><a href="<?php echo base_url();?>kategori">Kategori</a></li>
      <li><a href="<?php echo base_url();?>kategori/produk">Produk</a></li>
    </ul>
    <!-- Breadcrumb End-->
    <div class="row">
      <!--Left Part Start -->
      <aside id="column-left" class="col-sm-3 hidden-xs">
        <h3 class="subtitle">Categories</h3>
        <div class="box-category">
          <ul id="cat_accordion">
            <?php
              if(count($menu)>0){
                foreach ($menu as $scroll) {
                  $nme_link1 =  $scroll->id_kategori.'-'.$scroll->nama_kategori;
                  $Id1     = strtolower(str_replace(" ", "-", $nme_link1));
                  $sub     = $this->home_model->menu_kategori('1',$scroll->id_kategori)->result();
                  ?>
            <li>
              <a href="<?php echo base_url();?>kategori/produk/<?php echo $Id1;?>"><?php echo $scroll->nama_kategori; ?></a> 
              <?php if(count($sub)>0){
                ?><span class="down"></span>                            
              <ul>
                <?php 
                  foreach ($sub as $subside) { 
                     $nme_link2 = $subside->id_kategori.'-'.$subside->nama_kategori;
                     $id2       = strtolower(str_replace(" ", "-", $nme_link2));
                     $sub2      = $this->home_model->menu_kategori('2',$subside->id_kategori)->result();  
                   ?>
                <li>
                  <a href="<?php echo base_url();?>kategori/produk/<?php echo $id2;?>"><?php echo $subside->nama_kategori; ?></a>
                  <?php if (count($sub2)>0) {
                    ?>
                  <span class="down"></span>
                  <ul>
                    <?php
                      foreach ($sub2 as $subside2) {
                          $nme_link3  = $subside2->id_kategori.'-'.$subside->nama_kategori;
                          $id3        = strtolower(str_replace(" ", "-", $nme_link3));
                          $sub3       = $this->home_model->menu_kategori('3',$subside->id_kategori)->result();
                        ?>
                    <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $id3;?>"><?php echo $subside2->nama_kategori; ?></a></li>
                    <?php   
                      } 
                      ?>
                  </ul>
                  <?php
                    } 
                    ?>
                </li>
                <?php
                  }
                  ?>
              </ul>
              <?php } ?>
            </li>
            <?php
              }
              }   
              ?>
          </ul>
        </div>
        <h3 class="subtitle">By Brands</h3>
        <div class="box-category">
          <ul id="cat_accordion">
            <?php
              if(count($perbrand)>0){
                foreach ($perbrand as $scroll) {
                  ?>
            <li><a href="<?php echo base_url();?>brand/get_produk/<?php echo $scroll->brand;?>"><?php echo $scroll->brand; ?></a></li>
            <?php
              }
              }   
              ?>
          </ul>
        </div>
      </aside>
      <!--Left Part End -->
      <!--Middle Part Start-->
      <div id="content" class="col-sm-9">
        <div class="row">
          <!--Middle Part Start-->
          <?php echo form_open('checkout/update_keranjang'); ?>
          <div id="content" class="col-sm-12">
            <h4 class="title">Shopping Cart</h4>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <td class="text-center">Gambar</td> -->
                    <td class="text-left">Nama Produk</td>
                    <td class="text-left">QTY</td>
                    <td class="text-right">Harga</td>
                    <td class="text-right">Sub Total</td>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($this->cart->contents() as $items): ?>
                  <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                  <tr <?php if($i>=1){ echo 'class="alt"'; }?>>
                    <!-- <td class="text-center"><a href="product.html"><img src="<?php echo base_url();?>assets/produk/<?php echo $items['options']['gbkecil']; ?>" data-image="<?php echo base_url();?>assets/produk/<?php echo $items['options']['gbkecil']; ?>" title="<?php echo $items['name']; ?>"></a></td> -->
                    <td class="text-left"><a href="product.html"><?php echo $items['name']; ?></a><br>
                    </td>
                    <td class="text-left">
                      <div class="input-group btn-block quantity">
                        <input type="text" name="quantity[]" value="<?php echo $items['qty']; ?>"  class="form-control">
                        <span class="input-group-btn">
                          <div class="pull-right">
                            <a href="<?php echo base_url(); ?>keranjang/remove_cart/<?php echo $items['rowid']; ?>" data-original-title="Remove" class="btn btn-danger"><strong>X</strong></a>
                          </div>
                        </span>
                      </div>
                    </td>
                    <td class="text-right"><?php echo  number_format($items['price'],2,",",".") ?></td>
                    <td class="text-right">Rp <?php
                      $harga=number_format($items['subtotal'],2,",",".");
                      echo $harga;
                      ?>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-sm-12 col-sm-offset-0">
                <table class="table table-bordered">
                  <tr>
                    <td class="text-left">
                        <div class="pull-left"><button type="submit" data-toggle="update keranjang" title="" class="btn btn-update" data-original-title="update keranjang">Update</button></div>
                    </td>
                    <td class="text-right"><strong>Total Belanja:</strong></td>
                    <td class="text-right">Rp <?php $harga_tot=number_format($this->cart->total(),2,",","."); echo $harga_tot; ?></td>
                  </tr>
                </table>
              </div>
            </div>
            <?php echo form_close(); ?>

            <?php 
                if(count($det_member)>0){
                  foreach ($det_member as $dm) {
                    $nama     = $dm->nama;
                    $email    = $dm->email;
                    $alamat   = $dm->alamat;
                    $telpon   = $dm->telpon;
                    $propinsi = $dm->propinsi;
                    $kota     = $dm->kota;
                    $kodepos  = $dm->kode_pos;
                  }
                }else{
                  $nama       = '';
                    $email    = '';
                    $alamat   = '';
                    $telpon   = '';
                    $propinsi = '';
                    $kota     = '';
                    $kodepos  = '';
                }
            ?>
            <div class="row">  
              
                <form method="post" action="<?php echo base_url(); ?>checkout/kirim_invoice" name="frmCheckout" id="frmCheckout">
                <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><i class="fa fa-user"></i> Detail Data Pembeli</h4>
                  </div>
                  <div class="panel-body">
                    <fieldset id="account">
                      <div class="form-group required">
                        <label for="input-payment-firstname" class="control-label">Nama Pembeli</label>
                        <input type="text" class="form-control" id="namapem" placeholder="Data Pembeli" value="<?php echo $nama; ?>" name="namapem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-email" class="control-label">E-Mail</label>
                        <input type="text" class="form-control" id="input-payment-email" placeholder="E-Mail" value="<?php echo $email; ?>" name="emailpem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-address-1" class="control-label">Alamat Pembeli</label>
                        <input type="text" class="form-control" id="input-payment-address-1" placeholder="alamat pembeli" value="<?php echo $alamat; ?>" name="alamatpem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-telephone" class="control-label">Telpon</label>
                        <input type="text" class="form-control" id="input-payment-telephone" placeholder="Telphone pembeli" value="<?php echo $telpon; ?>" name="telponpem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-propinsi" class="control-label">Propinsi</label>
                        <input type="text" class="form-control" id="input-payment-propinsi" placeholder="propinsi pembeli" value="<?php echo $propinsi; ?>" name="propinsipem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-kota" class="control-label">Kota</label>
                        <input type="text" class="form-control" id="input-payment-kota" placeholder="kota pembeli" value="<?php echo $kota; ?>" name="kotapem">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-kodepos" class="control-label">Kode pos</label>
                        <input type="text" class="form-control" id="input-payment-kodepos" placeholder="Kode pos pembeli" value="<?php echo $kodepos; ?>" name="kodepospem">
                      </div>
                      
                    </fieldset>
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><i class="fa fa-book"></i> Detail Data Pengiriman / Penerima</h4>
                  </div>
                  <div class="panel-body">
                    <fieldset id="address" class="required">
                      <div class="checkbox">
                        <label for="chkSame" style="cursor:pointer">
                        <input type="checkbox"  value="checkbox" name="chkSame" onClick="setPaymentInfo(this.checked);">
                        Data pengiriman sama dengan data pembeli.</label>
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-namapen" class="control-label" >Nama Penerima</label>
                        <input type="text" class="form-control" id="namapen" placeholder="Data Penerima" value="" name="namapen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-emailpen" class="control-label">E-Mail</label>
                        <input type="text" class="form-control" id="input-payment-emailpen" placeholder="E-Mail Penerima" value="" name="emailpen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-alamatpen" class="control-label">Alamat Penerima</label>
                        <input type="text" class="form-control" id="input-payment-alamatpen" placeholder="Alamat Penerima" value="" name="alamatpen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-telponpen" class="control-label">Telpon / Handphone Penerima</label>
                        <input type="text" class="form-control" id="input-payment-telponpen" placeholder="Telphone penerima" value="" name="telponpen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-propinsi" class="control-label">Propinsi</label>
                        <input type="text" class="form-control" id="input-payment-propinsi" placeholder="propinsi Penerima" value="" name="propinsipen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-kota" class="control-label">Kota</label>
                        <input type="text" class="form-control" id="input-payment-kota" placeholder="kota penerima" value="" name="kotapen">
                      </div>
                      <div class="form-group required">
                        <label for="input-payment-kodepospen" class="control-label">Kode pos</label>
                        <input type="text" class="form-control" id="input-payment-kodepospen" placeholder="Kode pos penerima" value="" name="kodepospen">
                      </div>
                    </fieldset>
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><i class="fa fa-book"></i> Metode Pembayaran dan Pengiriman Paket</h4>
                  </div>
                  <div class="panel-body">
                    <fieldset id="payment" class="required">
                      <div class="form-group">
                        <label for="bank">Bank Tujuan:</label>
                        <select class="form-control" id="bank" name="bank">
                          <option value="Bank Central Asia - No. Rek 1800658299">Bank Central Asia - No. Rek 1800658299</option>
                          <option value="Bank Mandiri - No. Rek 143-00-1170047-1">Bank Mandiri - No. Rek 143-00-1170047-1</option>
                          <option value="Bank BRI - No. Rek 6125-01-003271-53-9">Bank BRI - No. Rek 6125-01-003271-53-9</option>
                          <option value="Bank Mandiri Syariah - No. Rek 2857027105">Bank Mandiri Syariah - No. Rek 2857027105</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="bank">Metode Pembayaran:</label>
                        <select class="form-control" id="metode" name="metode">
                          <option value="Setoran Tunai, Transfer Bank">Setoran Tunai, Transfer Bank</option>
                          <option value="Setoran Tunai, Transfer Antar Bank">Setoran Tunai, Transfer Antar Bank</option>
                          <option value="ATM">ATM</option>
                          <option value="ATM - Antar Bank">ATM - Antar Bank</option>
                          <option value="Internet Banking">Internet Banking</option>
                          <option value="Internet Banking - Antar Bank">Internet Banking - Antar Bank</option>
                          <option value="SMS Banking">SMS Banking</option>
                          <option value="SMS Banking - Antar Bank">SMS Banking - Antar Bank</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="paket">Paket Pengiriman:</label>
                        <select class="form-control" id="paket" name="paket">
                          <option value="TIKI">TIKI</option>
                          <option value="JNE">JNE</option>
                          <option value="ESL Express">ESL Express</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="pesan">Pesan (jika ada):</label>
                        <textarea class="form-control" rows="5" id="pesan" name="pesan"></textarea>
                      </div>
                    </fieldset>
                  </div>
                </div>
                </div>
                <div class="buttons">
                    <span class="input-group-btn">
                      <div class="pull-left">
                          <button type="submit" data-toggle="Kirim Data Pesanan" title="" class="btn btn-default" data-original-title="Kirim Data Pesanan">Kirim Data Pesanan</button>
                      </div>
                    </span>
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>
      <!--Middle Part End -->
    </div>
  </div>
</div>