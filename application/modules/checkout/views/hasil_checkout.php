<div id="container">
  <div class="container">
    <!-- Breadcrumb Start-->
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i></a></li>
      <li><a href="<?php echo base_url();?>kategori">Kategori</a></li>
      <li><a href="<?php echo base_url();?>kategori/produk">Produk</a></li>
    </ul>
    <!-- Breadcrumb End-->
    <div class="row">
      <!--Left Part Start -->
      <aside id="column-left" class="col-sm-3 hidden-xs">
        <h3 class="subtitle">Categories</h3>
        <div class="box-category">
          <ul id="cat_accordion">
            <?php
              if(count($menu)>0){
                foreach ($menu as $scroll) {
                  $nme_link1 =  $scroll->id_kategori.'-'.$scroll->nama_kategori;
                  $Id1     = strtolower(str_replace(" ", "-", $nme_link1));
                  $sub     = $this->home_model->menu_kategori('1',$scroll->id_kategori)->result();
                  ?>
            <li>
              <a href="<?php echo base_url();?>kategori/produk/<?php echo $Id1;?>"><?php echo $scroll->nama_kategori; ?></a> 
              <?php if(count($sub)>0){
                ?><span class="down"></span>                            
              <ul>
                <?php 
                  foreach ($sub as $subside) { 
                     $nme_link2 = $subside->id_kategori.'-'.$subside->nama_kategori;
                     $id2       = strtolower(str_replace(" ", "-", $nme_link2));
                     $sub2      = $this->home_model->menu_kategori('2',$subside->id_kategori)->result();  
                   ?>
                <li>
                  <a href="<?php echo base_url();?>kategori/produk/<?php echo $id2;?>"><?php echo $subside->nama_kategori; ?></a>
                  <?php if (count($sub2)>0) {
                    ?>
                  <span class="down"></span>
                  <ul>
                    <?php
                      foreach ($sub2 as $subside2) {
                          $nme_link3  = $subside2->id_kategori.'-'.$subside->nama_kategori;
                          $id3        = strtolower(str_replace(" ", "-", $nme_link3));
                          $sub3       = $this->home_model->menu_kategori('3',$subside->id_kategori)->result();
                        ?>
                    <li><a href="<?php echo base_url();?>kategori/produk/<?php echo $id3;?>"><?php echo $subside2->nama_kategori; ?></a></li>
                    <?php   
                      } 
                      ?>
                  </ul>
                  <?php
                    } 
                    ?>
                </li>
                <?php
                  }
                  ?>
              </ul>
              <?php } ?>
            </li>
            <?php
              }
              }   
              ?>
          </ul>
        </div>
        <h3 class="subtitle">By Brands</h3>
        <div class="box-category">
          <ul id="cat_accordion">
            <?php
              if(count($perbrand)>0){
                foreach ($perbrand as $scroll) {
                  ?>
            <li><a href="<?php echo base_url();?>brand/get_produk/<?php echo $scroll->brand;?>"><?php echo $scroll->brand; ?></a></li>
            <?php
              }
              }   
              ?>
          </ul>
        </div>
      </aside>
      <!--Left Part End -->
      <!--Middle Part Start-->
      <div id="content" class="col-sm-9">
          <div class="row">
            <div id="content" class="col-sm-12">
                <h5 class="title"><?php echo $pesan; ?></h5>
            </div>
          </div>
      </div>
      <!--Middle Part End -->
    </div>
  </div>
</div>