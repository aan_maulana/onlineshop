<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loginadmin extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->load->model('login_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			redirect("dashboard","refresh");
		}else{
			$this->load->view('loginadmin/login_view');
		}
	}
	public function do_login(){
		$this->form_validation->set_rules('txtuser','Username','required');
        $this->form_validation->set_rules('txtpass','Password','required');
		if ($this->form_validation->run()==TRUE){
			$username=$this->kacang->anti($this->input->post('txtuser'));
			$password=md5($this->kacang->anti($this->input->post('txtpass')));
			$data['response'] = 'false';
			$result = $this->login_model->login($username,$password);	
			if($result==TRUE){
				$data['response'] = 'true';
			}
		}else{
			$data['response'] = 'false';
		}
		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  	
	}
}
