<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Admin LKLTSK online shop</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <link rel="shortcut icon" href="<?php echo base_url();?>assets-admin/img/favicon.ico">
    <link href="<?php echo base_url();?>assets-admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets-admin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets-admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets-admin/css/animate.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets-admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/style_nyunyu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/style-responsive_nyunyu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets-admin/css/theme/default_nyunyu.css">
    <link rel="stylesheet" href="http://mis.com/assets-admin/css/notifIt.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets-admin/css/notifIt.css" type="text/css" />
    <link href="<?php echo base_url();?>assets-admin/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
	<script src="<?php echo base_url();?>assets-admin/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets-admin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets-admin/js/notifIt.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets-admin/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets-admin/js/apps.min.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script type="text/javascript">
        function doLogin(){
            var host = window.location.host;
            $BASE_URL = 'http://'+host+'/';
            var uname = jQuery("#username").val();
            var password = jQuery("#password").val();
            if (uname == "") {
                jQuery("#username").effect('shake','1500').attr('placeholder','Username tidak boleh kosong');
            } else if (password == "") {
                jQuery("#password").effect('shake','1500').attr('placeholder','Password tidak boleh kosong');
            } else {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Pengecekan Username dan Password <br/> Mohon menunggu ... '
                });
                
                jQuery.ajax({
                    url : $BASE_URL+'loginadmin/do_login',
                    data : {txtuser:uname,txtpass:password},
                    type : 'POST',
                    dataType: 'json',
                    success:function(data){
                        jQuery.unblockUI();
                        if(data.response=='true'){
                            jQuery.blockUI({
                                css: { 
                                    border: 'none', 
                                    padding: '15px', 
                                    backgroundColor: '#000', 
                                    '-webkit-border-radius': '10px', 
                                    '-moz-border-radius': '10px', 
                                    opacity: 2, 
                                    color: '#fff' 
                                },
                                message : 'Berhasil Login !!! Sedang Memuat Halaman, Mohon menunggu ... '
                            });
                            window.location = $BASE_URL+'dashboard';
                            setTimeout(function(){
                                jQuery.unblockUI();
                            },1000);
                        } else {
                            notif({
                                type: "warning",
                                msg: "Ups Username dan Password Anda tidak dikenal Silahkan Login Kembali",
                                position: "center",
                                width: 500,
                                height: 60,
                                autohide: true
                            });
                            jQuery("#username").val('');
                            jQuery("#password").val('');
                        }            
                    }
                });
            }
        }
    </script>
</head>
<body class="pace-top bg-white">
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<div id="page-container" class="fade">
        <div class="login login-with-news-feed">
            <div class="news-feed">
                <div class="news-image">
                <?php
                $gal = $this->db->query("SELECT * FROM tbl_bglogin ORDER BY RAND()")->result();
                foreach ($gal as $key) {
                    $bgna = $key->logo;
                }
                ?>
                    <img src="<?php echo base_url();?>assets-admin/img/login-bg/<?php echo $bgna;?>" data-id="login-cover-image" alt="" />
                </div>
                <div class="news-caption">
                <h4 class="caption-title"><i class="ion-ios-list-outline fa-2x text-success"></i> LKLTSK ONLINE STORE PANEL ADMIN </h4>
                    <?php
                    $profile = $this->db->query("SELECT nama,alamat FROM tbl_profile")->result();
                    if(count($profile)>0){
                        foreach ($profile as $hey) {
                            $nama = $hey->nama;
                            $alamat = $hey->alamat;
                        }
                        ?>
                        <p>
                            <?php echo $nama;?>
                        </p>
                        <?php
                    }else{
                        ?>
                        <p>
                            LKLTSK
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="right-content">
                <div class="login-header">
                    <div class="brand">
                        <span class="logo"></span> LKLTSK
                        <small>Online Store</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sign-in"></i>
                    </div>
                </div>
                <div class="login-content">
	                <form action='javascript:doLogin()' autocomplete='off' method="POST" class="margin-bottom-0">
                        <div class="form-group m-b-15">
                            <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Masukan Username" />
                        </div>
                        <div class="form-group m-b-15">
                            <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Masukan Password" />
                        </div>
                        <div class="login-buttons">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Login</button>
                        </div>
                        <div class="m-t-20 m-b-40 p-b-40">
                            Belum Registrasi ? Klik <a href="<?php echo base_url();?>reg" class="text-success">Disini</a> Untuk Melakukan Registrasi.
                        </div>
                        <hr />
                        
                    </form>
                </div>
            </div>
        </div>
	</div>
</body>
</html>
