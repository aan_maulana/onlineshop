<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class D_transaksi extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('d_transaksi/Dtrans_model');
 	}
	public function index(){
		if($this->session->userdata('loginadmin')==TRUE){
			$this->_content();
		}else{
			redirect("loginadmin","refresh");
		}
	}
	public function _content(){
		if($this->session->userdata('loginadmin')==TRUE){
			$menu = 'mntrans';
			$cekmenu = $this->db->get_where('tbl_menu',array('kelas'=>$menu))->result();
			foreach ($cekmenu as $menu) {
				$statusmenu = $menu->status;
			}
			if($statusmenu=='1'){	
				$submenu = 'Info Transaksi';
				$cksubmenu = $this->db->get_where("tbl_submenu",array("nama_smenu"=>$submenu))->result();
				foreach ($cksubmenu as $submenu) {
					$statussubmenu = $submenu->sstatus;	
				}
				$page = "D_transaksi";
				$amenu = $this->dashboard_model->cekmenu($page);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['kelas'] = "mntrans";
					$isi['namamenu'] = "Info Transaksi";
					$isi['page'] = "D_transaksi";
					$isi['link'] = 'D_transaksi';
					$isi['actionhapus'] = 'hapus';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "History Data Transaksi";
					$isi['judul'] = "Halaman Data Transaksi";
					$isi['content'] = "d_transaksi_view";
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function get_data(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$aColumns = array('kode_transaksi','nama_penerima','email_penerima','alamat_penerima','propinsi','kota','kodepos','telpon','kode_transaksi');
	        $sIndexColumn = "kode_transaksi";
	        // pagings
	        $sLimit = "";
	        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
	            $sLimit = "LIMIT ".$this->kacang->anti( $_GET['iDisplayStart'] ).", ".
	                $this->kacang->anti( $_GET['iDisplayLength'] );
	        }
	        $numbering = $this->kacang->anti( $_GET['iDisplayStart'] );
	        $page = 1;
	        // ordering
	        if ( isset( $_GET['iSortCol_0'] ) ){
	            $sOrder = "ORDER BY  ";
	            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
	                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
	                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
	                        ".$this->kacang->anti( $_GET['sSortDir_'.$i] ) .", ";
	                }
	            }            
	            $sOrder = substr_replace( $sOrder, "", -2 );
	            if ( $sOrder == "ORDER BY" ){
	                $sOrder = "";
	            }
	        }
	        // filtering
	        $sWhere = "";
	        if ( $_GET['sSearch'] != "" ){
	            $sWhere = "WHERE (";
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti( $_GET['sSearch'] )."%' OR ";
	            }
	            $sWhere = substr_replace( $sWhere, "", -3 );
	            $sWhere .= ')';
	        }
	        // individual column filtering
	        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
	                if ( $sWhere == "" ){
	                    $sWhere = "WHERE ";
	                }
	                else{
	                    $sWhere .= " AND ";
	                }
	                $sWhere .= $aColumns[$i]." LIKE '%".$this->kacang->anti($_GET['sSearch_'.$i])."%' ";
	            }
	        }
	        $rResult = $this->Dtrans_model->data_trans($aColumns, $sWhere, $sOrder, $sLimit);
	        $iFilteredTotal = 10;
	        $rResultTotal = $this->Dtrans_model->data_trans_total($sIndexColumn);
	        $iTotal = $rResultTotal->num_rows();
	        $iFilteredTotal = $iTotal;
	        $output = array(
	            "sEcho" => intval($_GET['sEcho']),
	            "iTotalRecords" => $iTotal,
	            "iTotalDisplayRecords" => $iFilteredTotal,
	            "aaData" => array()
	        );
	        foreach ($rResult->result_array() as $aRow){
	            $row = array();
	            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
	                /* General output */
	                if($i < 1)
	                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
	                else
	                    $row[] = $aRow[ $aColumns[$i] ];
	            }
	            $page++;
	            $output['aaData'][] = $row;
	        }
	        echo json_encode( $output );
	    }    
	}
	
	public function add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'D_transaksi';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$isi['cek']	 = "add";
					$isi['kelas'] = "D_transaksi";
					$isi['namamenu'] = "Tambah Data Produk";
					$isi['page'] = "D_transaksi";
					$isi['link'] = 'D_transaksi';
					$isi['actionhapus'] = 'hapus';
					$isi['action'] = "proses_add";
					$isi['tombolsimpan'] = 'Simpan';
					$isi['tombolbatal'] = 'Batal';
					$isi['actionedit'] = 'edit';
					$isi['halaman'] = "Tambah Data Produk";
					$isi['judul'] = "Halaman Data Produk";
					$isi['jk'] = "";
					$isi['content'] = "D_transaksi/form_prod";

					$isi['option_kategori']['']="-- Pilih kategori --";
					$tipe = $this->db->query("SELECT * FROM tbl_kategori where status = '1'ORDER BY id_kategori ASC")->result();
					foreach($tipe as $tah){
						$isi['option_kategori'][$tah->id_kategori] = strtoupper($tah->nama_kategori);
					}
					$this->load->view("dashboard/dashboard_view",$isi);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_add(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'D_transaksi';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('kode_produk','kode produk','required|min_length[2]');
		 			$this->form_validation->set_rules('nama','nama produk','required|min_length[2]');
		 			$this->form_validation->set_rules('brand','band produk','required|min_length[2]');
		 			$this->form_validation->set_rules('jnskategori','jnskategori','required');
		 			$this->form_validation->set_rules('harga','harga produk','required');
		 			$this->form_validation->set_rules('stok','stok produk','required|numeric');
		 			$this->form_validation->set_rules('tipe','tipe produk','required');
		 			$this->form_validation->set_rules('desc','desc','required');

					if ($this->form_validation->run() == TRUE){
						
						$kode_produk = $this->kacang->anti($this->input->post('kode_produk'));
						$nama = $this->kacang->anti($this->input->post('nama'));
						$brand = $this->kacang->anti($this->input->post('brand'));
						$jnskategori = $this->kacang->anti($this->input->post('jnskategori'));
						$harga = $this->kacang->anti(str_replace(",", "", $this->input->post('harga')));						
						$stok = $this->kacang->anti($this->input->post('stok'));
						$desc = $this->kacang->anti($this->input->post('desc'));					
						$tipe = $this->kacang->anti($this->input->post('tipe'));
													
						$cekcode = $this->db->get_where('tbl_produk',array('kode_produk'=>$kode_produk))->result();
						if(count($cekcode)>0){
							?> 
								<script type="text/javascript">
					 				alert("Data Tidak Bisa disimpan Kode Produk / nama produk Sudah Ada Sebelumnya !");
					 				window.location.href = "<?php echo base_url();?>D_transaksi/add";
				 				</script>
							<?php
						}else{
							$foto = $this->kacang->anti(str_replace(" ", "_", $_FILES['foto']['name']));
							$nmfile = "file_".time();
				 			$tmpName = $_FILES['foto']['tmp_name'];
				 			
				 			$bigfoto = $this->kacang->anti(str_replace(" ", "_", $_FILES['bigfoto']['name']));
							$nmfile2 = "file_".time();
				 			$tmpName2 = $_FILES['bigfoto']['tmp_name'];

				 			if($tmpName!=''){
				 				$config['file_name'] = $nmfile;
								$config['upload_path'] = 'assets/produk/detail/';
								$config['allowed_types'] = 'jpg|jpeg|png';
								$config['max_size'] = '104800';
								$config['max_width'] = '0';
						    	$config['max_height'] = '0';
						   		$config['overwrite'] = TRUE;
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if ($this->upload->do_upload('foto')){
									$gbr = $this->upload->data();
									$simpan = array('kode_produk'=>$kode_produk,'nama_produk'=>$nama,'brand'=>$brand,'id_kategori'=>$jnskategori,
										  'harga'=>$harga,'stok'=>$stok,'stts'=>1,'tipe_produk'=>$tipe,'deskripsi'=>$desc,
										  'gbr_kecil'=>$gbr['file_name']
										  );
								}else{
									?>
									<script type="text/javascript">
						                alert("Pastikan  gambar kecil type file jpeg /jpg / png /  dan ukuran file maksimal 10MB");
						                window.location.href="<?php echo base_url();?>D_transaksi";
									</script>
									<?php
								}
				 			}else{
								$simpan = array('kode_produk'=>$kode_produk,'nama_produk'=>$nama,'brand'=>$brand,'id_kategori'=>$jnskategori,
												'harga'=>$harga,'stok'=>$stok,'stts'=>1,'tipe_produk'=>$tipe,'deskripsi'=>$desc
										  );				 				
				 			}

	                        $this->db->trans_start();
							$this->db->insert('tbl_produk',$simpan);
							// $this->db->insert('tbl_username',$simpanusername);
							// $this->db->insert('tbl_usermenu',$simpanusermenu);
							$this->db->trans_complete();
							redirect('D_transaksi','refresh');

						}	
	                }else{
                        $this->add();
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'D_transaksi';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					if($jns=="aktif"){
						$data = array('stts'=>'0');
					}else{
						$data = array('stts'=>'1');
					}
					$this->db->where('kode_produk',$this->kacang->anti($id));	
					$this->db->update('tbl_produk',$data);
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}

	public function hapus($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'D_transaksi';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_produk',array('kode_produk'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$this->hapusfoto($kode);
							$this->db->where('kode_produk',$this->kacang->anti($kode));
							if($this->db->delete('tbl_produk')){	
								$data['say'] = "ok";
							}else{
								$data['say'] = "NotOk";
							}
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}

	function hapusfoto($kode=null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			$ckdata = $this->db->get_where('tbl_produk',array('kode_produk'=>$kode))->result();
			foreach ($ckdata as $hehe) {
				$fotona = $hehe->gbr_kecil;
			}
			if($fotona!=""){
				if($fotona!="no.jpg"){
					unlink('assets/produk/detail/' . $fotona);
				}
			}
		}	
	}

	public function cekdata($kode=Null){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}else{
			if($this->session->userdata('loginadmin')==TRUE){
				$tah = 'D_transaksi';
				$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
				foreach ($cekheula as $xxx) {
					$sstatus = $xxx->sstatus;
				}
				if($sstatus=='1'){	
					$amenu = $this->dashboard_model->cekmenu($tah);
			 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
						$ckdata = $this->db->get_where('tbl_produk',array('kode_produk'=>$this->kacang->anti($kode)))->result();
						if(count($ckdata)>0){
							$data['say'] = "ok";
						}else{
							$data['say'] = "NotOk";
						}
						if('IS_AJAX'){
						    echo json_encode($data); //echo json string if ajax request
						}  	
					}else{
						redirect('error','refresh');
					}
			   	}else{
					redirect('error','refresh');
				}
			}else{
				redirect('loginadmin','refresh');
			}
		}	
	}
	public function edit($kode=Null){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'D_transaksi';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
					$ckdata = $this->db->get_where('tbl_produk',array('kode_produk'=>$this->kacang->anti($kode)))->result();
					if(count($ckdata)>0){
						foreach ($ckdata as $key) {
						}
						$isi['default']['kode_produk'] 	= $key->kode_produk;
						$isi['default']['nama'] 	= $key->nama_produk;
						$isi['default']['brand'] = $key->brand;
						$isi['default']['harga'] = $key->harga;
						$isi['default']['stok'] = $key->stok;
						$isi['default']['tipe'] = $key->tipe_produk;
						$isi['default']['desc'] = $key->deskripsi;
						$isi['foto'] = $key->gbr_kecil;

						$id_kategorix = $key->id_kategori;
						$ck_kategori = $this->db->query("SELECT * FROM tbl_kategori WHERE id_kategori = '$id_kategorix' ORDER BY id_kategori ASC")->result();
						if(count($ck_kategori)>0){
							foreach ($ck_kategori as $jkategori) {
							}
								$isi['option_kategori'][$id_kategorix] = strtoupper($jkategori->nama_kategori);
						}else{
							$isi['option_kategori'][''] = "-- Pilih kategori --";
						}

						$ck_kategoris = $this->db->query("SELECT * FROM tbl_kategori ORDER BY id_kategori ASC")->result();
						if(count($ck_kategoris)>0){
							foreach($ck_kategoris as $ckt){
								$isi['option_kategori'][$ckt->id_kategori] = strtoupper($ckt->nama_kategori);
							}
						}else{
							$isi['option_kategori'][""] = "-- Pilih kategori --";							
						}

						$this->session->set_userdata('skode_produk',$kode);
						$isi['cek']	 = "edit";
						$isi['kelas'] = "D_transaksi";
						$isi['namamenu'] = "Edit Data Produk";
						$isi['page'] = "D_transaksi";
						$isi['link'] = 'D_transaksi';
						$isi['actionhapus'] = 'hapus';
						$isi['action'] = "proses_edit";
						$isi['tombolsimpan'] = 'Edit';
						$isi['tombolbatal'] = 'Batal';
						$isi['actionedit'] = 'edit';
						$isi['halaman'] = "Edit Data Produk";
						$isi['judul'] = "Halaman Data Produk";
						$isi['content'] = "D_transaksi/form_prod";
						
						$this->load->view("dashboard/dashboard_view",$isi);
					}else{
						redirect('error','refresh');
					}
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}
	}
	public function proses_edit(){
		if($this->session->userdata('loginadmin')==TRUE){
			$tah = 'D_transaksi';
			$cekheula = $this->db->get_where('tbl_submenu',array('slink'=>$tah))->result();
			foreach ($cekheula as $xxx) {
				$sstatus = $xxx->sstatus;
			}
			if($sstatus=='1'){	
				$amenu = $this->dashboard_model->cekmenu($tah);
		 		if($this->session->userdata('level')=='0' || $amenu==TRUE){	
		 			$this->form_validation->set_rules('nama','nama produk','required|min_length[2]');
		 			$this->form_validation->set_rules('brand','band produk','required|min_length[2]');
		 			$this->form_validation->set_rules('jnskategori','jnskategori','required');
		 			$this->form_validation->set_rules('harga','harga produk','required');
		 			$this->form_validation->set_rules('stok','stok produk','required|numeric');
		 			$this->form_validation->set_rules('tipe','tipe produk','required');
		 			$this->form_validation->set_rules('desc','desc','required');
					if ($this->form_validation->run() == TRUE){	
						
						$nama = $this->kacang->anti($this->input->post('nama'));
						$brand = $this->kacang->anti($this->input->post('brand'));
						$jnskategori = $this->kacang->anti($this->input->post('jnskategori'));
						$harga = $this->kacang->anti(str_replace(",", "", $this->input->post('harga')));						
						$stok = $this->kacang->anti($this->input->post('stok'));
						$desc = $this->kacang->anti($this->input->post('desc'));					
						$tipe = $this->kacang->anti($this->input->post('tipe'));
						$cekcode = $this->db->get_where('tbl_produk',array('kode_produk'=>$this->session->userdata('skode_produk')))->result();
						if(count($cekcode)>0){
							$foto = $this->kacang->anti(str_replace(" ", "_", $_FILES['foto']['name']));
							$nmfile = "file_".time();
				 			$tmpName = $_FILES['foto']['tmp_name'];
				 			if($tmpName!=''){
				 				$config['file_name'] = $nmfile;
								$config['upload_path'] = 'assets/produk/detail/';
								$config['allowed_types'] = 'jpg|jpeg|png';
								$config['max_size'] = '104800';
								$config['max_width'] = '0';
						    	$config['max_height'] = '0';
						   		$config['overwrite'] = TRUE;
						   		$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if ($this->upload->do_upload('foto')){
									$gbr = $this->upload->data();
									$edit = array('nama_produk'=>$nama,'brand'=>$brand,'id_kategori'=>$jnskategori,
										  'harga'=>$harga,'stok'=>$stok,'stts'=>1,'tipe_produk'=>$tipe,'deskripsi'=>$desc,
										  'gbr_kecil'=>$gbr['file_name']
										  );
								}else{
									?>
									<script type="text/javascript">
						                alert("Pastikan  gambar kecil type file jpeg /jpg / png /  dan ukuran file maksimal 10MB");
						                window.location.href="<?php echo base_url();?>D_transaksi/edit/<?php echo $this->session->userdata('skode_produk'); ?>";
									</script>
									<?php	
								}	
						   	}else{
						   		$edit = array('nama_produk'=>$nama,'brand'=>$brand,'id_kategori'=>$jnskategori,
										  'harga'=>$harga,'stok'=>$stok,'stts'=>1,'tipe_produk'=>$tipe,'deskripsi'=>$desc
										  );
						   	}	
							$this->db->where('kode_produk',$this->kacang->anti($this->session->userdata('skode_produk')));
							$this->db->update('tbl_produk',$edit);
							$this->session->unset_userdata('skode_produk');
							redirect('D_transaksi','refresh');
						}	
	                }else{
						redirect(base_url().'D_transaksi/edit/'.$this->session->userdata('skode_produk'),'refresh');
	                }
				}else{
					redirect('error','refresh');
				}
			}else{
				redirect('error','refresh');
			}
		}else{
			redirect('loginadmin','refresh');
		}	
	}
}
