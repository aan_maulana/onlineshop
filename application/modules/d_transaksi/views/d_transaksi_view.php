<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets-admin/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets-admin/js/table-manage-responsive.demo.min.js"></script>

<script>
$(document).ready(function() {

    TableManageResponsive.init();
    var host = window.location.host;
    $BASE_URL = 'http://'+host+'/';  
    $('#data-produk').dataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp = $('td:eq(0)', nRow).text();
            var temp = temp.split('|');
            var no = temp[0]+".";
            var kode = temp[1];
            var nama = $('td:eq(1)', nRow).text();
            var email = $('td:eq(2)', nRow).text();
            var alamat = $('td:eq(3)', nRow).text();
            var propinsi = $('td:eq(4)', nRow).text();
            var kota = $('td:eq(5)', nRow).text();
            var kodepos = $('td:eq(6)', nRow).text();
            var telpon = $('td:eq(7)', nRow).text();
            
            
            $('td:eq(0)', nRow).html(no);
            $('td:eq(1)', nRow).html(kode);
            $('td:eq(2)', nRow).html(nama);
            $('td:eq(3)', nRow).html(email);
            $('td:eq(4)', nRow).html(alamat);
            $('td:eq(5)', nRow).html(propinsi);
            $('td:eq(6)', nRow).html(kota);
            $('td:eq(7)', nRow).html(kodepos);
            $('td:eq(8)', nRow).html(telpon);
            $('td:eq(0),td:eq(3),td:eq(4)', nRow).css('text-align','center');
        },
        "bAutoWidth": false,
        "aoColumns": [
            { "sWidth": "1%" },
            { "sWidth": "10%" },
            { "sWidth": "20%" },
            { "sWidth": "20%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "10%" },
            { "sWidth": "1%" }
        ],
        "aoColumnDefs": [ {
               "aTargets": [4],
               "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                 var $currencyCell = $(nTd);
                 var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                 $currencyCell.text(commaValue);
               }
            }],
        "bProcessing": false,
        "bServerSide": true,
        "responsive":false,
        "sAjaxSource": $BASE_URL+"d_transaksi/get_data"
    });
    $('#data-nasabah').each(function(){
        var datatable = $(this);
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    });
});
</script>    
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="<?php echo base_url();?><?php echo $link;?>/add" title="Tambah <?php echo $halaman;?>" class="btn btn-primary btn-xs m-r-5"><i class="icon-plus-sign"></i> Tambah Data</a>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table id="data-produk" class="table table-striped table-bordered nowrap" width="100%">
                        <thead> 
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">Kode Transaksi</th>
                                <th style="text-align:center" width="10%">Nama Penerima</th>
                                <th style="text-align:center" width="20%">Email Penerima</th>
                                <th style="text-align:center" width="20%">Alamat Pengirim</th>
                                <th style="text-align:center" width="10%">Propinsi</th>
                                <th style="text-align:center" width="10%">Kota</th>
                                <th style="text-align:center" width="9%">Kode Pos</th>
                                <th style="text-align:center" width="10%">telpon</th>
                            </tr>
                        </thead>
                        <tbody>                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>