<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dtrans_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    /*==================================== data_produk ============================================*/


    function data_trans($aColumns, $sWhere, $sOrder, $sLimit){
        $query = $this->db->query("
           SELECT * FROM (
                SELECT th.kode_transaksi,th.nama_penerima,th.email_penerima, th.alamat_penerima,
                th.propinsi,th.kota,th.kodepos,th.telpon,th.paket_kirim,th.bank,th.pesan from tbl_transaksi_header th
                ORDER BY th.kode_transaksi ASC
            ) A 
            $sWhere
            $sOrder
            $sLimit
        ");
        return $query;
        $query->free_result();
    }

    function data_trans_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT th.kode_transaksi,th.nama_penerima,th.email_penerima, th.alamat_penerima,
                th.propinsi,th.kota,th.kodepos,th.telpon,th.paket_kirim,th.bank,th.pesan from tbl_transaksi_header th
                ORDER BY th.kode_transaksi ASC
            ) A
        ");
        return $query;
    }   

}